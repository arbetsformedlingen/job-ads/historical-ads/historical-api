# Search API for historical ads - getting started

The historical search API intends to make it easier for more people to work with [historical ads data sets](https://data.jobtechdev.se/annonser/historiska/index.html). Since they are large it takes a bit of computer savy to get something usefull out of them. This API also tries to help out with a bit of data cleaning often needed to work with the real sets. This means that the API can be a helpful resource for trends but not exact statistics. It means you are likely to find ads where the data has been "cleaned" I.E changed to be more usable with the API but you will also still find ads with data errors. The API is a usefull tool for historical ads but should not be considered as the exact truth about whats has been published in Platsbanken.


The aim of this text is to walk you through what you're seeing in the [swagger interface for historical ads](https://historical.api.jobtechdev.se/) to give you a bit of orientation on what can be done with the Historical ads Search API. If you are looking for a way get all the current ads use [Stream API](https://jobtechdev.se/sv/komponenter/jobstream) and if you want to search among current ads you can use [JobSearch](https://jobsearch.api.jobtechdev.se)
To make as much use of all ready written code as possible this application reuses most of JobSearch. Because of that it has several features that doesnt work well with the historical data sets. The things we have focused on to start with are the features most usefull to answer the type of questions asked of Arbetsförmedlingen regarding historical ads.


Provide a table of what taxonomy versions used through time? Or describe the risk of changing ids while expaining we're trying to ad relevant concept_id to all?
Change of date time throghout the years
Number of vaccancies changed from string to number 
Describe all the data changes done in the importer
Common usage scenarios that we've been able to solve using API


# Table of Contents
* [Short Introduction](#short-introduction)
* [License](#license)
* [Authentication](#authentication)
* [Endpoints](#endpoints)
* [Results](#results)
* [Errors](#errors)
* [Use cases](#use-cases)
* [Ad fields](#ad-fields)
* [Whats next](#whats-next)

## Short introduction

The endpoints for the historical API are:
* `/search` - returning ads matching a search phrase.
* `/stats` - get information on how many ads match certain terms
* `/ad/<id>`- returning the ad matching an id.

The easiest way to try out the API is to go to the [Swagger-GUI](https://historical.api.jobtechdev.se/).


## License
The ads in the historical-ads database are
[CC0](https://creativecommons.org/publicdomain/zero/1.0/deed) and the source code for the API is [Apache 2](https://www.apache.org/licenses/LICENSE-2.0) 


## Endpoints
In this document we only show the URLs to the endpoint. If you prefer the curl command, it's used as follwing:

	curl "{URL}" -H "accept: application/json"

The base URL to the historical API is 
	
	https://historical.api.jobtechdev.se 




### Ad
/ad/{id} 

This endpoint is used for fetching specific job ads with all available meta data, by their ad ID number. The ID number can be found by doing a search query.

	https://historical.api.jobtechdev.se/ad/8430129

To handle duplicates, the historical ads have a new id which is created as a hash of several ad fields. 
The ad´s original id is available in the field "original_id".
Both new and original ids can be used in this endpoint.
In case there are duplicates for an id, this endpoint will raise an error (http 409 / Conflict) with a message saying which ids match the desired id.



### Code examples
Code examples for accessing the api can be found in the 'getting-started-code-examples' repository on Github: 
https://github.com/JobtechSwe/getting-started-code-examples


### Jobtech-Taxonomy 
If you need help finding the official names for occupations, skills, or geographic locations you will find them in our [Taxonomy API](https://jobtechdev.se/en/products/jobtech-taxonomy) or use the user friendly frontend application [Jobtech Atlas](https://atlas.jobtechdev.se/page/taxonomy.html)

## Results
The results of your queries will be in [JSON](https://en.wikipedia.org/wiki/JSON) format. We won't attempt to explain this attribute by attribute in this document. Instead we've decided to try to include this in the data model which you can find in our [Swagger-GUI](https://jobsearch.api.jobtechdev.se).

Successful queries will have a response code of 200 and give you a result set that consists of:
1. Some meta data about your search such as number of hits and the time it took to execute the query and 
2. The ads that matched your search. 


## Errors
Unsuccessful queries will have a response code of:

| HTTP Status code | Reason                | Explanation                                               |
|------------------|-----------------------|-----------------------------------------------------------|
| 400              | Bad Request           | Something wrong in the query                              |
| 404              | Missing ad            | The ad you requested is not available                     |
| 409              | Conflict              | An ad id is duplicated, new ids are found in the response |
| 500              | Internal Server Error | Something wrong on the server side                        |


## Searching
More info on the different parameters that can be used are found in the "GettingStartedJobSearch" documents.


## Info for developers
# Historical Ads API

## Running the app

To run the Historical Ads API, set FLASK_APP=historical


## Editing the app

Most of the code is shared between Historical and Jobsearch.
The following files are of special interest when building functionality for Historical:

### historical.py
Starts the Flask app.


### search/rest/endpoint/historical.py
Defines the endpoints for Historical.

### search/rest/endpoint/common.py
Endpoint functions that are common between Jobsearch and Historical.

### search/rest/model/apis.py
Definition of the APIs for both Jobsearch and Historical. Three sections:
1. First the root_api is augmented with all models that are common
   to Jobsearch and Historical, but do not belong in Jobstream.
2. Then API for Jobsearch is created, adding parameters that is unique for Jobsearch
3. Finally the Historical API is created.

### search/rest/model/queries.py
Here the endpoint query fields are defined:
* **base_annons_query** - common for Jobsearch and Historical
* **annons_complete_query** - for Jobsearch /complete
* **annons_search_query** - for Jobsearch /search
* **annons_complete_query** - for Historical /search

### search/repository/querybuilder.py
The same query builder is used both for Jobsearch and Historical,
but the behaviour will differ depending on app and endpoint - especially concerning 'aggs'.
(More to come about how we will deal with this)
