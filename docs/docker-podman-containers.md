# Running a Historical Container

**Note:** This might not be fully correct for current `Dockerfile`.

These instructions shall work on Linux and on WSL 2 with podman installed.
If you not run podman, replace `podman` with `docker` and it shall work.

## Install Opensearch localhost on podman or docker desktop:
This installs latest opensearch image with no authentication, e.g. username/password dont need to be provided when querying.
Prerequisite: Install podman + podman-compose or docker desktop.

### 1. Build a new dashboard
This disables the security plugin and builds a new image
tagged opensearch-dashboards-no-security.
#### If Podman:
- In a shell move to [this repo]/opensearch/docker/
- Run `podman build --tag=opensearch-dashboards-no-security .`
###
- In a shell move to [this repo]/opensearch/docker/
- Run `docker build --tag=opensearch-dashboards-no-security .`

### 2. Run opensearch:
This uses the new dashboard image previoudly built.
Starts the containers in detached mode.
#### If Podman:
- Run: `podman-compose up -d`
#### If Docker:
- Run: `docker-compose up -d`

Verify start up by surfing into Opensearch Dashboards in a browser: localhost:5601

### Stopping
To stop and remove containers
#### If Podman:
- Run: `podman-compose down`

#### If Docker:
- Run: `docker-compose down`

### Troubleshoot docker for desktop on Windows:
If open search fails to start in docker for desktop on Windows due to 'vm.max_map_count [65530] is too low':

- In Powershell as admin:
```
docker-machine ssh
sudo sysctl -w vm.max_map_count=262144
```

### Create indices in opensearch:
```shell
curl -X PUT http://$(hostname -I | xargs):9200/jae-synonym-dictionary
curl -X PUT http://$(hostname -I | xargs):9200/platsannons-read
```

If you run both IPv4 and IPv6, `hostname -I` might return two addresses. Then just
replace `$(hostname -I | xargs)` with your primary Ipv4 address.

### Start

In another terminal window build and start historical:
```shell
podman build -t historical:dev .
podman run -e ES_HOST=$(hostname -I) -p 8081:8081 --name=historical --rm historical:dev
```
Troubleshoot: if you dont have access rights to docker-images.jobtechdev.se, try run:
```shell
podman login docker-images.jobtechdev.se
```

Now you can point your browser on the IP `hostname -I` shows and 
port 8081, example `http://172.18.9.180:8081/`. It will show the 
API-documentation.

To stop the containers:
```shell
podman kill historical opensearch
```

## MAC

These instructions shall work on Mac with docker installed.

Turn off VPN!

## Install Opensearch localhost on podman or docker desktop:
See default section above 

Create indices in Opensearch:
```shell
curl -X PUT http://localhost:9200/jae-synonym-dictionary
curl -X PUT http://localhost:9200/platsannons-read
```

### Start

In another terminal build and start historical:
```shell
docker build -t historical:dev .
docker run -e ES_HOST=host.docker.internal -p 8081:8081 --name=historical --rm historical:dev
```

The API is accessible at http://0.0.0.0:8081, and also at http://localhost:8081.

To stop the containers:
```shell
docker kill historical elastic
```
