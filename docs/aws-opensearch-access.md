# OpenSearch Access

## DEPRECATED - this is for the old deployments
Don't configure anything new in AWS OpenSearch.

## Installations

Amazon OpenSearch Service is used to run OpenSearch. It gives us a managed service
and lower maintenance. They runs within the same VPC
and Zone as corresponding OpenShift cluster. We will have
one setup per environment and system.

Reason we separate usage of OpenSearch is due to the fact
that sometimes a high load on one system have cause issues
on another. We want to avoid this.

To access the clusters from your local machine a tunnel via an http proxy is needed,
read more in the section [Access](#access).

Instructions to setup OpenSearch on AWS can be found
[here](https://gitlab.com/arbetsformedlingen/job-ads/job-ads-env-init/-/blob/main/docs/install-opensearch-aws.md).

### Dev configuration

This is a single node instance of OpenSearch intended for
development.

|Param   | Value |
|--------|-----------|
| Region | Frankfurt |
| Name   | historical-develop |
| Nodes  | 1 |
| VPC    | vpc-0f5419acdf761f3f2 |
| URL    | https://vpc-historical-develop-pnbx4klyn7go7y3vk3x5xrg7ne.eu-central-1.es.amazonaws.com |
| Dashboard | https://vpc-historical-develop-pnbx4klyn7go7y3vk3x5xrg7ne.eu-central-1.es.amazonaws.com/_dashboards |

### Staging configuration

*TBD* This is still not setup with OpenSearch, its running
Opensearch to be able to do comparison.

### Prod configuration

|Param   | Value |
|--------|-----------|
| Region | Stockholm |
| Name   | historical-prod |
| Nodes  | 3 |
| VPC    | vpc-049bc4b81fbd93a2a |
| URL    | https://vpc-historical-prod-opqvowt5py3vh4ntkjo7rtrkmi.eu-north-1.es.amazonaws.com |
| Dashboard | https://vpc-historical-prod-opqvowt5py3vh4ntkjo7rtrkmi.eu-north-1.es.amazonaws.com/_dashboards |

*Note:* This cluster is not reachable from the standby OpenShift cluster.

## Access

### Access Dev

Login to the OpenShift test/dev-cluster with `oc`.

Forward port from the openserach proxy to you local machine:
`oc port-forward -n historical-develop svc/proxy-historical-os 8080:8080`
Keep the command running as long as you use the tunnel.

*Note:* Do not try to set up route or ingress to the service, that will open security issues.

From a browser go to `http://localhost:8080/_dashboards`.
It is okej to use http since the traffic between you and the service is in an encrypted tunnel

Login with your OpenSearch user.

If you want to run an app towards OpenSearch, use the URL `http://localhost:8080/`.

Once you are done with the tunnel, press `^C` to interrupt `oc port-forward`.

### Access Staging

*TBD*

### Access Prod

Login to the OpenShift prod-cluster with `oc`.

Forward port from the openserach proxy to you local machine:
`oc port-forward -n historical-prod svc/proxy-historical-os 8080:8080`
Keep the command running as long as you use the tunnel.

*Note:* Do not try to set up route or ingress to the service, that will open security issues.

From a browser go to `http://localhost:8080/_dashboards`.
It is okej to use http since the traffic between you and the service is in an encrypted tunnel

Login with your OpenSearch user.

If you want to run an app towards OpenSearch, use the URL `http://localhost:8080/`.

Once you are done with the tunnel, press `^C` to interrupt `oc port-forward`.
