# coding=utf-8
import pytest
from common.ad_formatter import format_hits_with_only_original_values


def test_format_hits_with_only_original_values_formats_contact_persons():
    # Arrange
    hits_contacts = [
        {
            "id": 123,
            "application_contacts": [
                {
                    "name": "Testy Testsson",
                    "description": "blabla",
                    "email": "test@jobtechdev.se",
                    "telephone": "+01011122233",
                    "contactType": None,
                }
            ],
        },
        {
            "id": 456,
            "application_contacts": [
                {
                    "name": "Testy Testsson",
                    "description": "blabla",
                    "email": "test@jobtechdev.se",
                    "telephone": "+01011122233",
                    "contactType": None,
                }
            ],
        },
    ]
    expected = [
        {
            "id": 123,
            "application_contacts": [
                {
                    "name": "Testy Testsson",
                    "description": "blabla",
                    "email": "test@jobtechdev.se",
                    "telephone": "+01011122233",
                    "contactType": None,
                }
            ],
        },
        {
            "id": 456,
            "application_contacts": [
                {
                    "name": "Testy Testsson",
                    "description": "blabla",
                    "email": "test@jobtechdev.se",
                    "telephone": "+01011122233",
                    "contactType": None,
                }
            ],
        },
    ]

    # Act
    result_contacts = format_hits_with_only_original_values(hits_contacts)

    # Assert
    assert result_contacts == expected


@pytest.mark.parametrize(
    "value",
    [
        [{"something": 123}, {"something_else": "abc"}],
        [{"id": 123, "application_contacts": [None]}],
        [{"occupation": [{"concept_id": "123", "label": "Engineer", "legacy_ams_taxonomy_id": "1"}]}],
        [{"must_have": {"skills": []}}, {"nice_to_have": {"skills": [{"concept_id": "1", "label": "Python"}]}}],
    ],
)
def test_format_hits_with_only_original_values_no_change(value):
    assert format_hits_with_only_original_values(value) == value


@pytest.mark.parametrize("value", [None, False, 0])
def test_format_hits_with_only_original_values_type_error(value):
    with pytest.raises(TypeError):
        format_hits_with_only_original_values(value)


@pytest.mark.parametrize("value", [None, False, 0, "some_string"])
def test_format_hits_with_only_original_values_attribute_error(value):
    with pytest.raises(AttributeError):
        format_hits_with_only_original_values([value])


@pytest.mark.parametrize("field", ["occupation_group", "occupation_field"])
@pytest.mark.parametrize(
    "data, expected",
    [
        (["some group"], "some group"),
        (["some group", "some_other_group"], "some group"),
        ([False], False),
        (False, False),
        ([True, False], True),
        ("not_a_list", "not_a_list"),
        ({"my_key": "value"}, {"my_key": "value"}),
    ],
)
def test_format_hits_with_only_original_values_occupation_group_field(data, expected, field):
    # Arrange
    hits = [{field: data}]

    # Act
    result = format_hits_with_only_original_values(hits)

    # Assert
    assert result == [{field: expected}]


@pytest.mark.parametrize(
    "skill_type, data, expected_skills",
    [
        (
            "must_have",
            {"skills": [{"concept_id": "1", "label": "Python", "original_value": True}]},
            [{"concept_id": "1", "label": "Python", "legacy_ams_taxonomy_id": None}]
        ),
        (
            "nice_to_have",
            {"skills": [{"concept_id": "2", "label": "Java", "original_value": True}]},
            [{"concept_id": "2", "label": "Java", "legacy_ams_taxonomy_id": None}]
        ),
    ],
)
def test_format_hits_with_original_value_skills(skill_type, data, expected_skills):
    # Arrange
    hits = [{skill_type: data}]

    # Act
    result = format_hits_with_only_original_values(hits)

    # Assert
    expected_hits = [{skill_type: {"skills": data["skills"], "skill": expected_skills}}]
    assert result == expected_hits


@pytest.mark.parametrize(
    "skill_type, data, expected_result",
    [
        (
            "must_have",
            {"skills": [{"concept_id": "3", "label": "C++"}]},
            [{"must_have": {"skills": [{"concept_id": "3", "label": "C++"}], "skill": []}}]  # Expect empty 'skill'
        ),
        (
            "nice_to_have",
            {"skills": []},
            [{"nice_to_have": {"skills": []}}]  # Do not expect 'skill' if it's empty
        ),
    ],
)
def test_format_hits_without_original_value_skills(skill_type, data, expected_result):
    # Arrange
    hits = [{skill_type: data}]

    # Act
    result = format_hits_with_only_original_values(hits)

    # Assert
    assert result == expected_result


def test_format_hits_with_only_original_values_employment_type_original_value():
    # Arrange
    hits = [
        {
            "ad": "1",
            "employment_type": [
                {
                    "concept_id": "Jh8f_q9J_pbJ",
                    "label": "Sommarjobb / feriejobb",
                    "legacy_ams_taxonomy_id": "2",
                    "original_value": True,
                },
                {"concept_id": "EBhX_Qm2_8eX", "label": "Säsongsanställning", "legacy_ams_taxonomy_id": None},
            ],
        }
    ]

    # Act
    result = format_hits_with_only_original_values(hits)

    # Assert
    assert result == [
        {
            "ad": "1",
            "employment_type": {
                "concept_id": "Jh8f_q9J_pbJ",
                "label": "Sommarjobb / feriejobb",
                "legacy_ams_taxonomy_id": "2",
            },
        }
    ]


def test_format_hits_with_only_original_values_employment_type_original_value_false():
    # Arrange
    hits = [
        {
            "ad": "1",
            "employment_type": [
                {
                    "concept_id": "Jh8f_q9J_pbJ",
                    "label": "Sommarjobb / feriejobb",
                    "legacy_ams_taxonomy_id": "2",
                    "original_value": False,
                },
                {"concept_id": "EBhX_Qm2_8eX", "label": "Säsongsanställning", "legacy_ams_taxonomy_id": None},
            ],
        }
    ]

    # Act
    result = format_hits_with_only_original_values(hits)

    # Assert
    assert result == [{"ad": "1", "employment_type": None}]


def test_format_hits_with_only_original_values_remove_contacts_if_null():
    # Arrange
    hits = [
        {"id": 789, "application_contacts": [{"name": None, "email": None, "telephone": None, "contactType": None}]},
        {
            "id": 101,
            "application_contacts": [{"name": "John Doe", "email": None, "telephone": None, "contactType": None}],
        },
    ]

    # Act
    result = format_hits_with_only_original_values(hits)

    # Assert
    assert result == [
        {"id": 789, "application_contacts": []},
        {
            "id": 101,
            "application_contacts": [{"name": "John Doe", "email": None, "telephone": None, "contactType": None}],
        },
    ]
