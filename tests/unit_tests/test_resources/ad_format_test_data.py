# coding=utf-8

ad_before = {
    "id": 24681267,
    "external_id": "46-556386-7398-1130699-2",
    "headline": "Frösunda Personlig assistans söker en kundstödjande jurist",
    "application_deadline": "2021-04-30T23:59:59",
    "number_of_vacancies": 1,
    "remote_work": False,
    "open_for_all": False,
    "trainee": False,
    "larling": False,
    "franchise": False,
    "hire_work_place": False,
    "description": {
        "text": "Om Frösunda Personlig assistans",
        "text_formatted": "Om Frösunda Personlig assistans",
        "company_information": None,
        "needs": None,
        "requirements": None,
        "conditions": "Heltid\r\nAnställningstid enligt överenskommelse"
    },
    "detected_language": "sv",
    "employment_type": [
        {
            "concept_id": "PFZr_Syz_cUq",
            "label": "Vanlig anställning",
            "legacy_ams_taxonomy_id": "1",
            "original_value": True
        },
        {
            "concept_id": "kpPX_CNN_gDU",
            "label": "Tillsvidareanställning (inkl. eventuell provanställning)",
            "legacy_ams_taxonomy_id": None
        }
    ],
    "salary_type": {
        "concept_id": "oG8G_9cW_nRf",
        "label": "Fast månads- vecko- eller timlön",
        "legacy_ams_taxonomy_id": "1"
    },
    "salary_description": "Lön enligt överenskommelse",
    "duration": {
        "concept_id": "a7uU_j21_mkL",
        "label": "Tills vidare",
        "legacy_ams_taxonomy_id": "1"
    },
    "working_hours_type": {
        "concept_id": "6YE1_gAC_R2G",
        "label": "Heltid",
        "legacy_ams_taxonomy_id": "1"
    },
    "scope_of_work": {
        "min": 100,
        "max": 100
    },
    "access": None,
    "employer": {
        "phone_number": None,
        "email": None,
        "url": "http://frosunda.se/personligassistans",
        "organization_number": "5563867398",
        "name": "Frösunda Personlig Assistans AB",
        "workplace": "Frösunda Personlig assistans",
        "workplace_id": "0"
    },
    "application_details": {
        "information": None,
        "reference": None,
        "email": None,
        "via_af": False,
        "url": "https://frosundapersonligassistans.teamtailor.com/jobs/1130699-frosunda-personlig-assistans-soker-en-kundstodjande-jurist?promotion=223993-arbetsformedlingen",
        "other": None
    },
    "experience_required": True,
    "access_to_own_car": False,
    "driving_license_required": False,
    "occupation": [
        {
            "concept_id": "YHB5_wmX_UCt",
            "label": "Juridisk ombudsman",
            "legacy_ams_taxonomy_id": "4424",
            "original_value": True
        },
        {
            "concept_id": "7YVF_kE4_aPm",
            "label": "Ombudsman",
            "legacy_ams_taxonomy_id": "5775"
        }
    ],
    "occupation_group": [
        {
            "concept_id": "vPP6_rsw_dck",
            "label": "Planerare och utredare m.fl.",
            "legacy_ams_taxonomy_id": "2422"
        }
    ],
    "occupation_field": [
        {
            "concept_id": "X82t_awd_Qyc",
            "label": "Administration, ekonomi, juridik",
            "legacy_ams_taxonomy_id": "1"
        }
    ],
    "collections": [],
    "workplace_address": {
        "municipality_code": None,
        "municipality_concept_id": None,
        "municipality": None,
        "region_code": None,
        "region_concept_id": None,
        "region": None,
        "country_code": "199",
        "country_concept_id": "i46j_HmG_v64",
        "country": "Sverige",
        "street_address": None,
        "postcode": None,
        "city": None,
        "coordinates": [
            None,
            None
        ]
    },
    "must_have": {
        "skills": [],
        "languages": [],
        "work_experiences": [
            {
                "concept_id": "YHB5_wmX_UCt",
                "label": "Juridisk ombudsman",
                "weight": 10,
                "legacy_ams_taxonomy_id": "4424"
            }
        ],
        "education": [],
        "education_level": []
    },
    "nice_to_have": {
        "skills": [],
        "languages": [],
        "work_experiences": [],
        "education": [],
        "education_level": []
    },
    "application_contacts": [
        {
            "name": "Testy Testsson",
            "description": "blabla",
            "email": "test@jobtechdev.se",
            "telephone": "+01011122233",
            "contactType": None
        }
    ],
    "publication_date": "2021-04-06T16:05:16",
    "last_publication_date": "2023-03-14T13:44:17",
    "removed": False,
    "removed_date": None,
    "source_type": "VIA_PLATSBANKEN_DXA",
    "timestamp": 1617717916553,
    "logo_url": None,
    "keywords": {
        "extracted": {
            "occupation": [
                "ekonomi",
                "juridisk ombudsman",
                "administration",
                "utredare",
                "juridik",
                "ombudsman",
                "planerare"
            ],
            "skill": [],
            "location": [
                "sverige"
            ],
            "employer": [
                "frösunda personlig assistans"
            ]
        },
        "enriched": {
            "occupation": [
                "ombudsman",
                "jurist",
                "juridisk ombudsman"
            ],
            "skill": [
                "social omsorg",
                "socialtjänstlagen",
                "svenska",
                "resor",
                "lss",
                "körkort",
                "högskola"
            ],
            "trait": [
                "utåtriktad",
                "lyhörd",
                "analytisk",
                "omdöme",
                "varm",
                "nyfikenhet",
                "noggrann",
                "empatisk"
            ],
            "location": [
                "linköping",
                "sverige",
                "frösunda",
                "mjölby"
            ],
            "compound": [
                "ombudsman",
                "jurist",
                "juridisk ombudsman",
                "social omsorg",
                "socialtjänstlagen",
                "svenska",
                "resor",
                "lss",
                "körkort",
                "högskola",
                "linköping",
                "sverige",
                "frösunda",
                "mjölby"
            ]
        },
        "enriched_typeahead_terms": {
            "occupation": [
                "ombudsman",
                "jurist",
                "juridisk ombudsman"
            ],
            "skill": [
                "social omsorg",
                "socialtjänstlagen",
                "svenska",
                "resor",
                "lss",
                "körkort",
                "högskola"
            ],
            "trait": [
                "analytisk förmåga",
                "utåtriktad",
                "lyhörd",
                "analytisk",
                "omdöme",
                "varm",
                "nyfikenhet",
                "gott omdöme",
                "lyhördhet",
                "noggrann",
                "empatisk"
            ],
            "location": [
                "linköping",
                "sverige",
                "frösunda",
                "mjölby"
            ],
            "compound": [
                "ombudsman",
                "jurist",
                "juridisk ombudsman",
                "social omsorg",
                "socialtjänstlagen",
                "svenska",
                "resor",
                "lss",
                "körkort",
                "högskola",
                "linköping",
                "sverige",
                "frösunda",
                "mjölby"
            ]
        }
    },
    "relevance": 0.0
}

ad_after_formatting = {
    "id": 24681267,
    "external_id": "46-556386-7398-1130699-2",
    "headline": "Frösunda Personlig assistans söker en kundstödjande jurist",
    "application_deadline": "2021-04-30T23:59:59",
    "number_of_vacancies": 1,
    "remote_work": False,
    "open_for_all": False,
    "trainee": False,
    "larling": False,
    "franchise": False,
    "hire_work_place": False,
    "description": {
        "text": "Om Frösunda Personlig assistans",
        "text_formatted": "Om Frösunda Personlig assistans",
        "company_information": None,
        "needs": None,
        "requirements": None,
        "conditions": "Heltid\r\nAnställningstid enligt överenskommelse"
    },
    "detected_language": "sv",
    "employment_type": {
        "concept_id": "PFZr_Syz_cUq",
        "label": "Vanlig anställning",
        "legacy_ams_taxonomy_id": "1"
    },
    "salary_type": {
        "concept_id": "oG8G_9cW_nRf",
        "label": "Fast månads- vecko- eller timlön",
        "legacy_ams_taxonomy_id": "1"
    },
    "salary_description": "Lön enligt överenskommelse",
    "duration": {
        "concept_id": "a7uU_j21_mkL",
        "label": "Tills vidare",
        "legacy_ams_taxonomy_id": "1"
    },
    "working_hours_type": {
        "concept_id": "6YE1_gAC_R2G",
        "label": "Heltid",
        "legacy_ams_taxonomy_id": "1"
    },
    "scope_of_work": {
        "min": 100,
        "max": 100
    },
    "access": None,
    "employer": {
        "phone_number": None,
        "email": None,
        "url": "http://frosunda.se/personligassistans",
        "organization_number": "5563867398",
        "name": "Frösunda Personlig Assistans AB",
        "workplace": "Frösunda Personlig assistans",
        "workplace_id": "0"
    },
    "application_details": {
        "information": None,
        "reference": None,
        "email": None,
        "via_af": False,
        "url": "https://frosundapersonligassistans.teamtailor.com/jobs/1130699-frosunda-personlig-assistans-soker-en-kundstodjande-jurist?promotion=223993-arbetsformedlingen",
        "other": None
    },
    "experience_required": True,
    "access_to_own_car": False,
    "driving_license_required": False,
    "occupation": {
        "concept_id": "YHB5_wmX_UCt",
        "label": "Juridisk ombudsman",
        "legacy_ams_taxonomy_id": "4424"
    },
    "occupation_group": {
        "concept_id": "vPP6_rsw_dck",
        "label": "Planerare och utredare m.fl.",
        "legacy_ams_taxonomy_id": "2422"
    },
    "occupation_field": {
        "concept_id": "X82t_awd_Qyc",
        "label": "Administration, ekonomi, juridik",
        "legacy_ams_taxonomy_id": "1"
    },
    "collections": [],
    "workplace_address": {
        "municipality_code": None,
        "municipality_concept_id": None,
        "municipality": None,
        "region_code": None,
        "region_concept_id": None,
        "region": None,
        "country_code": "199",
        "country_concept_id": "i46j_HmG_v64",
        "country": "Sverige",
        "street_address": None,
        "postcode": None,
        "city": None,
        "coordinates": [
            None,
            None
        ]
    },
    "must_have": {
        "skills": [],
        "languages": [],
        "work_experiences": [
            {
                "concept_id": "YHB5_wmX_UCt",
                "label": "Juridisk ombudsman",
                "weight": 10,
                "legacy_ams_taxonomy_id": "4424"
            }
        ],
        "education": [],
        "education_level": []
    },
    "nice_to_have": {
        "skills": [],
        "languages": [],
        "work_experiences": [],
        "education": [],
        "education_level": []
    },
    "application_contacts": [
        {
            "name": "Testy Testsson",
            "description": "blabla",
            "email": "test@jobtechdev.se",
            "telephone": "+01011122233",
            "contactType": None
        }
    ],
    "publication_date": "2021-04-06T16:05:16",
    "last_publication_date": "2023-03-14T13:44:17",
    "removed": False,
    "removed_date": None,
    "source_type": "VIA_PLATSBANKEN_DXA",
    "timestamp": 1617717916553,
    "logo_url": None,
    "keywords": {
        "extracted": {
            "occupation": [
                "ekonomi",
                "juridisk ombudsman",
                "administration",
                "utredare",
                "juridik",
                "ombudsman",
                "planerare"
            ],
            "skill": [],
            "location": [
                "sverige"
            ],
            "employer": [
                "frösunda personlig assistans"
            ]
        },
        "enriched": {
            "occupation": [
                "ombudsman",
                "jurist",
                "juridisk ombudsman"
            ],
            "skill": [
                "social omsorg",
                "socialtjänstlagen",
                "svenska",
                "resor",
                "lss",
                "körkort",
                "högskola"
            ],
            "trait": [
                "utåtriktad",
                "lyhörd",
                "analytisk",
                "omdöme",
                "varm",
                "nyfikenhet",
                "noggrann",
                "empatisk"
            ],
            "location": [
                "linköping",
                "sverige",
                "frösunda",
                "mjölby"
            ],
            "compound": [
                "ombudsman",
                "jurist",
                "juridisk ombudsman",
                "social omsorg",
                "socialtjänstlagen",
                "svenska",
                "resor",
                "lss",
                "körkort",
                "högskola",
                "linköping",
                "sverige",
                "frösunda",
                "mjölby"
            ]
        },
        "enriched_typeahead_terms": {
            "occupation": [
                "ombudsman",
                "jurist",
                "juridisk ombudsman"
            ],
            "skill": [
                "social omsorg",
                "socialtjänstlagen",
                "svenska",
                "resor",
                "lss",
                "körkort",
                "högskola"
            ],
            "trait": [
                "analytisk förmåga",
                "utåtriktad",
                "lyhörd",
                "analytisk",
                "omdöme",
                "varm",
                "nyfikenhet",
                "gott omdöme",
                "lyhördhet",
                "noggrann",
                "empatisk"
            ],
            "location": [
                "linköping",
                "sverige",
                "frösunda",
                "mjölby"
            ],
            "compound": [
                "ombudsman",
                "jurist",
                "juridisk ombudsman",
                "social omsorg",
                "socialtjänstlagen",
                "svenska",
                "resor",
                "lss",
                "körkort",
                "högskola",
                "linköping",
                "sverige",
                "frösunda",
                "mjölby"
            ]
        }
    },
    "relevance": 0.0
}
