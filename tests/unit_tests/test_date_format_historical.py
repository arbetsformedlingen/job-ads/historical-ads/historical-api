import pytest
from datetime import datetime, timedelta
from common.date_and_time_handling import format_start_date, format_end_date
from common import constants


@pytest.mark.parametrize('start_date, expected', [
    ('2020', '2020-01-01'),
    ('2020-01', '2020-01-01'),
    ('2020-09', '2020-09-01'),
    ('2020-12', '2020-12-01'),
    ('2020-02', '2020-02-01'),
    ('2020-02-29', '2020-02-29'),
    ('2020-02-28', '2020-02-28')
])
def test_start_date(start_date, expected):
    """
    Check that incomplete dates are returned as full timestamps incl hh:mm:ss
    'T00:00:00' is omitted from the 'expected' part of the test cases for readability
    """
    # Act
    result = format_start_date(start_date)

    # Assert
    assert result == f"{expected}T00:00:00"


@pytest.mark.parametrize('end_date, expected', [
    ('2020', '2020-12-31'),
    ('2020-01', '2020-01-31'),
    ('2020-09', '2020-09-30'),
    ('2020-12', '2020-12-31'),
    ('2020-02', '2020-02-29'),
    ('2021-02', '2021-02-28'),
    ('2021-02-27', '2021-02-27'),
])
def test_end_date(end_date, expected):
    """
    Check that incomplete dates are returned as datetime objects
    'T23:59:59' is omitted from the 'expected' part of the test cases for readability
    """
    # Act
    result = format_end_date(end_date)

    # Assert
    assert result == f"{expected}T23:59:59"


def test_start_date_none():
    """
    Ensure the start date returns None if no date is provided.
    """
    assert format_start_date(None) is None


def test_end_date_none():
    """
    Ensure the end date has a default value to prevent the query from
    returning ads with future dates.
    """
    # Arrange
    dt = datetime.now() - timedelta(days=1)
    default_end_date = dt.replace(hour=23, minute=59, second=59, microsecond=0)

    # Act
    result = format_end_date(None)

    # Assert
    assert result == default_end_date.strftime(constants.DATE_FORMAT)


def test_future_end_date():
    """
    Check that a future date is capped to the current default end date
    (which is the day before the current day).
    """
    # Arrange
    future_date = (datetime.now() + timedelta(days=10)).strftime('%Y-%m-%d')
    dt = datetime.now() - timedelta(days=1)
    default_end_date = dt.replace(hour=23, minute=59, second=59, microsecond=0)

    # Act
    result = format_end_date(future_date)

    # Assert
    assert result == default_end_date.strftime(constants.DATE_FORMAT)


@pytest.mark.parametrize('end_date, expected', [
    ('2020-12-31', '2020-12-31T23:59:59'),  # Last day of the year
    ('2021-01-01', '2021-01-01T23:59:59'),  # First day of the year
])
def test_end_date_boundary_conditions(end_date, expected):
    """
    Check that boundary conditions like the last day of the year or
    first day of the year are handled correctly.
    """
    # Act
    result = format_end_date(end_date)

    # Assert
    assert result == expected


@pytest.mark.parametrize('end_date, expected', [
    ('2019-02', '2019-02-28T23:59:59'),  # Non-leap year February
    ('2024-02', '2024-02-29T23:59:59'),  # Leap year February
])
def test_february_dates(end_date, expected):
    """
    Test specific February dates to ensure leap year logic is correct.
    """
    # Act
    result = format_end_date(end_date)

    # Assert
    assert result == expected


@pytest.mark.parametrize('wrong_date', [
    '20201',
    '2021-09-31',
    '20-20',
    '2024-13',
])
def test_wrong_start_date(wrong_date):
    """
    Check that wrong dates raise a ValueError in the start date formatting.
    """
    with pytest.raises(ValueError):
        format_start_date(wrong_date)


@pytest.mark.parametrize('wrong_date', [
    '20201',
    '2021-09-31',
    '20-20',
    '2024-13',
])
def test_wrong_end_date(wrong_date):
    """
    Check that wrong dates raise a ValueError in the end date formatting.
    """
    with pytest.raises(ValueError):
        format_end_date(wrong_date)


@pytest.mark.parametrize('wrong_date', [
    '2020-12-31T25:00:00',  # Invalid hour
    '2020-12-31T23:61:00',  # Invalid minute
    '2020-12-31T23:59:61',  # Invalid second
])
def test_invalid_time_parts(wrong_date):
    """
    Check that invalid time parts in the date string raise a ValueError.
    """
    with pytest.raises(ValueError):
        format_end_date(wrong_date)