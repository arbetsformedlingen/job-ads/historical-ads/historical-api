import pytest  # noqa: F401
from unittest.mock import patch

from search.rest.endpoint.historical import AdById


def test_AdById_exists():
    # Act
    endpoint = AdById()

    # Assert
    assert endpoint


@patch("search.rest.endpoint.historical.format_hits_with_only_original_values")
@patch("search.rest.endpoint.historical.fetch_ad_by_id")
def test_AdById_get_endpoint(mock_fetch_ad_by_id, mock_format_hits_with_only_original_values):
    # Arrange
    mock_fetch_ad_by_id.return_value = "some-mock-ad"
    mock_format_hits_with_only_original_values.return_value = [{"id": "fake-id", "headline": "some-mock-result"}]

    # Act
    endpoint = AdById()
    response = endpoint.get("some-test-id")

    # Assert
    mock_fetch_ad_by_id.assert_called_once_with("some-test-id", historical=True)
    mock_format_hits_with_only_original_values.assert_called_once_with(["some-mock-ad"])

    # Being a bit cheap here and not checking the entire response.
    assert response["id"] == "fake-id"
    assert response["headline"] == "some-mock-result"
