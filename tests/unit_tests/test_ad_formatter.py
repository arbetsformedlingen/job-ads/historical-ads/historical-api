import pytest
from common.ad_formatter import format_one_ad, format_hits_with_only_original_values
from tests.unit_tests.test_resources.ad_format_test_data import ad_before, ad_after_formatting

# Test data
ad_empty = {}
ad_partial = {
    'occupation': [{'original_value': True, 'concept_id': '123', 'label': 'Engineer', 'legacy_ams_taxonomy_id': '1'}],
    'must_have': {'skills': [{'concept_id': '456', 'label': 'Python', 'original_value': True}]}
}
ad_no_original_value = {
    'occupation': [{'original_value': False, 'concept_id': '789', 'label': 'Developer', 'legacy_ams_taxonomy_id': '2'}],
    'must_have': {'skills': [{'concept_id': '456', 'label': 'JavaScript'}]}
}


def test_full_ad():
    # Act
    formatted = format_one_ad(ad_before)

    # Assert
    assert formatted == ad_after_formatting


def test_none():
    # Test formatting with None as input
    with pytest.raises(AttributeError):
        format_one_ad(None)


def test_empty_ad():
    # Test formatting with an empty ad
    result = format_one_ad(ad_empty)

    # Assert
    assert result == ad_empty  # Should return the same empty dictionary


def test_partial_ad():
    # Test formatting with partial ad data
    formatted = format_one_ad(ad_partial)

    # Assert
    assert formatted['occupation']['concept_id'] == '123'
    assert formatted['must_have']['skill'][0]['concept_id'] == '456'


def test_ad_without_original_value():
    # Test formatting where no original value exists
    formatted = format_one_ad(ad_no_original_value)

    # Assert
    assert formatted['occupation'] is None  # No original value should return None
    assert formatted['must_have']['skill'] == []  # No original value should return an empty list


def test_ad_with_none_values():
    # Test formatting where ad has None values
    with pytest.raises(AttributeError):
        format_one_ad(None)


def test_format_hits_with_only_original_values():
    # Test the format_hits_with_only_original_values function
    # Arrange
    hits = [ad_before, ad_partial, ad_no_original_value]

    # Act
    formatted_hits = format_hits_with_only_original_values(hits)

    # Assert
    assert len(formatted_hits) == 3
    assert formatted_hits[0] == ad_after_formatting  # Compare with pre-formatted expected output
    assert formatted_hits[1]['occupation']['concept_id'] == '123'  # Check occupation value for partial ad
    assert formatted_hits[2]['occupation'] is None  # No original value should result in None


def test_must_have_with_empty_skills():
    ad = {'must_have': {'skills': []}}
    formatted = format_one_ad(ad)

    # Ensure that 'skill' is not added if the 'skills' list is empty
    assert 'skill' not in formatted['must_have']


def test_must_have_with_none_skills():
    ad = {'must_have': {'skills': None}}
    formatted = format_one_ad(ad)
    assert 'skill' not in formatted['must_have']  # 'skill' should not be added when skills is None


def test_nice_to_have_with_empty_skills():
    ad = {'nice_to_have': {'skills': []}}
    formatted = format_one_ad(ad)
    assert 'skill' not in formatted['nice_to_have']


def test_nice_to_have_with_none_skills():
    ad = {'nice_to_have': {'skills': None}}
    formatted = format_one_ad(ad)
    assert 'skill' not in formatted['nice_to_have']  # 'skill' should not be added when skills is None


def test_missing_must_have_section():
    ad = {'occupation': [{'original_value': True, 'concept_id': '123', 'label': 'Engineer'}]}
    formatted = format_one_ad(ad)
    assert 'must_have' not in formatted  # Ensure 'must_have' is not added


def test_missing_nice_to_have_section():
    ad = {'occupation': [{'original_value': True, 'concept_id': '123', 'label': 'Engineer'}]}
    formatted = format_one_ad(ad)
    assert 'nice_to_have' not in formatted  # Ensure 'nice_to_have' is not added


def test_invalid_skills_data_structure():
    ad = {'must_have': {'skills': 'not_a_list'}}
    with pytest.raises(AttributeError):
        format_one_ad(ad)  # Should raise error as it expects a list for 'skills'


def test_invalid_occupation_data_structure():
    ad = {'occupation': 'not_a_list'}
    with pytest.raises(AttributeError):
        format_one_ad(ad)  # Should raise error as it expects a list for 'occupation'


def test_invalid_occupation_data_structure_error_message():
    ad = {'occupation': 'not_a_list'}
    with pytest.raises(AttributeError, match=".*'str' object has no attribute 'get'.*"):
        format_one_ad(ad)  # Should raise error and match the expected error message


def test_missing_occupation_group():
    ad = {'occupation': [{'original_value': True, 'concept_id': '123', 'label': 'Engineer'}]}
    formatted = format_one_ad(ad)
    assert 'occupation_group' not in formatted  # Should not add 'occupation_group' if not present


def test_combined_ad():
    ad = {
        'occupation': [{'original_value': True, 'concept_id': '123', 'label': 'Engineer'}],
        'must_have': {'skills': [{'concept_id': '456', 'label': 'Python', 'original_value': True}]},
        'nice_to_have': {'skills': [{'concept_id': '789', 'label': 'JavaScript'}]},
        'occupation_group': ['Software Development'],
        'application_contacts': [{'name': 'Jane Doe', 'email': None}]
    }
    formatted = format_one_ad(ad)

    assert formatted['occupation']['concept_id'] == '123'
    assert formatted['must_have']['skill'][0]['concept_id'] == '456'
    assert formatted['occupation_group'] == 'Software Development'


@pytest.mark.parametrize("ad_data,expected_occupation,expected_skills", [
    (ad_before, ad_after_formatting['occupation'], ad_after_formatting['must_have'].get('skill', [])),
    (ad_partial, {'concept_id': '123', 'label': 'Engineer', 'legacy_ams_taxonomy_id': '1'}, 
                 [{'concept_id': '456', 'label': 'Python', 'legacy_ams_taxonomy_id': None}]),
    (ad_no_original_value, None, [])
])
def test_format_one_ad(ad_data, expected_occupation, expected_skills):
    formatted = format_one_ad(ad_data)

    # Check if occupation is None or matches the entire dictionary
    if expected_occupation is None:
        assert formatted['occupation'] is None
    else:
        assert formatted['occupation'] == expected_occupation

    # Check if 'must_have' and 'skill' exist before asserting
    if expected_skills == []:
        assert 'skill' not in formatted['must_have'] or formatted['must_have']['skill'] == []
    else:
        assert formatted['must_have']['skill'] == expected_skills