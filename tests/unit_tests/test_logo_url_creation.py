import pytest
from search.common_search.logo import logo_url_creator, format_base_url


def test_workplace_id():
    ad = {'employer': {'workplace_id': 123456, 'organization_number': '01234567890'}}
    logo_url = logo_url_creator(ad)
    assert logo_url == 'https://www.arbetsformedlingen.se/rest/arbetsgivare/rest/af/v3/arbetsplatser/123456/logotyper/logo.png'
    print()


def test_org_nr():
    ad = {'employer': {'organization_number': '01234567890'}}
    logo_url = logo_url_creator(ad)
    assert logo_url == 'https://www.arbetsformedlingen.se/rest/arbetsgivare/rest/af/v3/organisation/01234567890/logotyper/logo.png'


@pytest.mark.parametrize("ad",
                         [
                             {'employer': {'no info': True}},
                             {'no-employer': True},
                             None,
                             "ad",
                             1,
                             [{"ad-in-list": True}]
                         ])
def test_not_a_valid_ad(ad):
    logo_url = logo_url_creator(ad)
    assert logo_url is None


@pytest.mark.parametrize("url", [
    "http/",
    "http"
])
def test_base_url_formatter(url):
    formatted_url = format_base_url(url)
    assert formatted_url == "http"

