import pytest
from common.ad_formatter import get_original_value_from_list, _keep_multiple_original_values_in_list


@pytest.mark.parametrize(
    "ad, expected",
    [
        ({"occupation": []}, {"concept_id": None, "label": None, "legacy_ams_taxonomy_id": None}),  # Empty list case
        (
            {"occupation": [{"concept_id": "123", "label": "Test Label", "legacy_ams_taxonomy_id": "999"}]},
            None,
        ),  # No original_value
        (
            {
                "occupation": {
                    "concept_id": "YHB5_wmX_UCt",
                    "label": "Juridisk ombudsman",
                    "legacy_ams_taxonomy_id": "4424",
                    "original_value": True,
                }
            },
            {"concept_id": "YHB5_wmX_UCt", "label": "Juridisk ombudsman", "legacy_ams_taxonomy_id": "4424"},
        ),  # Single dict case
        (
            {
                "occupation": [
                    {
                        "concept_id": "YHB5_wmX_UCt",
                        "label": "Juridisk ombudsman",
                        "legacy_ams_taxonomy_id": "4424",
                        "original_value": False,
                    }
                ]
            },
            None,
        ),  # No original_value True
    ],
)
def test_get_original_value_from_list(ad, expected):
    # Act
    result = get_original_value_from_list(ad["occupation"])

    # Assert
    assert result == expected


@pytest.mark.parametrize(
    "values, expected",
    [
        ([], []),  # Empty list
        (
            [{"concept_id": None, "label": "Test Label", "legacy_ams_taxonomy_id": "999", "original_value": True}],
            [{"concept_id": None, "label": "Test Label", "legacy_ams_taxonomy_id": "999"}],
        ),  # None concept_id
        (
            [{"concept_id": "123", "label": "Test Label", "legacy_ams_taxonomy_id": "999", "original_value": False}],
            [],
        ),  # No original_value True
        (
            [{"label": "Test Label", "legacy_ams_taxonomy_id": "999", "original_value": True}],
            [{"concept_id": None, "label": "Test Label", "legacy_ams_taxonomy_id": "999"}],
        ),  # Missing concept_id
        ([{"something_else": True}], []),  # Invalid dictionary structure
    ],
)
def test_keep_multiple_original_values_in_list(values, expected):
    # Act
    result = _keep_multiple_original_values_in_list(values)

    # Assert
    assert result == expected


@pytest.mark.parametrize("value", [None, False, 0, 1, "some_string", [None]])
def test_type_error(value):
    with pytest.raises((TypeError, AttributeError)):
        _keep_multiple_original_values_in_list(value)


def test_no_valid_keys():
    # Arrange
    hits = [{"my_key": True}, {"my_key": False}]

    # Act
    result = _keep_multiple_original_values_in_list(hits)

    # Assert
    assert result == []


@pytest.mark.parametrize("value", [None, False, 0, 1, "some_string"])
def test_attribute_error(value):
    with pytest.raises(AttributeError):
        _keep_multiple_original_values_in_list([value])
