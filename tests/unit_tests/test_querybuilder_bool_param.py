import pytest
from search.common_search.querybuilder import QueryBuilder


@pytest.mark.parametrize("bool_param", [True, False])
@pytest.mark.parametrize("query_term", ['remote_work', 'open_for_all', 'trainee', 'larling', 'franchise', None, 123])
def test_generic_bool(bool_param, query_term):
    expected = ({'bool': {'must': {'term': {query_term: bool_param}}}})
    query_result = QueryBuilder._build_generic_bool_query(bool_param, query_term)
    assert query_result == expected


def test_generic_bool_none():
    query_result = QueryBuilder._build_generic_bool_query(None, query_term='remote_work')
    assert query_result is None
