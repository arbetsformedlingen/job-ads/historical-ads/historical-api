import pytest  # noqa: F401
from unittest.mock import patch

from search.common_search.taxonomy_municipalities import TaxonomyMunicipalities


def test_TaxonomyMunicipalities_object_init():
    # Act
    tm = TaxonomyMunicipalities()

    # Assert
    assert tm


@patch("search.common_search.taxonomy_lookup.settings.TAXONOMY_VERSION", 21)
@patch("search.common_search.taxonomy_municipalities.log")
@patch("search.common_search.taxonomy_municipalities.TaxonomyLookup")
def test_TaxonomyMunicipalities_fetch_municipalities_from_taxonomy_returns(MockTaxonomyLookup, mock_log):
    # Arrange
    MockTaxonomyLookup.return_value.fetch_taxonomy_values.return_value = [{"id": "test-id"}]
    tm = TaxonomyMunicipalities()

    # Act
    tax_municipalities = tm._fetch_municipalities_from_taxonomy()

    # Assert
    MockTaxonomyLookup.return_value.fetch_taxonomy_values.assert_called_once_with(
        'query municipality {\n  concepts(type: "municipality", include_deprecated: true, version: 21) {\n    id\n    preferred_label\n    deprecated_legacy_id\n    lau_2_code_2015\n    broader{\n      id\n      preferred_label\n      deprecated_legacy_id\n      national_nuts_level_3_code_2019\n    }\n  }\n  }\n}\n'
    )

    mock_log.info.assert_called_once_with("Fetched 1 municipalities from taxonomy")
    assert tax_municipalities == [{"id": "test-id"}]


@patch("search.common_search.taxonomy_municipalities.TaxonomyLookup")
def test_TaxonomyMunicipalities_get_municipality_by_concept_id_returns_matching_id(MockTaxonomyLookup):
    # Arrange
    MockTaxonomyLookup.return_value.fetch_taxonomy_values.return_value = [{"id": "test-id-abc"}, {"id": "test-id-def"}]
    tm = TaxonomyMunicipalities()

    # Act
    municipality = tm.get_municipality_by_concept_id("test-id-abc")

    # Assert
    assert municipality == {"id": "test-id-abc"}


@patch("search.common_search.taxonomy_municipalities.TaxonomyLookup")
def test_TaxonomyMunicipalities_get_municipality_by_concept_id_returns_none_without_match(MockTaxonomyLookup):
    # Arrange
    MockTaxonomyLookup.return_value.fetch_taxonomy_values.return_value = [{"id": "test-id-abc"}, {"id": "test-id-def"}]
    tm = TaxonomyMunicipalities()

    # Act
    municipality = tm.get_municipality_by_concept_id("test-id-ghi")

    # Assert
    assert municipality is None
