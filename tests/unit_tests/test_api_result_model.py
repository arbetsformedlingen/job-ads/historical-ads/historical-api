from unittest.mock import patch
import pytest
from common import result_model as rm
from common.result_model import AdUrl


@pytest.mark.parametrize(
    "model, expected",
    [
        (rm.employer, ["phone_number", "email", "url", "organization_number", "name", "workplace"]),
        (rm.appl_details, ["information", "reference", "email", "via_af", "url", "other"]),
        (rm.application_contact, ["name", "description", "email", "telephone", "contact_type"]),
        (
            rm.work_address,
            [
                "municipality",
                "municipality_code",
                "municipality_concept_id",
                "region",
                "region_code",
                "region_concept_id",
                "country",
                "country_code",
                "country_concept_id",
                "street_address",
                "postcode",
                "city",
                "coordinates",
            ],
        ),
        (
            rm.job_ad,
            [
                "id",
                "external_id",
                "original_id",
                "webpage_url",
                "logo_url",
                "headline",
                "application_deadline",
                "number_of_vacancies",
                "label",
                "description",
                "employment_type",
                "salary_type",
                "salary_description",
                "duration",
                "working_hours_type",
                "scope_of_work",
                "access",
                "employer",
                "application_details",
                "experience_required",
                "access_to_own_car",
                "driving_license_required",
                "driving_license",
                "occupation",
                "occupation_group",
                "occupation_field",
                "workplace_address",
                "must_have",
                "nice_to_have",
                "application_contacts",
                "publication_date",
                "last_publication_date",
                "removed",
                "removed_date",
                "source_type",
                "timestamp",
            ],
        ),
        (rm.taxonomy_item, ["concept_id", "label", "legacy_ams_taxonomy_id"]),
        (rm.weighted_taxonomy_item, ["weight"]),
        (rm.min_max, ["min", "max"]),
        (rm.description, ["text", "text_formatted", "company_information", "needs", "requirements", "conditions"]),
        (rm.requirements, ["skills", "languages", "work_experiences", "education", "education_level"]),
    ],
)
def test_all(model, expected):
    model_items = model.items()
    model_fields = set(item[0] for item in model_items)
    expected_fields = set(expected)

    assert (
        model_fields == expected_fields
    ), f"Fields in model do not match expected fields. Found: {model_fields}, Expected: {expected_fields}"


@patch("common.result_model.settings.BASE_PB_URL", "https://some.url/without/trailing/slash")
def test_ad_url_format_without_trailing_slash():
    # Arrange
    ad_url = AdUrl()

    # Act
    url = ad_url.format("job_ad_id")

    # Assert
    assert url == "https://some.url/without/trailing/slash/job_ad_id"


@patch("common.result_model.settings.BASE_PB_URL", "https://some.url/with/trailing/slash/")
def test_ad_url_format_with_trailing_slash():
    # Arrange
    ad_url = AdUrl()

    # Act
    url = ad_url.format("job_ad_id")

    # Assert
    assert url == "https://some.url/with/trailing/slash/job_ad_id"
