import pytest
import requests
from search.common_search.logo import fetch_ad_logo, get_correct_logo_url, get_eventual_logo_url
from unittest.mock import Mock, patch
from werkzeug.exceptions import ServiceUnavailable

@patch('search.common_search.logo.fetch_ad_by_id')
def test_get_correct_logo_url_for_ad_is_none(mock_fetch_ad_by_id):
    """
    Test that get_correct_logo_url function handles None values correctly
    """
    mock_fetch_ad_by_id.return_value = {}

    result = get_correct_logo_url(None)

    assert result is None

@patch('search.common_search.logo.get_eventual_logo_url')
@patch('search.common_search.logo.logo_url_creator')
def test_get_correct_logo_url_for_workplace_id(mock_logo_url_creator, mock_get_eventual_logo_url):
    """
    Test that get_correct_logo_url function returns correct logo url with a valid ad_id
    """

    ad = {'employer': {'workplace_id': 123456}}
    mocked_function_return_value = 'https://www.arbetsformedlingen.se/rest/arbetsgivare/rest/af/v3/arbetsplatser/123456/logotyper/logo.png'

    mock_get_eventual_logo_url.return_value = mocked_function_return_value
    mock_logo_url_creator.return_value = mocked_function_return_value

    result = get_correct_logo_url(ad)

    mock_logo_url_creator.assert_called_with(ad)
    mock_get_eventual_logo_url.assert_called_with(mocked_function_return_value)

    assert result == mocked_function_return_value#"https://www.arbetsformedlingen.se/rest/arbetsgivare/rest/af/v3/arbetsplatser/123456/logotyper/logo.png"

@patch('search.common_search.logo.get_eventual_logo_url')
@patch('search.common_search.logo.logo_url_creator')
def test_get_correct_logo_url_for_organization_number(mock_logo_url_creator, mock_get_eventual_logo_url):
    """
    Test that get_correct_logo_url function returns correct logo url with a valid ad_id
    """

    ad = {'employer': {'workplace_id': 123456}}
    mocked_function_return_value = 'https://www.arbetsformedlingen.se/rest/arbetsgivare/rest/af/v3/organisation/123456/logotyper/logo.png'

    mock_get_eventual_logo_url.return_value = mocked_function_return_value
    mock_logo_url_creator.return_value = mocked_function_return_value

    result = get_correct_logo_url(ad)

    mock_logo_url_creator.assert_called_with(ad)
    mock_get_eventual_logo_url.assert_called_with(mocked_function_return_value)

    assert result == mocked_function_return_value



def test_get_eventual_logo_url_for_valid_url(monkeypatch):
    class MockResponse:
        status_code = 200

        def raise_for_status(self):
            pass

    def mock_head(*args, **kwargs):
        return MockResponse()

    # apply the monkeypatch for requests.head to mock_head
    monkeypatch.setattr(requests, "head", mock_head)

    result = get_eventual_logo_url("https://fakeurl")

    assert result == "https://fakeurl"


def test_get_eventual_logo_url_for_invalid_url(monkeypatch):
    class MockResponse:
        status_code = 400

        def raise_for_status(self):
            pass

    def mock_head(*args, **kwargs):
        return MockResponse()

    # apply the monkeypatch for requests.head to mock_head
    monkeypatch.setattr(requests, "head", mock_head)

    result = get_eventual_logo_url("https://fakeurl")
    assert result is None


def test_get_eventual_logo_url_when_throwing_exception(monkeypatch):
    class MockResponse:
        def __init__(self, *args, **kwargs):
            raise requests.exceptions.ConnectionError

    def mock_head(*args, **kwargs):
        return MockResponse()

    # apply the monkeypatch for requests.head to mock_head
    monkeypatch.setattr(requests, "head", mock_head)

    with pytest.raises(ServiceUnavailable) as excinfo:
        get_eventual_logo_url("https://fakeurl")
    assert "Service Unavailable" in str(excinfo.value)


@patch('search.common_search.logo.settings.COMPANY_LOGO_FETCH_DISABLED', True)
@patch('search.common_search.logo.file_formatter')
@patch('search.common_search.logo.get_not_found_logo_file')
def test_fetch_ad_logo_from_url_when_company_logo_fetch_disabled(mock_get_not_found_logo_file, mock_file_formatter):
    """
    Test that fetch_ad_logo function returns correct logo url with a valid ad_id
    when COMPANY_LOGO_FETCH_DISABLED is True in settings.
    """
    mock_get_not_found_logo_file.return_value = "logo bytes"
    mock_file_formatter.return_value = "some string"

    result = fetch_ad_logo("24373453")

    mock_get_not_found_logo_file.assert_called_with()
    mock_file_formatter.assert_called_with("logo bytes")
    assert result == "some string"


@patch('search.common_search.logo.settings.COMPANY_LOGO_FETCH_DISABLED', False)
@patch('search.common_search.logo.get_correct_logo_url')
@patch('search.common_search.logo.file_formatter')
@patch('search.common_search.logo.fetch_ad_by_id')
@patch('requests.get')
def test_fetch_ad_logo_from_url_when_company_logo_fetch_enabled(mock_requests_get, mock_fetch_ad_by_id, mock_file_formatter, mock_get_correct_logo_url):
    """
    Test that fetch_ad_logo function returns correct logo url with a valid ad_id
    when COMPANY_LOGO_FETCH_DISABLED is False in settings. This is the happy path
    that will return a valid URL for logo without calling the exception.
    """
    mock_ad_id = "24373453"
    mock_file_formatter.return_value = "some string"
    mock_get_correct_logo_url.return_value = "https://fakeurl"
    mock_response = Mock()
    mock_response.status_code = 200
    mock_response.raw.read.return_value = "logo bytes"
    mock_requests_get.return_value = mock_response

    mock_fetch_ad_by_id.return_value = {"id":mock_ad_id}

    result = fetch_ad_logo(mock_ad_id)

    mock_fetch_ad_by_id.assert_called_with(mock_ad_id)
    mock_get_correct_logo_url.assert_called_with({"id":mock_ad_id})
    mock_requests_get.assert_called_with("https://fakeurl", stream=True, timeout=5)
    mock_file_formatter.assert_called_with("logo bytes")
    assert result == "some string"
    """
    Check that the raise_for_status did not get called
    """
    assert not mock_requests_get.raise_for_status.called


@patch('search.common_search.logo.settings.COMPANY_LOGO_FETCH_DISABLED', False)
@patch('search.common_search.logo.get_correct_logo_url')
@patch('search.common_search.logo.file_formatter')
@patch('search.common_search.logo.fetch_ad_by_id')
@patch('requests.get')
def test_fetch_ad_logo_from_url_for_missing_logo(mock_requests_get,mock_fetch_ad_by_id, mock_file_formatter, mock_get_correct_logo_url):
    """
    Test that fetch_ad_logo function returns correct logo url with a valid ad_id
    when COMPANY_LOGO_FETCH_DISABLED is False in settings. This is the unhappy path
    that will throw raise_for_status() exception.
    """
    def raise_error(ex):
        raise ex
    mock_file_formatter.return_value = "some string"
    mock_get_correct_logo_url.return_value = "https://fakeurl"
    mock_response = Mock()
    mock_response.status_code = 404
    mock_response.raise_for_status = lambda: raise_error(requests.exceptions.HTTPError)
    mock_requests_get.return_value = mock_response

    mock_fetch_ad_by_id.return_value = {"id":"24373453"}

    with pytest.raises(requests.exceptions.HTTPError):
        fetch_ad_logo("24373453")

    mock_fetch_ad_by_id("24373453")
    mock_get_correct_logo_url.assert_called_with({"id":"24373453"})
    mock_requests_get.assert_called_with("https://fakeurl", stream=True, timeout=5)




