import pytest  # noqa: F401
from unittest.mock import patch

from search.common_search.taxonomy_lookup import TaxonomyLookup


def test_TaxonomyLookup_object_init():
    # Act
    tl = TaxonomyLookup()

    # Assert
    assert tl


@patch("search.common_search.taxonomy_lookup.settings.TAXONOMY_GRAPHQL_URL", "some-test-url")
@patch("search.common_search.taxonomy_lookup.requests")
def test_TaxonomyLookup_fetch_taxonomy_values(mock_requests):
    # Arrange
    mock_requests.get.return_value.json.return_value = {"data": {"concepts": "test-response"}}
    tl = TaxonomyLookup()

    # Act
    taxonomy_values = tl.fetch_taxonomy_values("some-test-query")

    # Assert
    mock_requests.get.assert_called_once_with(
        "some-test-url", headers={}, params={"query": "some-test-query"}, timeout=10
    )

    assert taxonomy_values == "test-response"
