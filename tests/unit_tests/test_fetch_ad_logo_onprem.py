import unittest
from unittest.mock import patch
from flask import Flask

from search.common_search.logo import fetch_ad_logo

binary_data = b'fake_logo_data'
app = Flask(__name__)


class TestFetchAdLogoFunction(unittest.TestCase):
    def verify_response(self, response):
        assert response.status == '200 OK'
        assert response.content_length == len(binary_data)
        assert response.content_type == 'image/png'

    @patch('common.settings.COMPANY_LOGO_FETCH_DISABLED', False)
    @patch('search.common_search.logo.fetch_ad_by_id')
    @patch('search.common_search.logo.get_correct_logo_url')
    @patch('search.common_search.logo.requests.get')
    @patch('search.common_search.logo.log')
    def test_fetch_ad_logo_success(self, mock_log, mock_requests_get, mock_get_correct_logo_url, mock_fetch_ad_by_id):
        # Mocking external dependencies
        mock_fetch_ad_by_id.return_value = {'ad_id': 123, 'logo_url': 'http://example.com/logo.png'}
        mock_get_correct_logo_url.return_value = 'http://example.com/logo.png'
        mock_requests_get.return_value.status_code = 200
        mock_requests_get.return_value.raw.read.return_value = binary_data

        with app.test_request_context():
            response = fetch_ad_logo(123)
        assert response.status == '200 OK'
        assert response.content_length == len(binary_data)
        assert response.content_type == 'image/png'

        mock_log.debug.assert_called_once_with("Logo found on: http://example.com/logo.png")

    @patch('search.common_search.logo.settings.COMPANY_LOGO_FETCH_DISABLED', True)
    @patch('search.common_search.logo.log')
    def test_fetch_ad_logo_disabled(self, mock_log):
        with app.test_request_context():
            ad_id = 123
            response = fetch_ad_logo(ad_id)
        assert response.status == '200 OK'
        # The response is a 1x1 pixel image
        assert response.content_length == 68
        assert response.content_type == 'image/png'



if __name__ == '__main__':
    unittest.main()
