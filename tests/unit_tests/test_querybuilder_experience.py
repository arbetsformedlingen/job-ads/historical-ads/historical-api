import pytest

from tests.unit_tests.test_resources.mock_for_querybuilder_tests import mock_querybuilder_historical
from datetime import datetime, timedelta


@pytest.mark.parametrize('value', [True, False, 'true', 'false', 'True', 'False'])
def test_experience_values_historical(value):
    args = {'experience': value}
    if value in [True, 'true', 'True']:
        value_in_expected = True
    else:
        value_in_expected = False

    mock_querybuilder = mock_querybuilder_historical
    filter = []
    expected_publication_date = (datetime.now() - timedelta(days=1)).replace(hour=23, minute=59, second=59, microsecond=0).strftime('%Y-%m-%dT%H:%M:%S')
    expected_query_dsl = {
        'from': 0,
        'size': 10,
        'track_total_hits': True,
        'track_scores': True,
        'query': {
            'bool': {
                'must': [
                    {'range': {'publication_date': {'lte': expected_publication_date}}},
                    {'term': {'experience_required': value_in_expected}}
                ],
                'filter': filter
            }
        },
        'aggs': {'positions': {'sum': {'field': 'number_of_vacancies'}}},
        'sort': ['_score', {'publication_date': 'desc'}]
    }

    result = mock_querybuilder.parse_args(args)
    assert result == expected_query_dsl
