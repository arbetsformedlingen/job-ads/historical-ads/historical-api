from unittest.mock import patch

import pytest
from requests import RequestException
from werkzeug.exceptions import ServiceUnavailable

from search.common_search.logo import get_eventual_logo_url


@patch("search.common_search.logo.requests.head")
def test_get_eventual_logo_url_success(mock_head):
    # Arrange
    # Configure the mocked head method to return a successful response
    mock_head.return_value.status_code = 200

    # Act
    result = get_eventual_logo_url("http://logo-endpoint/logo.png")

    # Assert
    assert result == "http://logo-endpoint/logo.png"


@patch("search.common_search.logo.requests.head")
def test_get_eventual_logo_url_error(mock_head):
    # Arrange
    # Configure the mocked head method to raise an exception
    # Test fails if ServiceUnavailable is not raised
    mock_head.side_effect = RequestException()

    # Act & Assert
    with pytest.raises(ServiceUnavailable):
        get_eventual_logo_url("http://logo-endpoint/logo.png")


@patch("search.common_search.logo.requests.head")
def test_get_eventual_logo_url_warning(mock_head):
    # Arrange
    # Configure the mocked head method to return a non-200 status code
    mock_head.return_value.status_code = 404

    # Act
    result = get_eventual_logo_url("http://logo-endpoint/logo.png")

    # Assert
    assert result is None
