import pytest
from common.date_and_time_handling import format_start_date
from unittest.mock import patch
from datetime import datetime

# Valid date test cases
valid_dates = [
    ("2023", "2023-01-01T00:00:00"),
    ("2023-02", "2023-02-01T00:00:00"),
    ("2023-02-28", "2023-02-28T00:00:00"),
    ("2023-02-28T23:45:48", "2023-02-28T23:45:48"),
    ("2020-02-29", "2020-02-29T00:00:00"),  # Leap year
    ("1999", "1999-01-01T00:00:00"),  # Before default_start
    ("2020-12-31T23:59:59", "2020-12-31T23:59:59"),  # Full date-time
    ("1900", "1900-01-01T00:00:00"),  # Historical date
    ("2021-02-28", "2021-02-28T00:00:00"),  # Non-leap year February end
]

# Invalid date test cases
invalid_dates = [
    "invalid-date",
    "2023-13",  # Invalid month
    "2023-02-29",  # Not a leap year
    "2023-02-28T23:45",  # Missing seconds in time
    "2023-00-10",  # Invalid month
    "2023-02-30",  # Impossible day
    "2023-02-28T25:00:00",  # Invalid hour
    "2023-12-31T23:59:60",  # Invalid second
    "abc123",  # Random non-date string
    "2023-02-1",  # Partial date with missing digit
    " ",  # Whitespace input
]

@pytest.mark.parametrize("date, expected", valid_dates)
def test_valid_published_after_dates(date, expected):
    result = format_start_date(date)
    assert result == expected

@pytest.mark.parametrize("date", invalid_dates)
def test_invalid_published_after_dates(date):
    with pytest.raises(ValueError):
        format_start_date(date)

def test_empty_published_after_date():
    assert format_start_date(None) is None

# Test for consistent default value when no date is provided
@patch('common.date_and_time_handling.datetime')
def test_default_start_date(mock_datetime):
    mock_datetime.now.return_value = datetime(2023, 9, 7, 12, 0, 0)
    assert format_start_date(None) is None  # Default should return None
