import pytest
from search.common_search.querybuilder import QueryBuilder


@pytest.mark.parametrize("arg, expected", [
    (True, True),
    ('true', True),
    ('True', True),
    ('TrUe', True),
    (False, False),
    ('false', False),
    ('False', False),
    ('fAlSe', False),
    (None, None),
    ('wrong arg', 'wrong arg'),
    (123, 123),
])
def test_bool_parser(arg, expected):
    query_result = QueryBuilder._parse_boolean_arg(arg)
    assert query_result == expected
