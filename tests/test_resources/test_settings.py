import datetime
import os

from common import constants

# environment variables must be set
TEST_USE_STATIC_DATA = os.getenv('TEST_USE_STATIC_DATA', True)

NUMBER_OF_ADS = 5029
NUMBER_OF_HISTORICAL_ADS = 17750
DAWN_OF_TIME = '2020-01-01T00:00:01'
current_time_stamp = datetime.datetime.now().strftime(constants.DATE_FORMAT)

test_headers = {'accept': 'application/json'}


TEST_URL = os.getenv('TEST_URL', "http://127.0.0.1:5000")

STREAM = "STREAM"
SNAPSHOT = "SNAPSHOT"

EXPECTED_GYMNASIE_LARARE = 62

REMOTE_MATCH_PHRASES = [y.lower() for y in
                        ["Arbeta på distans", "Arbete på distans", "Jobba på distans", "Arbeta hemifrån",
                         "Arbetar hemifrån", "Jobba hemifrån", "Jobb hemifrån", "remote work", "jobba tryggt hemifrån",
                         "work remote", "jobba remote", "arbeta remote", "delvis på distans"]]

OPEN_FOR_ALL_PHRASE = ["öppen för alla"]

TRAINEE_PHRASES = ["rollen som trainee",
                   "söker trainee",
                   "som trainee",
                   "anställningsvillkor Trainee",
                   "as a trainee",
                   "anställer trainee"]

LARLING_PHRASES = ["som lärling",
                   "lärling sökes",
                   "lärlingsplats",
                   "lärlingstjänst",
                   "och lärling"]

FRANCHISE_PHRASES = ["franchisetagare sökes",
                     "rollen som franchisetagare",
                     "som franchisetagare",
                     "franchisetagare/egen företagare"]

HIRE_WORKPLACE_PHRASES = ["anställa/hyra stol",
                          "hyra stol/plats",
                          "hyr du stol",
                          "uthyrning av stol",
                          "uthyrning stol",
                          "hyra stol",
                          "hyrstol",
                          "frisörstol"]
BASE_TAXONOMY_URL = os.getenv('BASE_TAXONOMY_URL', 'https://taxonomy.api.jobtechdev.se/v1/taxonomy/')
