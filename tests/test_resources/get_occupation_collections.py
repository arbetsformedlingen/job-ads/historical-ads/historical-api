import logging
import requests

from tests.test_resources.test_settings import BASE_TAXONOMY_URL

log = logging.getLogger(__name__)

TAXONOMY_GRAPHQL_URL = f"{BASE_TAXONOMY_URL}graphql?"


def _fetch_taxonomy_values(params):
    taxonomy_response = requests.get(url=TAXONOMY_GRAPHQL_URL, headers={}, params=params)
    taxonomy_response.raise_for_status()
    return taxonomy_response.json()


def _fetch_value(query):
    params = {'query': query}
    response = _fetch_taxonomy_values(params)
    values = response.get('data', {}).get('concepts', [])
    return values


occupation_collection_query = """query collections {
  concepts(type: "occupation-collection") {
    id
    related {
      id
    }
  }
}"""


def get_occupation_collections():
    return _fetch_value(occupation_collection_query)
