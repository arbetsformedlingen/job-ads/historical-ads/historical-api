# coding=utf-8


def check_types_top_level(ad):
    for key, expected_type, required in [
        ('id', str, True),
        ('external_id', str, False),
        ('webpage_url', str, True),
        ('logo_url', str, False),
        ('headline', str, False),
        ('application_deadline', str, False),  # date-time
        ('number_of_vacancies', int, False),
        ('description', dict, False),
        ('employment_type', dict, False),
        ('salary_type', dict, False),
        ('salary_description', str, False),
        ('duration', dict, False),
        ('working_hours_type', dict, False),
        ('scope_of_work', dict, False),
        ('access', str, False),
        ('employer', dict, False),
        ('application_details', dict, False),
        ('experience_required', bool, False),
        ('access_to_own_car', bool, False),
        ('driving_license_required', bool, False),
        ('driving_license', list, False),
        ('occupation', dict, True),
        ('occupation_group', dict, True),
        ('occupation_field', dict, True),
        ('workplace_address', dict, False),
        ('must_have', dict, False),
        ('nice_to_have', dict, False),
        ('publication_date', str, True),
        ('last_publication_date', str, False),
        ('removed', bool, True),
        ('removed_date', str, False),
        ('source_type', str, False),
        ('timestamp', int, True)]:
        value = ad[key]
        _check_type(value, expected_type, ad['id'], required)


def check_types_occupation(ad):
    _check_taxonomy_item(ad['occupation'], ad['id'], required=True)
    _check_taxonomy_item(ad['occupation_group'], ad['id'])
    _check_taxonomy_item(ad['occupation_field'], ad['id'])


def check_types_contact(ad):
    if contact_persons := ad.get('application_contacts', None):
        _check_type(ad['application_contacts'], list, ad['id'])
        for person in contact_persons:
            _check_type(person, dict, ad['id'])
            _check_type(person['name'], str, ad['id'])
            _check_type(person['description'], str, ad['id'])
            _check_type(person['email'], str, ad['id'])
            _check_type(person['telephone'], str, ad['id'])
            if contact_type := person['contact_type']:
                _check_type(contact_type, str, ad['id'])
            assert len(person) == 5

    _check_taxonomy_item(ad['occupation_group'], ad['id'])
    _check_taxonomy_item(ad['occupation_field'], ad['id'])


def check_duration(ad):
    _check_taxonomy_item(ad['duration'], ad['id'])


def check_employment_type(ad):
    _check_taxonomy_item(ad['employment_type'], ad['id'])


def check_salary_type(ad):
    _check_taxonomy_item(ad['salary_type'], ad['id'])


def check_types_employer(ad):
    if employer := ad['employer']:
        for field in ['phone_number', 'email', 'url', 'organization_number', 'name', 'workplace']:
            _check_type(employer[field], str, ad['id'])


def check_scope_of_work(ad):
    if scope := ad['scope_of_work']:
        _check_type(scope['min'], int, ad['id'])
        _check_type(scope['max'], int, ad['id'])


def check_application_details(ad):
    if details := ad['application_details']:
        _check_type(details['via_af'], bool, ad['id'], required=False)
        for field in ['information', 'reference', 'email', 'url', 'other']:
            _check_type(details[field], str, ad['id'])


def check_job_ad_description(ad):
    if description := ad['description']:
        for field in ['text', 'text_formatted', 'company_information', 'needs', 'requirements', 'conditions']:
            _check_type(description[field], str, ad['id'])


def check_workplace_address(ad):
    if wpa := ad['workplace_address']:
        for field in ['municipality', 'municipality_code', 'municipality_concept_id', 'region', 'region_code',
                      'region_concept_id', 'country', 'country_code', 'country_concept_id', 'street_address',
                      'postcode', 'city']:
            _check_type(wpa[field], str, ad['id'])


def check_length(ad, expected):
    assert len(ad) == expected, f"ad had length {len(ad)}, but expected {expected}"


def check_requirements(ad):
    for req in ['must_have', 'nice_to_have']:
        if section := ad[req]:
            for field in ['skills', 'languages', 'work_experiences', 'education', 'education_level']:
                for item in section[field]:
                    _check_weighted_taxonomy_item(item, ad['id'])


def check_working_hours_type(ad):
    if hours := ad['working_hours_type']:
        _check_taxonomy_item(hours, ad['id'])


def check_historical_stats(list_of_values):
    for item in list_of_values:
        _check_type(item, dict, 'stats')
        _check_type(item['concept_id'], str, 'stats', required=True)
        _check_type(item['label'], str, 'stats', required=True)
        _check_type(item['occurrences'], int, 'stats', required=True)
        assert len(item) == 4


def _check_taxonomy_item(section, ad_id, required=False):
    assert isinstance(section, dict)
    for field in ['concept_id', 'label', 'legacy_ams_taxonomy_id']:
        _check_type(section[field], str, ad_id, required)


def _check_weighted_taxonomy_item(section, ad_id, required=False):
    _check_type(section['weight'], int, ad_id)
    for field in ['concept_id', 'label', 'legacy_ams_taxonomy_id']:
        _check_type(section[field], str, ad_id, required)


def _check_type(value, expected_type, ad_id, required=False):
    if value is None and not required:
        return True
    try:
        assert isinstance(value,
                          expected_type), f"expected {expected_type}  but got {type(value)}. Value: {value}, ad id: {ad_id}"
    except AssertionError as e:
        print(e)
