from tests.test_resources.helper import select_first_items_in_list


def get_muncipalities_with_hits():
    with_hits = []
    for m in municipalities:
        if m['hits'] > 0:
            with_hits.append(m)
    return with_hits


def get_ten_municipalities_with_hits():
    return select_first_items_in_list(full_list=get_muncipalities_with_hits(), how_many=10)

def get_municipality_by_id(id):
    for m in municipalities:
        if m['id'] == id:
            return m
    return None


municipalities = [
    {'id': 'XWKY_c49_5nv', 'name': 'Upplands Väsby', 'type': 'municipality', 'code': '0114', 'region_code': '01',
     'region_name': 'Stockholms län', 'hits': 14,
     'ad_ids': ['24641303', '24641232', '24636479', '24630746', '24627847', '24612651', '24608713', '24603235',
                '24598186', '24580428']},

    {'id': 'K4az_Bm6_hRV', 'name': 'Vallentuna', 'type': 'municipality', 'code': '0115', 'region_code': '01',
     'region_name': 'Stockholms län', 'hits': 7,
     'ad_ids': ['24649005', '24648696', '24641486', '24611368', '24590586', '24556609', '24519998']},

    {'id': 'mBKv_q3B_SK8', 'name': 'Nykvarn', 'type': 'municipality', 'code': '0140', 'region_code': '01',
     'region_name': 'Stockholms län', 'hits': 7,
     'ad_ids': ['24649772', '24645739', '24644051', '24637850', '24630053', '24625534', '24589239']},

    {'id': 'U4XJ_hYF_FBA', 'name': 'Kinda', 'type': 'municipality', 'code': '0513', 'region_code': '05',
     'region_name': 'Östergötlands län', 'hits': 4, 'ad_ids': ['24640366', '24620200', '24513240', '24434591']},

    {'id': 'mPt5_3QD_LTM', 'name': 'Storfors', 'type': 'municipality', 'code': '1760', 'region_code': '17',
     'region_name': 'Värmlands län', 'hits': 2, 'ad_ids': ['24605489', '24586914']},

    {'id': 'p8Mv_377_bxp', 'name': 'Robertsfors', 'type': 'municipality', 'code': '2409', 'region_code': '24',
     'region_name': 'Västerbottens län', 'hits': 1, 'ad_ids': ['24643631']},

    {'id': '7sHJ_YCE_5Zv', 'name': 'Malå', 'type': 'municipality', 'code': '2418', 'region_code': '24',
     'region_name': 'Västerbottens län', 'hits': 1, 'ad_ids': ['24641503']},

    {'id': 'VM7L_yJK_Doo', 'name': 'Sorsele', 'type': 'municipality', 'code': '2422', 'region_code': '24',
     'region_name': 'Västerbottens län', 'hits': 1, 'ad_ids': ['24644670']},

    {'id': 'vkQW_GB6_MNk', 'name': 'Arjeplog', 'type': 'municipality', 'code': '2506', 'region_code': '25',
     'region_name': 'Norrbottens län', 'hits': 4, 'ad_ids': ['24650467', '24594796', '24505073', '24488974']},

    {'id': 'wYFb_q7w_Nnh', 'name': 'Torsås', 'type': 'municipality', 'code': '0834', 'region_code': '08',
     'region_name': 'Kalmar län', 'hits': 4, 'ad_ids': ['24620345', '24621317', '24604125', '24566576']},

    {'id': 'xk68_bJa_6Fh', 'name': 'Nybro', 'type': 'municipality', 'code': '0881', 'region_code': '08',
     'region_name': 'Kalmar län', 'hits': 6,
     'ad_ids': ['24649149', '24638088', '24631517', '24625340', '24621795', '24612228']},

    {'id': 'roiB_uVV_4Cj', 'name': 'Gullspång', 'type': 'municipality', 'code': '1447', 'region_code': '14',
     'region_name': 'Västra Götalands län', 'hits': 1, 'ad_ids': ['24643272']},

    {'id': 'Zsf5_vpP_Bs4', 'name': 'Tidaholm', 'type': 'municipality', 'code': '1498', 'region_code': '14',
     'region_name': 'Västra Götalands län', 'hits': 2, 'ad_ids': ['24617483', '24600800']},

    {'id': '8gKt_ZsV_PGj', 'name': 'Österåker', 'type': 'municipality', 'code': '0117', 'region_code': '01',
     'region_name': 'Stockholms län', 'hits': 8,
     'ad_ids': ['24640198', '24634294', '23699999', '24624738', '24577311', '24576867', '24532362', '24518247']},

    {'id': '15nx_Vut_GrH', 'name': 'Värmdö', 'type': 'municipality', 'code': '0120', 'region_code': '01',
     'region_name': 'Stockholms län', 'hits': 11,
     'ad_ids': ['24632907', '24631461', '24620746', '24614472', '24610276', '24602803', '24590922', '24577016',
                '24574442', '24509435']},

    {'id': 'qm5H_jsD_fUF', 'name': 'Järfälla', 'type': 'municipality', 'code': '0123', 'region_code': '01',
     'region_name': 'Stockholms län', 'hits': 16,
     'ad_ids': ['24648843', '24647618', '24638674', '24637614', '24627854', '24626944', '24623198', '24622371',
                '24619258', '24616216']},

    {'id': 'magF_Gon_YL2', 'name': 'Ekerö', 'type': 'municipality', 'code': '0125', 'region_code': '01',
     'region_name': 'Stockholms län', 'hits': 11,
     'ad_ids': ['24647866', '24643150', '24638266', '24637006', '24626766', '24618010', '24617986', '24609385',
                '24601615', '24541639']},

    {'id': 'g1Gc_aXK_EKu', 'name': 'Huddinge', 'type': 'municipality', 'code': '0126', 'region_code': '01',
     'region_name': 'Stockholms län', 'hits': 47,
     'ad_ids': ['24649947', '24649919', '24649818', '24647357', '24641723', '24640701', '24639742', '24638132',
                '24636901',
                '24633282']},

    {'id': 'CCVZ_JA7_d3y', 'name': 'Botkyrka', 'type': 'municipality', 'code': '0127', 'region_code': '01',
     'region_name': 'Stockholms län', 'hits': 20,
     'ad_ids': ['24649834', '24649058', '24645367', '24642952', '24637457', '24635479', '24634919', '24629461',
                '24622699', '24615777']},

    {'id': '4KBw_CPU_VQv', 'name': 'Salem', 'type': 'municipality', 'code': '0128', 'region_code': '01',
     'region_name': 'Stockholms län', 'hits': 2, 'ad_ids': ['24635325', '24596057']},

    {'id': 'Q7gp_9dT_k2F', 'name': 'Haninge', 'type': 'municipality', 'code': '0136', 'region_code': '01',
     'region_name': 'Stockholms län', 'hits': 17,
     'ad_ids': ['24650480', '24647950', '24630278', '24623592', '24616301', '24614366', '24606163', '24604574',
                '24589305', '24586867']},

    {'id': 'sTPc_k2B_SqV', 'name': 'Tyresö', 'type': 'municipality', 'code': '0138', 'region_code': '01',
     'region_name': 'Stockholms län', 'hits': 7,
     'ad_ids': ['24643255', '24624425', '24618754', '24614988', '24613766', '24598566', '24597634']},

    {'id': 'w6yq_CGR_Fiv', 'name': 'Upplands-Bro', 'type': 'municipality', 'code': '0139', 'region_code': '01',
     'region_name': 'Stockholms län', 'hits': 11,
     'ad_ids': ['24649677', '24644808', '24643168', '24643140', '24641431', '24554813', '24628120', '24614768',
                '24604245', '24603297']},

    {'id': 'onpA_B5a_zfv', 'name': 'Täby', 'type': 'municipality', 'code': '0160', 'region_code': '01',
     'region_name': 'Stockholms län', 'hits': 25,
     'ad_ids': ['24640762', '24640430', '24640171', '24637534', '24625126', '24624931', '24613119', '24611999',
                '24610353', '24609668']},

    {'id': 'E4CV_a5E_ucX', 'name': 'Danderyd', 'type': 'municipality', 'code': '0162', 'region_code': '01',
     'region_name': 'Stockholms län', 'hits': 18,
     'ad_ids': ['24650650', '24650652', '24643754', '24639629', '24638561', '24638260', '24614446', '24611837',
                '24609748', '24597392']},

    {'id': 'Z5Cq_SgB_dsB', 'name': 'Sollentuna', 'type': 'municipality', 'code': '0163', 'region_code': '01',
     'region_name': 'Stockholms län', 'hits': 8,
     'ad_ids': ['24648025', '24646503', '24634127', '24627899', '24624197', '24612586', '24605084', '24604254']},

    {'id': 'AvNB_uwa_6n6', 'name': 'Stockholm', 'type': 'municipality', 'code': '0180', 'region_code': '01',
     'region_name': 'Stockholms län', 'hits': 775,
     'ad_ids': ['24650983', '24650895', '24650889', '24650784', '24650759', '24650748', '24650728', '24650682',
                '24650497', '24650349']},

    {'id': 'g6hK_M1o_hiU', 'name': 'Södertälje', 'type': 'municipality', 'code': '0181', 'region_code': '01',
     'region_name': 'Stockholms län', 'hits': 52,
     'ad_ids': ['24650352', '24647317', '24646726', '24644816', '24644223', '24642658', '24642513', '24641912',
                '24641018', '24640024']},

    {'id': 'aYA7_PpG_BqP', 'name': 'Nacka', 'type': 'municipality', 'code': '0182', 'region_code': '01',
     'region_name': 'Stockholms län', 'hits': 27,
     'ad_ids': ['24649527', '24649274', '24645898', '24644478', '24581554', '24636080', '24634930', '24628301',
                '24627163', '24621428']},

    {'id': 'UTJZ_zHH_mJm', 'name': 'Sundbyberg', 'type': 'municipality', 'code': '0183', 'region_code': '01',
     'region_name': 'Stockholms län', 'hits': 11,
     'ad_ids': ['24643175', '24638423', '24637895', '24621759', '24611001', '24605909', '24602420', '24599279',
                '24590884', '24552397']},

    {'id': 'zHxw_uJZ_NJ8', 'name': 'Solna', 'type': 'municipality', 'code': '0184', 'region_code': '01',
     'region_name': 'Stockholms län', 'hits': 61,
     'ad_ids': ['24650876', '24649894', '24649491', '24648158', '24644646', '24643212', '24641524', '24640665',
                '24639217', '24638221']},

    {'id': 'FBbF_mda_TYD', 'name': 'Lidingö', 'type': 'municipality', 'code': '0186', 'region_code': '01',
     'region_name': 'Stockholms län', 'hits': 13,
     'ad_ids': ['24648043', '24648032', '24645326', '24631039', '24238908', '24611032', '24606240', '24604895',
                '24603294', '24593721']},

    {'id': '9aAJ_j6L_DST', 'name': 'Vaxholm', 'type': 'municipality', 'code': '0187', 'region_code': '01',
     'region_name': 'Stockholms län', 'hits': 2, 'ad_ids': ['24618958', '24542055']},

    {'id': 'btgf_fS7_sKG', 'name': 'Norrtälje', 'type': 'municipality', 'code': '0188', 'region_code': '01',
     'region_name': 'Stockholms län', 'hits': 26,
     'ad_ids': ['24647156', '24645902', '24645523', '24644525', '24638282', '24639147', '24636961', '24633665',
                '24628281', '24626958']},

    {'id': '8ryy_X54_xJj', 'name': 'Sigtuna', 'type': 'municipality', 'code': '0191', 'region_code': '01',
     'region_name': 'Stockholms län', 'hits': 19,
     'ad_ids': ['24649537', '24648645', '24645926', '24643945', '24636904', '24636803', '24634498', '24633655',
                '24631760', '24628534']},

    {'id': '37UU_T7x_oxG', 'name': 'Nynäshamn', 'type': 'municipality', 'code': '0192', 'region_code': '01',
     'region_name': 'Stockholms län', 'hits': 12,
     'ad_ids': ['24649508', '24649463', '24643147', '24642006', '24636125', '24620822', '24616700', '24600203',
                '24575057', '24566977']},

    {'id': 'Bbs5_JUs_Qh5', 'name': 'Håbo', 'type': 'municipality', 'code': '0305', 'region_code': '03',
     'region_name': 'Uppsala län', 'hits': 5, 'ad_ids': ['24650354', '24645507', '24643909', '24562714', '24485221']},

    {'id': 'cbyw_9aK_Cni', 'name': 'Älvkarleby', 'type': 'municipality', 'code': '0319', 'region_code': '03',
     'region_name': 'Uppsala län', 'hits': 1, 'ad_ids': ['24623380']},

    {'id': 'KALq_sG6_VrW', 'name': 'Knivsta', 'type': 'municipality', 'code': '0330', 'region_code': '03',
     'region_name': 'Uppsala län', 'hits': 5, 'ad_ids': ['24634631', '24610544', '24593401', '24577340', '24499916']},

    {'id': 'K8A2_JBa_e6e', 'name': 'Tierp', 'type': 'municipality', 'code': '0360', 'region_code': '03',
     'region_name': 'Uppsala län', 'hits': 5, 'ad_ids': ['24646413', '24642957', '24632440', '24629420', '24593674']},

    {'id': 'otaF_bQY_4ZD', 'name': 'Uppsala', 'type': 'municipality', 'code': '0380', 'region_code': '03',
     'region_name': 'Uppsala län', 'hits': 116,
     'ad_ids': ['24650128', '24649375', '24649111', '24648825', '24648007', '24641555', '24647083', '24642165',
                '24645085', '24643472']},

    {'id': 'HGwg_unG_TsG', 'name': 'Enköping', 'type': 'municipality', 'code': '0381', 'region_code': '03',
     'region_name': 'Uppsala län', 'hits': 31,
     'ad_ids': ['24649986', '24640955', '24636030', '24634261', '24634124', '24633958', '24633755', '24632200',
                '24631332', '24630750']},

    {'id': 'VE3L_3Ei_XbG', 'name': 'Östhammar', 'type': 'municipality', 'code': '0382', 'region_code': '03',
     'region_name': 'Uppsala län', 'hits': 7,
     'ad_ids': ['24636870', '24620508', '24610180', '24602559', '24545246', '24528574', '24456277']},

    {'id': 'rut9_f5W_kTX', 'name': 'Vingåker', 'type': 'municipality', 'code': '0428', 'region_code': '04',
     'region_name': 'Södermanlands län', 'hits': 3, 'ad_ids': ['24630384', '24611402', '24610522']},

    {'id': 'os8Y_RUo_U3u', 'name': 'Gnesta', 'type': 'municipality', 'code': '0461', 'region_code': '04',
     'region_name': 'Södermanlands län', 'hits': 7,
     'ad_ids': ['24624678', '24614695', '24606086', '24539789', '24527883', '24452669', '24437897']},

    {'id': 'KzvD_ePV_DKQ', 'name': 'Nyköping', 'type': 'municipality', 'code': '0480', 'region_code': '04',
     'region_name': 'Södermanlands län', 'hits': 32,
     'ad_ids': ['24648507', '24647220', '24646241', '24644029', '24643813', '24640685', '24638209', '24621102',
                '24619295', '24618870']},

    {'id': '72XK_mUU_CAH', 'name': 'Oxelösund', 'type': 'municipality', 'code': '0481', 'region_code': '04',
     'region_name': 'Södermanlands län', 'hits': 6,
     'ad_ids': ['24650534', '24638207', '24635193', '24627634', '24572186', '24470292']},

    {'id': 'P8yp_WT9_Bks', 'name': 'Flen', 'type': 'municipality', 'code': '0482', 'region_code': '04',
     'region_name': 'Södermanlands län', 'hits': 8,
     'ad_ids': ['24642920', '24642061', '24617764', '24617668', '24584928', '24570283', '24457975', '24252077']},

    {'id': 'snx9_qVD_Dr1', 'name': 'Katrineholm', 'type': 'municipality', 'code': '0483', 'region_code': '04',
     'region_name': 'Södermanlands län', 'hits': 11,
     'ad_ids': ['24641421', '24640118', '24638917', '24634274', '24632221', '24627764', '24624267', '24572047',
                '24499697', '24462987']},

    {'id': 'kMxr_NiX_YrU', 'name': 'Eskilstuna', 'type': 'municipality', 'code': '0484', 'region_code': '04',
     'region_name': 'Södermanlands län', 'hits': 37,
     'ad_ids': ['24649500', '24648293', '24642848', '24642008', '24635208', '24634840', '24634180', '24624433',
                '24623481', '24622346']},

    {'id': 'shnD_RiE_RKL', 'name': 'Strängnäs', 'type': 'municipality', 'code': '0486', 'region_code': '04',
     'region_name': 'Södermanlands län', 'hits': 13,
     'ad_ids': ['24645062', '24640755', '24633881', '24629018', '24628036', '24626818', '24620342', '24617315',
                '24613763', '24606350']},

    {'id': 'rjzu_nQn_mCK', 'name': 'Trosa', 'type': 'municipality', 'code': '0488', 'region_code': '04',
     'region_name': 'Södermanlands län', 'hits': 6,
     'ad_ids': ['24629673', '24617357', '24615644', '24601519', '24573221', '24568390']},

    {'id': 'Fu8g_29u_3xF', 'name': 'Ödeshög', 'type': 'municipality', 'code': '0509', 'region_code': '05',
     'region_name': 'Östergötlands län', 'hits': 4, 'ad_ids': ['24633236', '24604733', '24604699', '24536516']},

    {'id': 'vRRz_nLT_vYv', 'name': 'Ydre', 'type': 'municipality', 'code': '0512', 'region_code': '05',
     'region_name': 'Östergötlands län', 'hits': 1, 'ad_ids': ['24489383']},

    {'id': 'e5LB_m9V_TnT', 'name': 'Boxholm', 'type': 'municipality', 'code': '0560', 'region_code': '05',
     'region_name': 'Östergötlands län', 'hits': 1, 'ad_ids': ['24614590']},

    {'id': 'bFWo_FRJ_x2T', 'name': 'Åtvidaberg', 'type': 'municipality', 'code': '0561', 'region_code': '05',
     'region_name': 'Östergötlands län', 'hits': 1, 'ad_ids': ['24614818']},

    {'id': 'dMFe_J6W_iJv', 'name': 'Finspång', 'type': 'municipality', 'code': '0562', 'region_code': '05',
     'region_name': 'Östergötlands län', 'hits': 7,
     'ad_ids': ['24644134', '24625540', '24612693', '24611379', '24590409', '24583823', '24471628']},

    {'id': 'Sb3D_iGB_aXu', 'name': 'Valdemarsvik', 'type': 'municipality', 'code': '0563', 'region_code': '05',
     'region_name': 'Östergötlands län', 'hits': 3, 'ad_ids': ['24645130', '24645129', '24597478']},

    {'id': 'bm2x_1mr_Qhx', 'name': 'Linköping', 'type': 'municipality', 'code': '0580', 'region_code': '05',
     'region_name': 'Östergötlands län', 'hits': 110,
     'ad_ids': ['24650847', '24650299', '24650286', '24650132', '24650066', '24649942', '24649791', '24649190',
                '24648447', '24647737']},

    {'id': 'SYty_Yho_JAF', 'name': 'Norrköping', 'type': 'municipality', 'code': '0581', 'region_code': '05',
     'region_name': 'Östergötlands län', 'hits': 77,
     'ad_ids': ['24645698', '24644342', '24642624', '24642581', '24642596', '24638971', '24637959', '24636774',
                '24635847', '24635810']},

    {'id': 'Pcv9_yYh_Uw8', 'name': 'Söderköping', 'type': 'municipality', 'code': '0582', 'region_code': '05',
     'region_name': 'Östergötlands län', 'hits': 5,
     'ad_ids': ['24643307', '24593500', '24592848', '24579758', '24540435']},

    {'id': 'E1MC_1uG_phm', 'name': 'Motala', 'type': 'municipality', 'code': '0583', 'region_code': '05',
     'region_name': 'Östergötlands län', 'hits': 13,
     'ad_ids': ['24650644', '24647728', '24647601', '24644258', '24639992', '24635027', '24627968', '24625493',
                '24611718', '24574665']},

    {'id': 'VcCU_Y86_eKU', 'name': 'Vadstena', 'type': 'municipality', 'code': '0584', 'region_code': '05',
     'region_name': 'Östergötlands län', 'hits': 6,
     'ad_ids': ['24636656', '24631849', '24613868', '24605632', '24597399', '24579124']},

    {'id': 'stqv_JGB_x8A', 'name': 'Mjölby', 'type': 'municipality', 'code': '0586', 'region_code': '05',
     'region_name': 'Östergötlands län', 'hits': 17,
     'ad_ids': ['24648019', '24644313', '24632147', '24628098', '24622278', '24616742', '24596459', '24615796',
                '24613095', '24605455']},

    {'id': 'yaHU_E7z_YnE', 'name': 'Lekeberg', 'type': 'municipality', 'code': '1814', 'region_code': '18',
     'region_name': 'Örebro län', 'hits': 3, 'ad_ids': ['24618574', '24590700', '24573858']},

    {'id': 'oYEQ_m8Q_unY', 'name': 'Laxå', 'type': 'municipality', 'code': '1860', 'region_code': '18',
     'region_name': 'Örebro län', 'hits': 1, 'ad_ids': ['24613284']},

    {'id': 'Ak9V_rby_yYS', 'name': 'Hallsberg', 'type': 'municipality', 'code': '1861', 'region_code': '18',
     'region_name': 'Örebro län', 'hits': 5, 'ad_ids': ['24645029', '24641916', '24630258', '24625794', '23791205']},

    {'id': 'pvzC_muj_rcq', 'name': 'Degerfors', 'type': 'municipality', 'code': '1862', 'region_code': '18',
     'region_name': 'Örebro län', 'hits': 2, 'ad_ids': ['24640136', '24612483']},

    {'id': 'sCbY_r36_xhs', 'name': 'Hällefors', 'type': 'municipality', 'code': '1863', 'region_code': '18',
     'region_name': 'Örebro län', 'hits': 2, 'ad_ids': ['24597505', '24562629']},

    {'id': 'eF2n_714_hSU', 'name': 'Ljusnarsberg', 'type': 'municipality', 'code': '1864', 'region_code': '18',
     'region_name': 'Örebro län', 'hits': 1, 'ad_ids': ['24620853']},

    {'id': 'kuMn_feU_hXx', 'name': 'Örebro', 'type': 'municipality', 'code': '1880', 'region_code': '18',
     'region_name': 'Örebro län', 'hits': 83,
     'ad_ids': ['24650582', '24650365', '24649289', '24649147', '24648442', '24648272', '24648247', '24647801',
                '24646747', '24646234']},

    {'id': 'viCA_36P_pQp', 'name': 'Kumla', 'type': 'municipality', 'code': '1881', 'region_code': '18',
     'region_name': 'Örebro län', 'hits': 6,
     'ad_ids': ['24628041', '24627103', '24620812', '24612404', '24591455', '24525516']},

    {'id': 'dbF7_Ecz_CWF', 'name': 'Askersund', 'type': 'municipality', 'code': '1882', 'region_code': '18',
     'region_name': 'Örebro län', 'hits': 1, 'ad_ids': ['24614139']},

    {'id': 'wgJm_upX_z5W', 'name': 'Karlskoga', 'type': 'municipality', 'code': '1883', 'region_code': '18',
     'region_name': 'Örebro län', 'hits': 14,
     'ad_ids': ['24641108', '24640044', '24638125', '24627556', '24626010', '24619701', '24614690', '24596123',
                '24574044', '24506835']},

    {'id': 'WFXN_hsU_gmx', 'name': 'Nora', 'type': 'municipality', 'code': '1884', 'region_code': '18',
     'region_name': 'Örebro län', 'hits': 2, 'ad_ids': ['24645958', '24641024']},

    {'id': 'JQE9_189_Ska', 'name': 'Lindesberg', 'type': 'municipality', 'code': '1885', 'region_code': '18',
     'region_name': 'Örebro län', 'hits': 9,
     'ad_ids': ['24621412', '24640141', '24640009', '24631398', '24629694', '24619671', '24594052', '24563255',
                '24559937']},

    {'id': 'Nufj_vmt_VrH', 'name': 'Skinnskatteberg', 'type': 'municipality', 'code': '1904', 'region_code': '19',
     'region_name': 'Västmanlands län', 'hits': 2, 'ad_ids': ['24616844', '24561824']},

    {'id': 'jfD3_Hdg_UhT', 'name': 'Surahammar', 'type': 'municipality', 'code': '1907', 'region_code': '19',
     'region_name': 'Västmanlands län', 'hits': 2, 'ad_ids': ['24569744', '24569099']},

    {'id': 'sD2e_1Tr_4WZ', 'name': 'Heby', 'type': 'municipality', 'code': '0331', 'region_code': '03',
     'region_name': 'Uppsala län', 'hits': 6,
     'ad_ids': ['24639987', '24637102', '24636561', '24622419', '24609485', '24606439']},

    {'id': 'Fac5_h7a_UoM', 'name': 'Kungsör', 'type': 'municipality', 'code': '1960', 'region_code': '19',
     'region_name': 'Västmanlands län', 'hits': 1, 'ad_ids': ['24615040']},

    {'id': 'oXYf_HmD_ddE', 'name': 'Hallstahammar', 'type': 'municipality', 'code': '1961', 'region_code': '19',
     'region_name': 'Västmanlands län', 'hits': 3, 'ad_ids': ['24620856', '24620183', '24570400']},

    {'id': 'jbVe_Cps_vtd', 'name': 'Norberg', 'type': 'municipality', 'code': '1962', 'region_code': '19',
     'region_name': 'Västmanlands län', 'hits': 1, 'ad_ids': ['24615173']},

    {'id': '8deT_FRF_2SP', 'name': 'Västerås', 'type': 'municipality', 'code': '1980', 'region_code': '19',
     'region_name': 'Västmanlands län', 'hits': 53,
     'ad_ids': ['24650320', '24648681', '24647561', '24647061', '24646018', '24645554', '24645115', '24643524',
                '24643419', '24641832']},

    {'id': 'dAen_yTK_tqz', 'name': 'Sala', 'type': 'municipality', 'code': '1981', 'region_code': '19',
     'region_name': 'Västmanlands län', 'hits': 9,
     'ad_ids': ['24647939', '24629771', '24628165', '24627608', '24625106', '24625081', '24618592', '24607178',
                '24568713']},

    {'id': '7D9G_yrX_AGJ', 'name': 'Fagersta', 'type': 'municipality', 'code': '1982', 'region_code': '19',
     'region_name': 'Västmanlands län', 'hits': 6,
     'ad_ids': ['24642864', '24636952', '24621583', '24611864', '24610150', '24596200']},

    {'id': '4Taz_AuG_tSm', 'name': 'Köping', 'type': 'municipality', 'code': '1983', 'region_code': '19',
     'region_name': 'Västmanlands län', 'hits': 5,
     'ad_ids': ['24641084', '24633855', '24614521', '24579728', '24554788']},

    {'id': 'Jkyb_5MQ_7pB', 'name': 'Arboga', 'type': 'municipality', 'code': '1984', 'region_code': '19',
     'region_name': 'Västmanlands län', 'hits': 6,
     'ad_ids': ['24634844', '24632224', '24630389', '24629338', '24628382', '24523724']},

    {'id': '1gEC_kvM_TXK', 'name': 'Olofström', 'type': 'municipality', 'code': '1060', 'region_code': '10',
     'region_name': 'Blekinge län', 'hits': 3, 'ad_ids': ['24650780', '24607103', '24588059']},

    {'id': 'YSt4_bAa_ccs', 'name': 'Karlskrona', 'type': 'municipality', 'code': '1080', 'region_code': '10',
     'region_name': 'Blekinge län', 'hits': 28,
     'ad_ids': ['24651075', '24649518', '24649448', '24645432', '24645121', '24644839', '24628869', '24633251',
                '24633023', '24630721']},

    {'id': 'vH8x_gVz_z7R', 'name': 'Ronneby', 'type': 'municipality', 'code': '1081', 'region_code': '10',
     'region_name': 'Blekinge län', 'hits': 10,
     'ad_ids': ['24648307', '24648283', '24635765', '24621669', '24621710', '24620613', '24605177', '24605428',
                '24603527', '24590026']},

    {'id': 'HtGW_WgR_dpE', 'name': 'Karlshamn', 'type': 'municipality', 'code': '1082', 'region_code': '10',
     'region_name': 'Blekinge län', 'hits': 15,
     'ad_ids': ['24649847', '24649768', '24647740', '24632517', '24612809', '24607875', '24607874', '24602594',
                '24600682', '24600425']},

    {'id': 'EVPy_phD_8Vf', 'name': 'Sölvesborg', 'type': 'municipality', 'code': '1083', 'region_code': '10',
     'region_name': 'Blekinge län', 'hits': 4, 'ad_ids': ['24644857', '24625704', '24620694', '24535492']},

    {'id': '2r6J_g2w_qp5', 'name': 'Svalöv', 'type': 'municipality', 'code': '1214', 'region_code': '12',
     'region_name': 'Skåne län', 'hits': 1, 'ad_ids': ['24645986']},

    {'id': 'vBrj_bov_KEX', 'name': 'Staffanstorp', 'type': 'municipality', 'code': '1230', 'region_code': '12',
     'region_name': 'Skåne län', 'hits': 8,
     'ad_ids': ['24648873', '24623551', '24614129', '24598158', '24593510', '24562275', '24565177', '24514487']},

    {'id': '64g5_Lio_aMU', 'name': 'Burlöv', 'type': 'municipality', 'code': '1231', 'region_code': '12',
     'region_name': 'Skåne län', 'hits': 9,
     'ad_ids': ['24649934', '24624002', '24615590', '24596313', '24598750', '24592314', '24583797', '24565624',
                '24440835']},

    {'id': 'Tcog_5sH_b46', 'name': 'Vellinge', 'type': 'municipality', 'code': '1233', 'region_code': '12',
     'region_name': 'Skåne län', 'hits': 3, 'ad_ids': ['24637147', '24622105', '24546803']},

    {'id': 'LTt7_CGG_RUf', 'name': 'Östra Göinge', 'type': 'municipality', 'code': '1256', 'region_code': '12',
     'region_name': 'Skåne län', 'hits': 4, 'ad_ids': ['24615444', '24610769', '24593919', '24589529']},

    {'id': 'nBTS_Nge_dVH', 'name': 'Örkelljunga', 'type': 'municipality', 'code': '1257', 'region_code': '12',
     'region_name': 'Skåne län', 'hits': 1, 'ad_ids': ['24620885']},

    {'id': 'waQp_FjW_qhF', 'name': 'Bjuv', 'type': 'municipality', 'code': '1260', 'region_code': '12',
     'region_name': 'Skåne län', 'hits': 2, 'ad_ids': ['24614178', '24590365']},

    {'id': '5ohg_WJU_Ktn', 'name': 'Kävlinge', 'type': 'municipality', 'code': '1261', 'region_code': '12',
     'region_name': 'Skåne län', 'hits': 10,
     'ad_ids': ['24645728', '24640743', '24637954', '24634361', '24631933', '24622076', '24615122', '24601405',
                '24584558', '24570281']},

    {'id': 'naG4_AUS_z2v', 'name': 'Lomma', 'type': 'municipality', 'code': '1262', 'region_code': '12',
     'region_name': 'Skåne län', 'hits': 10,
     'ad_ids': ['24650188', '24649514', '24630578', '24607479', '24607474', '24593043', '24587683', '24555292',
                '24549470', '24500680']},

    {'id': 'n6r4_fjK_kRr', 'name': 'Svedala', 'type': 'municipality', 'code': '1263', 'region_code': '12',
     'region_name': 'Skåne län', 'hits': 4, 'ad_ids': ['24648311', '24624676', '24615959', '24590287']},

    {'id': 'oezL_78x_r89', 'name': 'Skurup', 'type': 'municipality', 'code': '1264', 'region_code': '12',
     'region_name': 'Skåne län', 'hits': 4, 'ad_ids': ['24606877', '24596998', '24590239', '24555159']},

    {'id': 'P3Cs_1ZP_9XB', 'name': 'Sjöbo', 'type': 'municipality', 'code': '1265', 'region_code': '12',
     'region_name': 'Skåne län', 'hits': 5, 'ad_ids': ['24636041', '24624217', '24572488', '24562106', '24415688']},

    {'id': 'autr_KMa_cfp', 'name': 'Hörby', 'type': 'municipality', 'code': '1266', 'region_code': '12',
     'region_name': 'Skåne län', 'hits': 7,
     'ad_ids': ['24647623', '24612427', '24598016', '24585770', '24576073', '24575956', '24388252']},

    {'id': 'N29z_AqQ_Ppc', 'name': 'Höör', 'type': 'municipality', 'code': '1267', 'region_code': '12',
     'region_name': 'Skåne län', 'hits': 8,
     'ad_ids': ['24645789', '24611931', '24590427', '24580373', '24533802', '24524712', '24505268', '24495006']},

    {'id': 'UMev_wGs_9bg', 'name': 'Tomelilla', 'type': 'municipality', 'code': '1270', 'region_code': '12',
     'region_name': 'Skåne län', 'hits': 5, 'ad_ids': ['24629168', '24624712', '24621866', '24620688', '24616138']},

    {'id': 'WMNK_PXa_Khm', 'name': 'Bromölla', 'type': 'municipality', 'code': '1272', 'region_code': '12',
     'region_name': 'Skåne län', 'hits': 8,
     'ad_ids': ['24648756', '24645758', '24645101', '24624728', '24584175', '24582959', '24513573', '24513519']},

    {'id': 'najS_Lvy_mDD', 'name': 'Osby', 'type': 'municipality', 'code': '1273', 'region_code': '12',
     'region_name': 'Skåne län', 'hits': 6,
     'ad_ids': ['24622447', '24612064', '24590486', '24584265', '24570384', '24484317']},

    {'id': 'BN7r_iPV_F9p', 'name': 'Perstorp', 'type': 'municipality', 'code': '1275', 'region_code': '12',
     'region_name': 'Skåne län', 'hits': 3, 'ad_ids': ['24644535', '24620267', '24590493']},

    {'id': 'JARU_FAY_hTS', 'name': 'Klippan', 'type': 'municipality', 'code': '1276', 'region_code': '12',
     'region_name': 'Skåne län', 'hits': 11,
     'ad_ids': ['24649729', '24645668', '24630230', '24630125', '24627948', '24613981', '24602922', '24592988',
                '24587696', '24576122']},

    {'id': 'tEv6_ktG_QQb', 'name': 'Åstorp', 'type': 'municipality', 'code': '1277', 'region_code': '12',
     'region_name': 'Skåne län', 'hits': 7,
     'ad_ids': ['24646009', '24643953', '24611585', '24576459', '24572768', '24572674', '24488210']},

    {'id': 'i8vK_odq_6ar', 'name': 'Båstad', 'type': 'municipality', 'code': '1278', 'region_code': '12',
     'region_name': 'Skåne län', 'hits': 6,
     'ad_ids': ['24648777', '24642713', '24635648', '24546923', '24546926', '24437820']},

    {'id': 'oYPt_yRA_Smm', 'name': 'Malmö', 'type': 'municipality', 'code': '1280', 'region_code': '12',
     'region_name': 'Skåne län', 'hits': 208,
     'ad_ids': ['24650545', '24650520', '24650296', '24650097', '24650060', '24649950', '24648137', '24647586',
                '24647704', '24647414']},

    {'id': 'muSY_tsR_vDZ', 'name': 'Lund', 'type': 'municipality', 'code': '1281', 'region_code': '12',
     'region_name': 'Skåne län', 'hits': 94,
     'ad_ids': ['24650438', '24650135', '24649589', '24648781', '24647011', '24647010', '24646930', '24646668',
                '24645906', '24645894']},

    {'id': 'Yt5s_Vf9_rds', 'name': 'Landskrona', 'type': 'municipality', 'code': '1282', 'region_code': '12',
     'region_name': 'Skåne län', 'hits': 13,
     'ad_ids': ['24644146', '24644087', '24620576', '24620131', '24615771', '24573791', '24570452', '24567461',
                '24540874', '24488640']},

    {'id': 'qj3q_oXH_MGR', 'name': 'Helsingborg', 'type': 'municipality', 'code': '1283', 'region_code': '12',
     'region_name': 'Skåne län', 'hits': 75,
     'ad_ids': ['24649269', '24648344', '24647195', '24645865', '24645699', '24644730', '24644155', '24643877',
                '24643808', '24643688']},

    {'id': '8QQ6_e95_a1d', 'name': 'Höganäs', 'type': 'municipality', 'code': '1284', 'region_code': '12',
     'region_name': 'Skåne län', 'hits': 5, 'ad_ids': ['24634533', '24625162', '24623134', '24620349', '24576341']},

    {'id': 'gfCw_egj_1M4', 'name': 'Eslöv', 'type': 'municipality', 'code': '1285', 'region_code': '12',
     'region_name': 'Skåne län', 'hits': 7,
     'ad_ids': ['24634664', '24629637', '24616790', '24608987', '24607603', '24603214', '24500582']},

    {'id': 'hdYk_hnP_uju', 'name': 'Ystad', 'type': 'municipality', 'code': '1286', 'region_code': '12',
     'region_name': 'Skåne län', 'hits': 11,
     'ad_ids': ['24644036', '24625823', '24601172', '24599304', '24583510', '24574648', '24572130', '24511562',
                '24511260', '24473915']},

    {'id': 'STvk_dra_M1X', 'name': 'Trelleborg', 'type': 'municipality', 'code': '1287', 'region_code': '12',
     'region_name': 'Skåne län', 'hits': 18,
     'ad_ids': ['24649417', '24645920', '24645911', '24640015', '24632826', '24630587', '24614377', '24608596',
                '24608211', '24604616']},

    {'id': 'vrvW_sr8_1en', 'name': 'Kristianstad', 'type': 'municipality', 'code': '1290', 'region_code': '12',
     'region_name': 'Skåne län', 'hits': 36,
     'ad_ids': ['24649118', '24646547', '24645051', '24642902', '24639033', '24636394', '24636028', '24633571',
                '24631796', '24623098']},

    {'id': 'dLxo_EpC_oPe', 'name': 'Simrishamn', 'type': 'municipality', 'code': '1291', 'region_code': '12',
     'region_name': 'Skåne län', 'hits': 5, 'ad_ids': ['24636522', '24601452', '24550006', '24554806', '24502004']},

    {'id': 'pCuv_P5A_9oh', 'name': 'Ängelholm', 'type': 'municipality', 'code': '1292', 'region_code': '12',
     'region_name': 'Skåne län', 'hits': 29,
     'ad_ids': ['24648062', '24646052', '24645263', '24644897', '24643870', '24643102', '24642941', '24641238',
                '24641268', '24635578']},

    {'id': 'bP5q_53x_aqJ', 'name': 'Hässleholm', 'type': 'municipality', 'code': '1293', 'region_code': '12',
     'region_name': 'Skåne län', 'hits': 20,
     'ad_ids': ['24645166', '24639356', '24625756', '24621467', '24614517', '24615762', '24610880', '24605726',
                '24588549', '24597762']},

    {'id': 'ocMw_Rz5_B1L', 'name': 'Kil', 'type': 'municipality', 'code': '1715', 'region_code': '17',
     'region_name': 'Värmlands län', 'hits': 3, 'ad_ids': ['24649278', '24649261', '24492496']},

    {'id': 'N5HQ_hfp_7Rm', 'name': 'Eda', 'type': 'municipality', 'code': '1730', 'region_code': '17',
     'region_name': 'Värmlands län', 'hits': 8,
     'ad_ids': ['24650359', '24649389', '24641459', '24638309', '24625958', '24625946', '24621089', '24601242']},

    {'id': 'hQdb_zn9_Sok', 'name': 'Torsby', 'type': 'municipality', 'code': '1737', 'region_code': '17',
     'region_name': 'Värmlands län', 'hits': 6,
     'ad_ids': ['24649740', '24649672', '24641489', '24622563', '24547171', '24539768']},

    {'id': 'x5qW_BXr_aut', 'name': 'Hammarö', 'type': 'municipality', 'code': '1761', 'region_code': '17',
     'region_name': 'Värmlands län', 'hits': 2, 'ad_ids': ['24612858', '24606451']},

    {'id': 'x73h_7rW_mXN', 'name': 'Munkfors', 'type': 'municipality', 'code': '1762', 'region_code': '17',
     'region_name': 'Värmlands län', 'hits': 1, 'ad_ids': ['24546455']},

    {'id': 'xnEt_JN3_GkA', 'name': 'Forshaga', 'type': 'municipality', 'code': '1763', 'region_code': '17',
     'region_name': 'Värmlands län', 'hits': 2, 'ad_ids': ['24612399', '24602480']},

    {'id': 'PSNt_P95_x6q', 'name': 'Grums', 'type': 'municipality', 'code': '1764', 'region_code': '17',
     'region_name': 'Värmlands län', 'hits': 2, 'ad_ids': ['24539560', '24566552']},

    {'id': 'ymBu_aFc_QJA', 'name': 'Årjäng', 'type': 'municipality', 'code': '1765', 'region_code': '17',
     'region_name': 'Värmlands län', 'hits': 1, 'ad_ids': ['24621703']},

    {'id': 'oqNH_cnJ_Tdi', 'name': 'Sunne', 'type': 'municipality', 'code': '1766', 'region_code': '17',
     'region_name': 'Värmlands län', 'hits': 4, 'ad_ids': ['24642867', '24622363', '24601636', '24590484']},

    {'id': 'hRDj_PoV_sFU', 'name': 'Karlstad', 'type': 'municipality', 'code': '1780', 'region_code': '17',
     'region_name': 'Värmlands län', 'hits': 49,
     'ad_ids': ['24650860', '24645012', '24642294', '24641834', '24641215', '24641140', '24639997', '24637408',
                '24635991', '24635497']},

    {'id': 'SVQS_uwJ_m2B', 'name': 'Kristinehamn', 'type': 'municipality', 'code': '1781', 'region_code': '17',
     'region_name': 'Värmlands län', 'hits': 5, 'ad_ids': ['24649351', '24646057', '24617982', '24610610', '24535779']},

    {'id': 'UXir_vKD_FuW', 'name': 'Filipstad', 'type': 'municipality', 'code': '1782', 'region_code': '17',
     'region_name': 'Värmlands län', 'hits': 0, 'ad_ids': []},

    {'id': 'qk9a_g5U_sAH', 'name': 'Hagfors', 'type': 'municipality', 'code': '1783', 'region_code': '17',
     'region_name': 'Värmlands län', 'hits': 3, 'ad_ids': ['24632958', '24614330', '24504796']},

    {'id': 'yGue_F32_wev', 'name': 'Arvika', 'type': 'municipality', 'code': '1784', 'region_code': '17',
     'region_name': 'Värmlands län', 'hits': 5, 'ad_ids': ['24626919', '24595432', '24594812', '24591333', '24532373']},

    {'id': 'wmxQ_Guc_dsy', 'name': 'Säffle', 'type': 'municipality', 'code': '1785', 'region_code': '17',
     'region_name': 'Värmlands län', 'hits': 5, 'ad_ids': ['24649317', '24612041', '24574208', '24558595', '24381921']},

    {'id': '4eS9_HX1_M7V', 'name': 'Vansbro', 'type': 'municipality', 'code': '2021', 'region_code': '20',
     'region_name': 'Dalarnas län', 'hits': 2, 'ad_ids': ['24602964', '24464433']},

    {'id': 'FPCd_poj_3tq', 'name': 'Malung-Sälen', 'type': 'municipality', 'code': '2023', 'region_code': '20',
     'region_name': 'Dalarnas län', 'hits': 5, 'ad_ids': ['24649158', '24617319', '24608124', '24606239', '24544803']},

    {'id': 'Nn7p_W3Z_y68', 'name': 'Gagnef', 'type': 'municipality', 'code': '2026', 'region_code': '20',
     'region_name': 'Dalarnas län', 'hits': 1, 'ad_ids': ['24649209']},

    {'id': '7Zsu_ant_gcn', 'name': 'Leksand', 'type': 'municipality', 'code': '2029', 'region_code': '20',
     'region_name': 'Dalarnas län', 'hits': 3, 'ad_ids': ['24640824', '24626128', '24571716']},

    {'id': 'Jy3D_2ux_dg8', 'name': 'Rättvik', 'type': 'municipality', 'code': '2031', 'region_code': '20',
     'region_name': 'Dalarnas län', 'hits': 4, 'ad_ids': ['24647031', '24647028', '24621580', '24595880']},

    {'id': 'CRyF_5Jg_4ht', 'name': 'Orsa', 'type': 'municipality', 'code': '2034', 'region_code': '20',
     'region_name': 'Dalarnas län', 'hits': 6,
     'ad_ids': ['24629334', '24608617', '24566875', '24561183', '24523938', '24488375']},

    {'id': 'cZtt_qGo_oBr', 'name': 'Älvdalen', 'type': 'municipality', 'code': '2039', 'region_code': '20',
     'region_name': 'Dalarnas län', 'hits': 4, 'ad_ids': ['24626713', '24621302', '24576229', '24576225']},

    {'id': '5zZX_8FH_Sbq', 'name': 'Smedjebacken', 'type': 'municipality', 'code': '2061', 'region_code': '20',
     'region_name': 'Dalarnas län', 'hits': 3, 'ad_ids': ['24645686', '24645204', '24593349']},

    {'id': 'UGcC_kYx_fTs', 'name': 'Mora', 'type': 'municipality', 'code': '2062', 'region_code': '20',
     'region_name': 'Dalarnas län', 'hits': 12,
     'ad_ids': ['24648097', '24624498', '24623037', '24623017', '24593694', '24587438', '24585960', '24572253',
                '24548037', '24539727']},

    {'id': 'N1wJ_Cuu_7Cs', 'name': 'Falun', 'type': 'municipality', 'code': '2080', 'region_code': '20',
     'region_name': 'Dalarnas län', 'hits': 33,
     'ad_ids': ['24649195', '24646230', '24642403', '24640180', '24639093', '24637700', '24633983', '24628486',
                '24622858', '24621668']},

    {'id': 'cpya_jJg_pGp', 'name': 'Borlänge', 'type': 'municipality', 'code': '2081', 'region_code': '20',
     'region_name': 'Dalarnas län', 'hits': 22,
     'ad_ids': ['24648735', '24645716', '24643955', '24640978', '24638705', '24634803', '24627930', '24624426',
                '24620908', '24615413']},

    {'id': 'c3Zx_jBf_CqF', 'name': 'Säter', 'type': 'municipality', 'code': '2082', 'region_code': '20',
     'region_name': 'Dalarnas län', 'hits': 3, 'ad_ids': ['24646152', '24564510', '24638537']},

    {'id': 'DE9u_V4K_Z1S', 'name': 'Hedemora', 'type': 'municipality', 'code': '2083', 'region_code': '20',
     'region_name': 'Dalarnas län', 'hits': 5, 'ad_ids': ['24617875', '24596329', '24595146', '24577346', '24518006']},

    {'id': 'Szbq_2fg_ydQ', 'name': 'Avesta', 'type': 'municipality', 'code': '2084', 'region_code': '20',
     'region_name': 'Dalarnas län', 'hits': 4, 'ad_ids': ['24643237', '24616778', '24612778', '24596510']},

    {'id': 'Ny2b_2bo_7EL', 'name': 'Ludvika', 'type': 'municipality', 'code': '2085', 'region_code': '20',
     'region_name': 'Dalarnas län', 'hits': 15,
     'ad_ids': ['24641952', '24640038', '24636562', '24636403', '24636399', '24636392', '24627279', '24618666',
                '24608534', '24607134']},

    {'id': 'GEvW_wKy_A9H', 'name': 'Ockelbo', 'type': 'municipality', 'code': '2101', 'region_code': '21',
     'region_name': 'Gävleborgs län', 'hits': 3, 'ad_ids': ['24647677', '24644885', '24636701']},

    {'id': 'yuNd_3bg_ttc', 'name': 'Hofors', 'type': 'municipality', 'code': '2104', 'region_code': '21',
     'region_name': 'Gävleborgs län', 'hits': 2, 'ad_ids': ['24643718', '24638172']},

    {'id': 'JPSe_mUQ_NDs', 'name': 'Ovanåker', 'type': 'municipality', 'code': '2121', 'region_code': '21',
     'region_name': 'Gävleborgs län', 'hits': 3, 'ad_ids': ['24648796', '24626141', '24588647']},

    {'id': 'fFeF_RCz_Tm5', 'name': 'Nordanstig', 'type': 'municipality', 'code': '2132', 'region_code': '21',
     'region_name': 'Gävleborgs län', 'hits': 0, 'ad_ids': []},

    {'id': '63iQ_V6F_REB', 'name': 'Ljusdal', 'type': 'municipality', 'code': '2161', 'region_code': '21',
     'region_name': 'Gävleborgs län', 'hits': 3, 'ad_ids': ['24624560', '24621604', '24604812']},

    {'id': 'qk8Y_2b6_82D', 'name': 'Gävle', 'type': 'municipality', 'code': '2180', 'region_code': '21',
     'region_name': 'Gävleborgs län', 'hits': 61,
     'ad_ids': ['24648286', '24647457', '24645453', '24643119', '24641947', '24641752', '24641744', '24639606',
                '24639443', '24638181']},

    {'id': 'BbdN_xLB_k6s', 'name': 'Sandviken', 'type': 'municipality', 'code': '2181', 'region_code': '21',
     'region_name': 'Gävleborgs län', 'hits': 11,
     'ad_ids': ['24640410', '24639185', '24630586', '24610267', '24602729', '24587145', '24586302', '24578542',
                '24576851', '24558652']},

    {'id': 'JauG_nz5_7mu', 'name': 'Söderhamn', 'type': 'municipality', 'code': '2182', 'region_code': '21',
     'region_name': 'Gävleborgs län', 'hits': 6,
     'ad_ids': ['24640054', '24616079', '24614547', '24595502', '24503253', '24488845']},

    {'id': 'KxjG_ig5_exF', 'name': 'Bollnäs', 'type': 'municipality', 'code': '2183', 'region_code': '21',
     'region_name': 'Gävleborgs län', 'hits': 12,
     'ad_ids': ['24647491', '24623857', '24623859', '24632941', '24631240', '24613810', '24612003', '24590238',
                '24590045', '24548369']},

    {'id': 'Utks_mwF_axY', 'name': 'Hudiksvall', 'type': 'municipality', 'code': '2184', 'region_code': '21',
     'region_name': 'Gävleborgs län', 'hits': 26,
     'ad_ids': ['24650607', '24650499', '24649045', '24648589', '24641095', '24641643', '24641698', '24647095',
                '24646916', '24645190']},

    {'id': 'swVa_cyS_EMN', 'name': 'Ånge', 'type': 'municipality', 'code': '2260', 'region_code': '22',
     'region_name': 'Västernorrlands län', 'hits': 4, 'ad_ids': ['24639288', '24628389', '24610700', '24460553']},

    {'id': 'oJ8D_rq6_kjt', 'name': 'Timrå', 'type': 'municipality', 'code': '2262', 'region_code': '22',
     'region_name': 'Västernorrlands län', 'hits': 5,
     'ad_ids': ['24649312', '24628388', '24620973', '24579351', '24504810']},

    {'id': 'uYRx_AdM_r4A', 'name': 'Härnösand', 'type': 'municipality', 'code': '2280', 'region_code': '22',
     'region_name': 'Västernorrlands län', 'hits': 14,
     'ad_ids': ['24649670', '24634810', '24626434', '24626359', '24610521', '24604584', '24599753', '24595015',
                '24588000', '24568790']},

    {'id': 'dJbx_FWY_tK6', 'name': 'Sundsvall', 'type': 'municipality', 'code': '2281', 'region_code': '22',
     'region_name': 'Västernorrlands län', 'hits': 58,
     'ad_ids': ['24650752', '24650176', '24650130', '24650080', '24649995', '24649952', '24648358', '24646387',
                '24645570', '24645032']},

    {'id': 'yR8g_7Jz_HBZ', 'name': 'Kramfors', 'type': 'municipality', 'code': '2282', 'region_code': '22',
     'region_name': 'Västernorrlands län', 'hits': 8,
     'ad_ids': ['24649705', '24633992', '24633462', '24627834', '24587306', '24582328', '24574236', '24550420']},

    {'id': 'v5y4_YPe_TMZ', 'name': 'Sollefteå', 'type': 'municipality', 'code': '2283', 'region_code': '22',
     'region_name': 'Västernorrlands län', 'hits': 9,
     'ad_ids': ['24649400', '24649380', '24642982', '24642980', '24634683', '24605190', '24588189', '24540108',
                '24455320']},

    {'id': 'zBmE_n6s_MnQ', 'name': 'Örnsköldsvik', 'type': 'municipality', 'code': '2284', 'region_code': '22',
     'region_name': 'Västernorrlands län', 'hits': 23,
     'ad_ids': ['24650185', '24649663', '24648830', '24648370', '24646067', '24643411', '24641851', '24634399',
                '24632450', '24624920']},

    {'id': 'Voto_egJ_FZP', 'name': 'Ragunda', 'type': 'municipality', 'code': '2303', 'region_code': '23',
     'region_name': 'Jämtlands län', 'hits': 0, 'ad_ids': []},

    {'id': 'eNSc_Nj1_CDP', 'name': 'Bräcke', 'type': 'municipality', 'code': '2305', 'region_code': '23',
     'region_name': 'Jämtlands län', 'hits': 1, 'ad_ids': ['24615354']},

    {'id': 'yurW_aLE_4ga', 'name': 'Krokom', 'type': 'municipality', 'code': '2309', 'region_code': '23',
     'region_name': 'Jämtlands län', 'hits': 0, 'ad_ids': []},

    {'id': 'ppjq_Eci_Wz9', 'name': 'Strömsund', 'type': 'municipality', 'code': '2313', 'region_code': '23',
     'region_name': 'Jämtlands län', 'hits': 6,
     'ad_ids': ['24646613', '24635521', '24634425', '24611237', '24608422', '24566029']},

    {'id': 'D7ax_CXP_6r1', 'name': 'Åre', 'type': 'municipality', 'code': '2321', 'region_code': '23',
     'region_name': 'Jämtlands län', 'hits': 2, 'ad_ids': ['24587180', '24572426']},

    {'id': 'gRNJ_hVW_Gpg', 'name': 'Berg', 'type': 'municipality', 'code': '2326', 'region_code': '23',
     'region_name': 'Jämtlands län', 'hits': 1, 'ad_ids': ['24626189']},

    {'id': 'j35Q_VKL_NiM', 'name': 'Härjedalen', 'type': 'municipality', 'code': '2361', 'region_code': '23',
     'region_name': 'Jämtlands län', 'hits': 8,
     'ad_ids': ['24650648', '24649337', '24648475', '24626268', '24615961', '24600435', '24565037', '24560583']},

    {'id': 'Vt7P_856_WZS', 'name': 'Östersund', 'type': 'municipality', 'code': '2380', 'region_code': '23',
     'region_name': 'Jämtlands län', 'hits': 36,
     'ad_ids': ['24650954', '24650862', '24648805', '24647118', '24640165', '24637613', '24634928', '24634409',
                '24633737', '24631136']},

    {'id': 'wMab_4Zs_wpM', 'name': 'Nordmaling', 'type': 'municipality', 'code': '2401', 'region_code': '24',
     'region_name': 'Västerbottens län', 'hits': 2, 'ad_ids': ['24588012', '24494081']},

    {'id': 'vQkf_tw2_CmR', 'name': 'Bjurholm', 'type': 'municipality', 'code': '2403', 'region_code': '24',
     'region_name': 'Västerbottens län', 'hits': 0, 'ad_ids': []},

    {'id': 'izT6_zWu_tta', 'name': 'Vindeln', 'type': 'municipality', 'code': '2404', 'region_code': '24',
     'region_name': 'Västerbottens län', 'hits': 4, 'ad_ids': ['24610047', '24595064', '24585738', '24581311']},

    {'id': 'XmpG_vPQ_K7T', 'name': 'Norsjö', 'type': 'municipality', 'code': '2417', 'region_code': '24',
     'region_name': 'Västerbottens län', 'hits': 1, 'ad_ids': ['24577456']},

    {'id': 'gQgT_BAk_fMu', 'name': 'Storuman', 'type': 'municipality', 'code': '2421', 'region_code': '24',
     'region_name': 'Västerbottens län', 'hits': 3, 'ad_ids': ['24648927', '24612310', '24551322']},

    {'id': 'tSkf_Tbn_rHk', 'name': 'Dorotea', 'type': 'municipality', 'code': '2425', 'region_code': '24',
     'region_name': 'Västerbottens län', 'hits': 2, 'ad_ids': ['24648188', '24551059']},

    {'id': 'utQc_6xq_Dfm', 'name': 'Vännäs', 'type': 'municipality', 'code': '2460', 'region_code': '24',
     'region_name': 'Västerbottens län', 'hits': 1, 'ad_ids': ['24477260']},

    {'id': 'tUnW_mFo_Hvi', 'name': 'Vilhelmina', 'type': 'municipality', 'code': '2462', 'region_code': '24',
     'region_name': 'Västerbottens län', 'hits': 3, 'ad_ids': ['24650955', '24646598', '24389476']},

    {'id': 'xLdL_tMA_JJv', 'name': 'Åsele', 'type': 'municipality', 'code': '2463', 'region_code': '24',
     'region_name': 'Västerbottens län', 'hits': 2, 'ad_ids': ['24635862', '24596209']},

    {'id': 'QiGt_BLu_amP', 'name': 'Umeå', 'type': 'municipality', 'code': '2480', 'region_code': '24',
     'region_name': 'Västerbottens län', 'hits': 61,
     'ad_ids': ['24651085', '24650971', '24649245', '24649034', '24648882', '24647955', '24647299', '24646501',
                '24643021', '24642426']},

    {'id': '7rpN_naz_3Uz', 'name': 'Lycksele', 'type': 'municipality', 'code': '2481', 'region_code': '24',
     'region_name': 'Västerbottens län', 'hits': 5,
     'ad_ids': ['24650360', '24636787', '24624131', '24584326', '24461658']},

    {'id': 'kicB_LgH_2Dk', 'name': 'Skellefteå', 'type': 'municipality', 'code': '2482', 'region_code': '24',
     'region_name': 'Västerbottens län', 'hits': 26,
     'ad_ids': ['24650579', '24650245', '24647140', '24646439', '24644842', '24644817', '24641617', '24639673',
                '24633366', '24629478']},

    {'id': 'A5WX_XVo_Zt6', 'name': 'Arvidsjaur', 'type': 'municipality', 'code': '2505', 'region_code': '25',
     'region_name': 'Norrbottens län', 'hits': 0, 'ad_ids': []},

    {'id': 'mp6j_2b6_1bz', 'name': 'Jokkmokk', 'type': 'municipality', 'code': '2510', 'region_code': '25',
     'region_name': 'Norrbottens län', 'hits': 2, 'ad_ids': ['24636604', '24629767']},

    {'id': 'n5Sq_xxo_QWL', 'name': 'Överkalix', 'type': 'municipality', 'code': '2513', 'region_code': '25',
     'region_name': 'Norrbottens län', 'hits': 2, 'ad_ids': ['24626297', '24562294']},

    {'id': 'cUyN_C9V_HLU', 'name': 'Kalix', 'type': 'municipality', 'code': '2514', 'region_code': '25',
     'region_name': 'Norrbottens län', 'hits': 7,
     'ad_ids': ['24650117', '24647684', '24617672', '24610596', '24598334', '24578326', '24549825']},

    {'id': 'ehMP_onv_Chk', 'name': 'Övertorneå', 'type': 'municipality', 'code': '2518', 'region_code': '25',
     'region_name': 'Norrbottens län', 'hits': 2, 'ad_ids': ['24615259', '24602364']},

    {'id': 'dHMF_72G_4NM', 'name': 'Pajala', 'type': 'municipality', 'code': '2521', 'region_code': '25',
     'region_name': 'Norrbottens län', 'hits': 5,
     'ad_ids': ['24613530', '24579678', '24421022', '24421085', '24421026']},

    {'id': '6R2u_zkb_uoS', 'name': 'Gällivare', 'type': 'municipality', 'code': '2523', 'region_code': '25',
     'region_name': 'Norrbottens län', 'hits': 12,
     'ad_ids': ['24650616', '24647793', '24646122', '24646015', '24645490', '24639665', '24631189', '24626915',
                '24620850', '24613056']},

    {'id': '14WF_zh1_W3y', 'name': 'Älvsbyn', 'type': 'municipality', 'code': '2560', 'region_code': '25',
     'region_name': 'Norrbottens län', 'hits': 1, 'ad_ids': ['24648879']},

    {'id': 'CXbY_gui_14v', 'name': 'Luleå', 'type': 'municipality', 'code': '2580', 'region_code': '25',
     'region_name': 'Norrbottens län', 'hits': 54,
     'ad_ids': ['24650277', '24649382', '24646536', '24646256', '24645408', '24645272', '24644929', '24642846',
                '24640988', '24637961']},

    {'id': 'umej_bP2_PpK', 'name': 'Piteå', 'type': 'municipality', 'code': '2581', 'region_code': '25',
     'region_name': 'Norrbottens län', 'hits': 21,
     'ad_ids': ['24647791', '24646569', '24646410', '24642289', '24640981', '24629378', '24627010', '24615196',
                '24614741', '24613745']},

    {'id': 'y4NQ_tnB_eVd', 'name': 'Boden', 'type': 'municipality', 'code': '2582', 'region_code': '25',
     'region_name': 'Norrbottens län', 'hits': 6,
     'ad_ids': ['24641061', '24625531', '24621825', '24593711', '24591832', '24547017']},

    {'id': 'tfRE_hXa_eq7', 'name': 'Haparanda', 'type': 'municipality', 'code': '2583', 'region_code': '25',
     'region_name': 'Norrbottens län', 'hits': 3, 'ad_ids': ['24649761', '24641448', '24585171']},

    {'id': 'biN6_UiL_Qob', 'name': 'Kiruna', 'type': 'municipality', 'code': '2584', 'region_code': '25',
     'region_name': 'Norrbottens län', 'hits': 18,
     'ad_ids': ['24649230', '24637663', '24620869', '24616612', '24609586', '24609582', '24607586', '24605447',
                '24604398', '24594403']},

    {'id': 'y9HE_XD7_WaD', 'name': 'Aneby', 'type': 'municipality', 'code': '0604', 'region_code': '06',
     'region_name': 'Jönköpings län', 'hits': 2, 'ad_ids': ['24623297', '24579234']},

    {'id': '91VR_Hxi_GN4', 'name': 'Gnosjö', 'type': 'municipality', 'code': '0617', 'region_code': '06',
     'region_name': 'Jönköpings län', 'hits': 5,
     'ad_ids': ['24628681', '24604131', '24590061', '24566659', '24558133']},

    {'id': 'smXg_BXp_jTW', 'name': 'Mullsjö', 'type': 'municipality', 'code': '0642', 'region_code': '06',
     'region_name': 'Jönköpings län', 'hits': 5,
     'ad_ids': ['24643973', '24607032', '24606628', '24577681', '24537205']},

    {'id': '9zQB_3vU_BQA', 'name': 'Habo', 'type': 'municipality', 'code': '0643', 'region_code': '06',
     'region_name': 'Jönköpings län', 'hits': 1, 'ad_ids': ['24579245']},

    {'id': 'cNQx_Yqi_83Q', 'name': 'Gislaved', 'type': 'municipality', 'code': '0662', 'region_code': '06',
     'region_name': 'Jönköpings län', 'hits': 9,
     'ad_ids': ['24613432', '24626013', '24612979', '24609415', '24604594', '24601733', '24599387', '24593831',
                '24586269']},

    {'id': 'zFup_umX_LVv', 'name': 'Vaggeryd', 'type': 'municipality', 'code': '0665', 'region_code': '06',
     'region_name': 'Jönköpings län', 'hits': 5,
     'ad_ids': ['24647651', '24633078', '24629708', '24550532', '24583814']},

    {'id': 'KURg_KJF_Lwc', 'name': 'Jönköping', 'type': 'municipality', 'code': '0680', 'region_code': '06',
     'region_name': 'Jönköpings län', 'hits': 85,
     'ad_ids': ['24651032', '24650982', '24650248', '24648832', '24647954', '24647864', '24644298', '24647107',
                '24644341', '24644259']},

    {'id': 'KfXT_ySA_do2', 'name': 'Nässjö', 'type': 'municipality', 'code': '0682', 'region_code': '06',
     'region_name': 'Jönköpings län', 'hits': 10,
     'ad_ids': ['24644891', '24639953', '24633087', '24629181', '24621427', '24613566', '24610827', '24589332',
                '24575224', '24528277']},

    {'id': '6bS8_fzf_xpW', 'name': 'Värnamo', 'type': 'municipality', 'code': '0683', 'region_code': '06',
     'region_name': 'Jönköpings län', 'hits': 26,
     'ad_ids': ['24641062', '24634862', '24632296', '24631196', '24627728', '24623817', '24622843', '24620357',
                '24620014', '24614169']},

    {'id': 'L1cX_MjM_y8W', 'name': 'Sävsjö', 'type': 'municipality', 'code': '0684', 'region_code': '06',
     'region_name': 'Jönköpings län', 'hits': 7,
     'ad_ids': ['24640990', '24631381', '24612486', '24597187', '24581570', '24534133', '24468408']},

    {'id': 'xJqx_SLC_415', 'name': 'Vetlanda', 'type': 'municipality', 'code': '0685', 'region_code': '06',
     'region_name': 'Jönköpings län', 'hits': 16,
     'ad_ids': ['24643277', '24641529', '24640373', '24637057', '24628455', '24627538', '24627537', '24621685',
                '24613356', '24609736']},

    {'id': 'VacK_WF6_XVg', 'name': 'Eksjö', 'type': 'municipality', 'code': '0686', 'region_code': '06',
     'region_name': 'Jönköpings län', 'hits': 14,
     'ad_ids': ['24637053', '24632911', '24615733', '24617245', '24609055', '24599160', '24597461', '24594324',
                '24586118', '24585765']},

    {'id': 'Namm_SpC_RPG', 'name': 'Tranås', 'type': 'municipality', 'code': '0687', 'region_code': '06',
     'region_name': 'Jönköpings län', 'hits': 10,
     'ad_ids': ['24642284', '24641464', '24626417', '24623265', '24619248', '24618419', '24590114', '24499769',
                '24476521', '24433907']},

    {'id': '78cu_S5T_Pgp', 'name': 'Uppvidinge', 'type': 'municipality', 'code': '0760', 'region_code': '07',
     'region_name': 'Kronobergs län', 'hits': 3, 'ad_ids': ['24639253', '24617976', '24414696']},

    {'id': 'nXZy_1Jd_D8X', 'name': 'Lessebo', 'type': 'municipality', 'code': '0761', 'region_code': '07',
     'region_name': 'Kronobergs län', 'hits': 1, 'ad_ids': ['24538499']},

    {'id': 'qz8Q_kDz_N2Y', 'name': 'Tingsryd', 'type': 'municipality', 'code': '0763', 'region_code': '07',
     'region_name': 'Kronobergs län', 'hits': 4, 'ad_ids': ['24641348', '24624969', '24511207', '24491412']},

    {'id': 'MMph_wmN_esc', 'name': 'Alvesta', 'type': 'municipality', 'code': '0764', 'region_code': '07',
     'region_name': 'Kronobergs län', 'hits': 5,
     'ad_ids': ['24650556', '24621112', '24617389', '24576031', '24476169']},

    {'id': 'EK6X_wZq_CQ8', 'name': 'Älmhult', 'type': 'municipality', 'code': '0765', 'region_code': '07',
     'region_name': 'Kronobergs län', 'hits': 11,
     'ad_ids': ['24649769', '24649654', '24641236', '24641229', '24638136', '24632509', '24631497', '24625110',
                '24617479', '24614602']},

    {'id': 'ZhVf_yL5_Q5g', 'name': 'Markaryd', 'type': 'municipality', 'code': '0767', 'region_code': '07',
     'region_name': 'Kronobergs län', 'hits': 10,
     'ad_ids': ['24629043', '24643980', '24643878', '24639757', '24637755', '24627553', '24618793', '24583008',
                '24553167', '24470454']},

    {'id': 'mmot_H3A_auW', 'name': 'Växjö', 'type': 'municipality', 'code': '0780', 'region_code': '07',
     'region_name': 'Kronobergs län', 'hits': 68,
     'ad_ids': ['24649803', '24649739', '24649728', '24647829', '24647003', '24644326', '24644173', '24643867',
                '24643342', '24641504']},

    {'id': 'GzKo_S48_QCm', 'name': 'Ljungby', 'type': 'municipality', 'code': '0781', 'region_code': '07',
     'region_name': 'Kronobergs län', 'hits': 11,
     'ad_ids': ['24648839', '24648771', '24645854', '24643124', '24637014', '24636725', '24635767', '24633273',
                '24624230', '24622854']},

    {'id': 'WPDh_pMr_RLZ', 'name': 'Högsby', 'type': 'municipality', 'code': '0821', 'region_code': '08',
     'region_name': 'Kalmar län', 'hits': 2, 'ad_ids': ['24640369', '24636070']},

    {'id': 'Muim_EAi_EFp', 'name': 'Mörbylånga', 'type': 'municipality', 'code': '0840', 'region_code': '08',
     'region_name': 'Kalmar län', 'hits': 9,
     'ad_ids': ['24638175', '24629762', '24627653', '24611449', '24607639', '24594093', '24572030', '24546217',
                '24488349']},

    {'id': 'AEQD_1RT_vM9', 'name': 'Hultsfred', 'type': 'municipality', 'code': '0860', 'region_code': '08',
     'region_name': 'Kalmar län', 'hits': 9,
     'ad_ids': ['24648249', '24644661', '24636959', '24627269', '24627176', '24620579', '24588831', '24527019',
                '24510303']},

    {'id': '8eEp_iz4_cNN', 'name': 'Mönsterås', 'type': 'municipality', 'code': '0861', 'region_code': '08',
     'region_name': 'Kalmar län', 'hits': 5, 'ad_ids': ['24648226', '24625464', '24618320', '24583746', '24573755']},

    {'id': '1koj_6Bg_8K6', 'name': 'Emmaboda', 'type': 'municipality', 'code': '0862', 'region_code': '08',
     'region_name': 'Kalmar län', 'hits': 2, 'ad_ids': ['24635771', '24615839']},

    {'id': 'Pnmg_SgP_uHQ', 'name': 'Kalmar', 'type': 'municipality', 'code': '0880', 'region_code': '08',
     'region_name': 'Kalmar län', 'hits': 52,
     'ad_ids': ['24649776', '24649356', '24648115', '24647817', '24647756', '24643992', '24643651', '24641992',
                '24641371', '24613487']},

    {'id': 'tUP8_hRE_NcF', 'name': 'Oskarshamn', 'type': 'municipality', 'code': '0882', 'region_code': '08',
     'region_name': 'Kalmar län', 'hits': 14,
     'ad_ids': ['24648664', '24648628', '24646920', '24645350', '24640633', '24635629', '24630203', '24628734',
                '24624215', '24617181']},

    {'id': 't7H4_S2P_3Fw', 'name': 'Västervik', 'type': 'municipality', 'code': '0883', 'region_code': '08',
     'region_name': 'Kalmar län', 'hits': 18,
     'ad_ids': ['24647144', '24641319', '24634992', '24632176', '24628636', '24618501', '24617706', '24614479',
                '24608624', '24603700']},

    {'id': 'a7hJ_zwv_2FR', 'name': 'Vimmerby', 'type': 'municipality', 'code': '0884', 'region_code': '08',
     'region_name': 'Kalmar län', 'hits': 15,
     'ad_ids': ['24642646', '24642469', '24634998', '24632703', '24628660', '24619779', '24619775', '24614093',
                '24593900', '24590275']},

    {'id': 'LY9i_qNL_kXf', 'name': 'Borgholm', 'type': 'municipality', 'code': '0885', 'region_code': '08',
     'region_name': 'Kalmar län', 'hits': 4, 'ad_ids': ['24631885', '24600006', '24575680', '24496816']},

    {'id': 'Ft9P_E8F_VLJ', 'name': 'Gotland', 'type': 'municipality', 'code': '0980', 'region_code': '09',
     'region_name': 'Gotlands län', 'hits': 32,
     'ad_ids': ['24649062', '24646081', '24645577', '24643883', '24641060', '24637605', '24637052', '24636730',
                '24631180', '24631256']},

    {'id': '3XMe_nGt_RcU', 'name': 'Hylte', 'type': 'municipality', 'code': '1315', 'region_code': '13',
     'region_name': 'Hallands län', 'hits': 7,
     'ad_ids': ['24647998', '24646997', '24645469', '24640710', '24633518', '24629087', '24575788']},

    {'id': 'kUQB_KdK_kAh', 'name': 'Halmstad', 'type': 'municipality', 'code': '1380', 'region_code': '13',
     'region_name': 'Hallands län', 'hits': 48,
     'ad_ids': ['24650491', '24649086', '24648647', '24647496', '24646397', '24646379', '24645317', '24644390',
                '24643943', '24643937']},

    {'id': 'c1iL_rqh_Zja', 'name': 'Laholm', 'type': 'municipality', 'code': '1381', 'region_code': '13',
     'region_name': 'Hallands län', 'hits': 7,
     'ad_ids': ['24637743', '24624670', '24624583', '24615475', '24614113', '24588536', '24552904']},

    {'id': 'qaJg_wMR_C8T', 'name': 'Falkenberg', 'type': 'municipality', 'code': '1382', 'region_code': '13',
     'region_name': 'Hallands län', 'hits': 26,
     'ad_ids': ['24650289', '24646671', '24644450', '24644424', '24638795', '24638078', '24635865', '24635663',
                '24630122', '24612894']},

    {'id': 'AkUx_yAq_kGr', 'name': 'Varberg', 'type': 'municipality', 'code': '1383', 'region_code': '13',
     'region_name': 'Hallands län', 'hits': 34,
     'ad_ids': ['24650646', '24648999', '24648048', '24647269', '24647256', '24646756', '24646453', '24646370',
                '24646335', '24646315']},

    {'id': '3JKV_KSK_x6z', 'name': 'Kungsbacka', 'type': 'municipality', 'code': '1384', 'region_code': '13',
     'region_name': 'Hallands län', 'hits': 16,
     'ad_ids': ['24650147', '24650006', '24640093', '24638112', '24635197', '24632626', '24630508', '24624655',
                '24622514', '24607841']},

    {'id': 'dzWW_R3G_6Eh', 'name': 'Härryda', 'type': 'municipality', 'code': '1401', 'region_code': '14',
     'region_name': 'Västra Götalands län', 'hits': 7,
     'ad_ids': ['24631009', '24621857', '24606754', '24587980', '24587111', '24568233', '24531121']},

    {'id': 'CCiR_sXa_BVW', 'name': 'Partille', 'type': 'municipality', 'code': '1402', 'region_code': '14',
     'region_name': 'Västra Götalands län', 'hits': 9,
     'ad_ids': ['24644886', '24643254', '24642579', '24642082', '24623735', '24612128', '24611787', '24502309',
                '24468685']},

    {'id': 'Zjiv_rhk_oJK', 'name': 'Öckerö', 'type': 'municipality', 'code': '1407', 'region_code': '14',
     'region_name': 'Västra Götalands län', 'hits': 2, 'ad_ids': ['24616589', '24601739']},

    {'id': 'wHrG_FBH_hoD', 'name': 'Stenungsund', 'type': 'municipality', 'code': '1415', 'region_code': '14',
     'region_name': 'Västra Götalands län', 'hits': 5,
     'ad_ids': ['24635632', '24628237', '24612130', '24606880', '24587131']},

    {'id': 'TbL3_HmF_gnx', 'name': 'Tjörn', 'type': 'municipality', 'code': '1419', 'region_code': '14',
     'region_name': 'Västra Götalands län', 'hits': 2, 'ad_ids': ['24594903', '24590040']},

    {'id': 'tmAp_ykH_N6k', 'name': 'Orust', 'type': 'municipality', 'code': '1421', 'region_code': '14',
     'region_name': 'Västra Götalands län', 'hits': 5,
     'ad_ids': ['24642072', '24596275', '24586295', '24541260', '24519447']},

    {'id': 'aKkp_sEX_cVM', 'name': 'Sotenäs', 'type': 'municipality', 'code': '1427', 'region_code': '14',
     'region_name': 'Västra Götalands län', 'hits': 6,
     'ad_ids': ['24612938', '24564119', '24535667', '24520889', '24500709', '24485653']},

    {'id': '96Dh_3sQ_RFb', 'name': 'Munkedal', 'type': 'municipality', 'code': '1430', 'region_code': '14',
     'region_name': 'Västra Götalands län', 'hits': 3, 'ad_ids': ['24641856', '24624344', '24595408']},

    {'id': 'qffn_qY4_DLk', 'name': 'Tanum', 'type': 'municipality', 'code': '1435', 'region_code': '14',
     'region_name': 'Västra Götalands län', 'hits': 8,
     'ad_ids': ['24627761', '24623563', '24623580', '24621916', '24610846', '24608306', '24607156', '24600587']},

    {'id': 'NMc9_oEm_yxy', 'name': 'Dals-Ed', 'type': 'municipality', 'code': '1438', 'region_code': '14',
     'region_name': 'Västra Götalands län', 'hits': 0, 'ad_ids': []},

    {'id': 'kCHb_icw_W5E', 'name': 'Färgelanda', 'type': 'municipality', 'code': '1439', 'region_code': '14',
     'region_name': 'Västra Götalands län', 'hits': 2, 'ad_ids': ['24639807', '24556773']},

    {'id': '17Ug_Btv_mBr', 'name': 'Ale', 'type': 'municipality', 'code': '1440', 'region_code': '14',
     'region_name': 'Västra Götalands län', 'hits': 3, 'ad_ids': ['24636146', '24628807', '24616322']},

    {'id': 'yHV7_2Y6_zQx', 'name': 'Lerum', 'type': 'municipality', 'code': '1441', 'region_code': '14',
     'region_name': 'Västra Götalands län', 'hits': 4, 'ad_ids': ['24650394', '24620026', '24603503', '24588967']},

    {'id': 'NfFx_5jj_ogg', 'name': 'Vårgårda', 'type': 'municipality', 'code': '1442', 'region_code': '14',
     'region_name': 'Västra Götalands län', 'hits': 5,
     'ad_ids': ['24648984', '24648901', '24613448', '24595489', '24497926']},

    {'id': 'ypAQ_vTD_KLU', 'name': 'Bollebygd', 'type': 'municipality', 'code': '1443', 'region_code': '14',
     'region_name': 'Västra Götalands län', 'hits': 1, 'ad_ids': ['24576508']},

    {'id': 'ZNZy_Hh5_gSW', 'name': 'Grästorp', 'type': 'municipality', 'code': '1444', 'region_code': '14',
     'region_name': 'Västra Götalands län', 'hits': 0, 'ad_ids': []},

    {'id': 'ZzEA_2Fg_Pt2', 'name': 'Essunga', 'type': 'municipality', 'code': '1445', 'region_code': '14',
     'region_name': 'Västra Götalands län', 'hits': 2, 'ad_ids': ['24626743', '24523562']},

    {'id': 'e413_94L_hdh', 'name': 'Karlsborg', 'type': 'municipality', 'code': '1446', 'region_code': '14',
     'region_name': 'Västra Götalands län', 'hits': 0, 'ad_ids': []},

    {'id': 'SEje_LdC_9qN', 'name': 'Tranemo', 'type': 'municipality', 'code': '1452', 'region_code': '14',
     'region_name': 'Västra Götalands län', 'hits': 6,
     'ad_ids': ['24645375', '24615877', '24608246', '24595425', '24595398', '24589648']},

    {'id': 'hejM_Jct_XJk', 'name': 'Bengtsfors', 'type': 'municipality', 'code': '1460', 'region_code': '14',
     'region_name': 'Västra Götalands län', 'hits': 5,
     'ad_ids': ['24628976', '24627769', '24606787', '24606679', '24606575']},

    {'id': 'tt1B_7rH_vhG', 'name': 'Mellerud', 'type': 'municipality', 'code': '1461', 'region_code': '14',
     'region_name': 'Västra Götalands län', 'hits': 4, 'ad_ids': ['24644889', '24638627', '24615086', '24614249']},

    {'id': 'YQcE_SNB_Tv3', 'name': 'Lilla Edet', 'type': 'municipality', 'code': '1462', 'region_code': '14',
     'region_name': 'Västra Götalands län', 'hits': 4, 'ad_ids': ['24642553', '24632874', '24622421', '24610751']},

    {'id': '7HAb_9or_eFM', 'name': 'Mark', 'type': 'municipality', 'code': '1463', 'region_code': '14',
     'region_name': 'Västra Götalands län', 'hits': 14,
     'ad_ids': ['24648862', '24646848', '24646268', '24641228', '24630473', '24636557', '24636719', '24634116',
                '24622756', '24605393']},

    {'id': 'rZWC_pXf_ySZ', 'name': 'Svenljunga', 'type': 'municipality', 'code': '1465', 'region_code': '14',
     'region_name': 'Västra Götalands län', 'hits': 6,
     'ad_ids': ['24643128', '24623466', '24623505', '24615913', '24601846', '24557637']},

    {'id': 'J116_VFs_cg6', 'name': 'Herrljunga', 'type': 'municipality', 'code': '1466', 'region_code': '14',
     'region_name': 'Västra Götalands län', 'hits': 6,
     'ad_ids': ['24641573', '24636142', '24617273', '24602256', '24589027', '24344092']},

    {'id': 'fbHM_yhA_BqS', 'name': 'Vara', 'type': 'municipality', 'code': '1470', 'region_code': '14',
     'region_name': 'Västra Götalands län', 'hits': 9,
     'ad_ids': ['24629592', '24623071', '24600787', '24581780', '24573835', '24573476', '24564712', '24560074',
                '24550766']},

    {'id': 'txzq_PQY_FGi', 'name': 'Götene', 'type': 'municipality', 'code': '1471', 'region_code': '14',
     'region_name': 'Västra Götalands län', 'hits': 4, 'ad_ids': ['24640957', '24645273', '24482625', '24435186']},

    {'id': 'aLFZ_NHw_atB', 'name': 'Tibro', 'type': 'municipality', 'code': '1472', 'region_code': '14',
     'region_name': 'Västra Götalands län', 'hits': 8,
     'ad_ids': ['24645712', '24644627', '24629456', '24602786', '24606277', '24573354', '24571196', '24529612']},

    {'id': 'a15F_gAH_pn6', 'name': 'Töreboda', 'type': 'municipality', 'code': '1473', 'region_code': '14',
     'region_name': 'Västra Götalands län', 'hits': 4, 'ad_ids': ['24627789', '24606276', '24602447', '24522711']},

    {'id': 'PVZL_BQT_XtL', 'name': 'Göteborg', 'type': 'municipality', 'code': '1480', 'region_code': '14',
     'region_name': 'Västra Götalands län', 'hits': 366,
     'ad_ids': ['24681247', '24676773', '24650996', '24650965', '24650945', '24650584', '24650192', '24649925',
                '24649915', '24649703']},

    {'id': 'mc45_ki9_Bv3', 'name': 'Mölndal', 'type': 'municipality', 'code': '1481', 'region_code': '14',
     'region_name': 'Västra Götalands län', 'hits': 34,
     'ad_ids': ['24650280', '24647664', '24645598', '24645052', '24644887', '24642987', '24642642', '24641578',
                '24635304', '24632723']},

    {'id': 'ZkZf_HbK_Mcr', 'name': 'Kungälv', 'type': 'municipality', 'code': '1482', 'region_code': '14',
     'region_name': 'Västra Götalands län', 'hits': 10,
     'ad_ids': ['24649688', '24636513', '24631373', '24599794', '24597606', '24594445', '24587159', '24487095',
                '24361598', '24329253']},

    {'id': 'z2cX_rjC_zFo', 'name': 'Lysekil', 'type': 'municipality', 'code': '1484', 'region_code': '14',
     'region_name': 'Västra Götalands län', 'hits': 5,
     'ad_ids': ['24635267', '24618391', '24605089', '24566449', '24554224']},

    {'id': 'xQc2_SzA_rHK', 'name': 'Uddevalla', 'type': 'municipality', 'code': '1485', 'region_code': '14',
     'region_name': 'Västra Götalands län', 'hits': 19,
     'ad_ids': ['24649569', '24648333', '24641531', '24640747', '24639878', '24635023', '24633830', '24627768',
                '24624785', '24619542']},

    {'id': 'PAxT_FLT_3Kq', 'name': 'Strömstad', 'type': 'municipality', 'code': '1486', 'region_code': '14',
     'region_name': 'Västra Götalands län', 'hits': 3, 'ad_ids': ['24645683', '24640280', '24612600']},

    {'id': 'THif_q6H_MjG', 'name': 'Vänersborg', 'type': 'municipality', 'code': '1487', 'region_code': '14',
     'region_name': 'Västra Götalands län', 'hits': 12,
     'ad_ids': ['24643619', '24641986', '24626856', '24624150', '24615861', '24615817', '24615329', '24592505',
                '24598715', '24598400']},

    {'id': 'CSy8_41F_YvX', 'name': 'Trollhättan', 'type': 'municipality', 'code': '1488', 'region_code': '14',
     'region_name': 'Västra Götalands län', 'hits': 21,
     'ad_ids': ['24646037', '24639347', '24627262', '24624949', '24615219', '24615218', '24609960', '24607976',
                '24606466', '24604829']},

    {'id': 'UQ75_1eU_jaC', 'name': 'Alingsås', 'type': 'municipality', 'code': '1489', 'region_code': '14',
     'region_name': 'Västra Götalands län', 'hits': 19,
     'ad_ids': ['24641757', '24632495', '24631417', '24629793', '24626567', '24624598', '24618097', '24615747',
                '24607775', '24607729']},

    {'id': 'TpRZ_bFL_jhL', 'name': 'Borås', 'type': 'municipality', 'code': '1490', 'region_code': '14',
     'region_name': 'Västra Götalands län', 'hits': 74,
     'ad_ids': ['24674944', '24649696', '24649621', '24647020', '24641526', '24645912', '24644338', '24643016',
                '24641652', '24641101']},

    {'id': 'an4a_8t2_Zpd', 'name': 'Ulricehamn', 'type': 'municipality', 'code': '1491', 'region_code': '14',
     'region_name': 'Västra Götalands län', 'hits': 5,
     'ad_ids': ['24650721', '24645207', '24643748', '24639136', '24570351']},

    {'id': 'M1UC_Cnf_r7g', 'name': 'Åmål', 'type': 'municipality', 'code': '1492', 'region_code': '14',
     'region_name': 'Västra Götalands län', 'hits': 5,
     'ad_ids': ['24633826', '24629622', '24627722', '24593424', '24541403']},

    {'id': 'Lzpu_thX_Wpa', 'name': 'Mariestad', 'type': 'municipality', 'code': '1493', 'region_code': '14',
     'region_name': 'Västra Götalands län', 'hits': 3, 'ad_ids': ['24646647', '24607084', '24606384']},

    {'id': 'FN1Y_asc_D8y', 'name': 'Lidköping', 'type': 'municipality', 'code': '1494', 'region_code': '14',
     'region_name': 'Västra Götalands län', 'hits': 9,
     'ad_ids': ['24641522', '24624546', '24624090', '24606663', '24606030', '24586133', '24585588', '24516242',
                '24417820']},

    {'id': 'k1SK_gxg_dW4', 'name': 'Skara', 'type': 'municipality', 'code': '1495', 'region_code': '14',
     'region_name': 'Västra Götalands län', 'hits': 13,
     'ad_ids': ['24644221', '24639811', '24639532', '24631244', '24616809', '24614229', '24606824', '24600741',
                '24595277', '24552003']},

    {'id': 'fqAy_4ji_Lz2', 'name': 'Skövde', 'type': 'municipality', 'code': '1496', 'region_code': '14',
     'region_name': 'Västra Götalands län', 'hits': 31,
     'ad_ids': ['24649472', '24645228', '24642392', '24642391', '24638912', '24636831', '24636136', '24635424',
                '24633671', '24628305']},

    {'id': 'YbFS_34r_K2v', 'name': 'Hjo', 'type': 'municipality', 'code': '1497', 'region_code': '14',
     'region_name': 'Västra Götalands län', 'hits': 5,
     'ad_ids': ['24624908', '24585725', '24583812', '24573430', '24483113']},

    {'id': 'ZySF_gif_zE4', 'name': 'Falköping', 'type': 'municipality', 'code': '1499', 'region_code': '14',
     'region_name': 'Västra Götalands län', 'hits': 8,
     'ad_ids': ['24650374', '24640803', '24636373', '24635597', '24630448', '24628992', '24620333', '24617776']},

]
