import random
from tests.test_resources.helper import compare_two_lists
from tests.test_resources.historical_new_id_combinations import ok_new_id_combinations
from tests.test_resources.historical_duplicates import duplicate_id_combinations
from tests.test_resources.historical_ids_2017 import id_combinations_2017

all_keys = ['occupation-name', 'occupation-group', 'occupation-field', 'employment-type', 'country', 'region',
            'municipality', 'language', 'skill']

all_stats = ['occupation-name', 'occupation-group', 'municipality', 'region', 'country']

min_values = 8
min_values_employment_type = 4


def compare_keys(stats):
    keys = []
    for item in stats:
        keys.append(item)
    compare_two_lists(keys, all_keys)


def check_default_values(stats):
    for key, value in stats.items():
        expected = 8  # default
        if key == 'employment-type':
            expected = 4
        assert len(value) >= expected, f"Expected {expected} but got {len(value)} Key: {key}, Value: {value}"


expected_top_values = {
    'occupation': 392,
    'occupation_group': 396,
    'occupation_field': 971,
    'employment_type': 4389,
    'country': 4999,
    'region': 1227,
    'municipality': 775,
    'language': 490,
    'skill': 23}


def random_duplicates() -> list:
    return _random_elements(duplicate_id_combinations)


def random_ids_from_2017() -> list:
    return _random_elements(id_combinations_2017)


def random_non_duplicates() -> list:
    return _random_elements(ok_new_id_combinations)


def _random_elements(input_list: list) -> list:
    return random.sample(input_list, 10)


def get_new_ids_from_409_response(response: dict) -> list:
    new_ids = []
    for match in response['message']['matches']:
        new_ids.append(match['id'])
    return sorted(new_ids)


def _check_ad_from_search_result(result: dict) -> dict:
    assert len(result["hits"]) == 1
    ad = result["hits"][0]  # there should be only one ad in the result
    assert ad["relevance"] == 1.0
    ad.pop("relevance")  # to enable comparison between search and ad/id
    return ad
