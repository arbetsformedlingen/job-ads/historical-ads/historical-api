import random
import pytest

from tests.test_resources.helper import get_historical_stats, get_search


@pytest.fixture
def random_ads():
    offset = random.randint(0, 1900)
    params = {'limit': 100, 'offset': offset}
    return get_search(params)['hits']


@pytest.fixture(scope='session')
def historical_stats():
    """
    get stats once to save time and resources
    """
    stats = get_historical_stats(params={})
    return stats
