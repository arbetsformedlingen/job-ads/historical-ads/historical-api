import pytest

from tests.test_resources.concept_ids import occupation, occupation_group, occupation_field, concept_ids_geo as geo
from tests.test_resources.helper import get_total
from tests.test_resources.test_settings import NUMBER_OF_ADS, NUMBER_OF_HISTORICAL_ADS
from common.constants import UNSPECIFIED_SWEDEN_WORKPLACE, ABROAD, REMOTE, PUBLISHED_BEFORE, PUBLISHED_AFTER, \
    EXPERIENCE_REQUIRED, OCCUPATION, OCCUPATION_GROUP, OCCUPATION_FIELD, MUNICIPALITY
from tests.test_resources.api_test_runner import run_test_case

TEST_DATE = "2020-12-10T23:59:59"
NUMBER_OF_REMOTE_ADS = 36
# marks all tests as historical
pytestmark = [pytest.mark.historical]


@pytest.mark.parametrize("test_case", [
    {'params': {REMOTE: True, 'q': 'utvecklare'}, 'historical': 1},
    {'params': {REMOTE: False, 'q': 'utvecklare'}, 'historical': 238},
    {'params': {REMOTE: None, 'q': 'utvecklare'}, 'historical': 239},
    {'params': {REMOTE: True, 'q': 'säljare'}, 'historical': 2},
    pytest.param(
        {'params': {REMOTE: False, 'q': 'säljare'}, 'historical': 1277}, marks=pytest.mark.smoke),
    {'params': {REMOTE: None, 'q': 'säljare'}, 'historical': 1279},
    {'params': {REMOTE: True, OCCUPATION: occupation.saljkonsulent}, 'historical': 0},
    {'params': {REMOTE: None, OCCUPATION: occupation.saljkonsulent}, 'historical': 3},
    {'params': {OCCUPATION: occupation.saljkonsulent}, 'historical': 3},
    {'params': {OCCUPATION: occupation.mjukvaruutvecklare}, 'historical': 26},
    {'params': {REMOTE: True, OCCUPATION: occupation.mjukvaruutvecklare}, 'historical': 0},

])
def test_remote_occupation(test_case):
    """
    AND condition between REMOTE and other params
    """
    run_test_case(test_case)


@pytest.mark.parametrize("test_case", [
    {'params': {REMOTE: True, OCCUPATION_GROUP: occupation_group.mjukvaru__och_systemutvecklare_m_fl_},
     'historical': 1},
    {'params': {REMOTE: None, OCCUPATION_GROUP: occupation_group.mjukvaru__och_systemutvecklare_m_fl_},
     'historical': 145},
    pytest.param({'params': {REMOTE: False, OCCUPATION_GROUP: occupation_group.mjukvaru__och_systemutvecklare_m_fl_},
                  'historical': 144}, marks=pytest.mark.smoke),
    {'params': {REMOTE: True, OCCUPATION_GROUP: occupation_group.telefonforsaljare_m_fl_},
     'historical': 8},
    {'params': {REMOTE: None, OCCUPATION_GROUP: occupation_group.telefonforsaljare_m_fl_},
     'historical': 116},
    {'params': {REMOTE: True, OCCUPATION_GROUP: occupation_group.foretagssaljare}, 'historical': 0},
])
def test_remote_occupation_group(test_case):
    """
    AND condition between REMOTE and other params
    """
    run_test_case(test_case)


@pytest.mark.parametrize("test_case", [
    {"params": {REMOTE: True, OCCUPATION_FIELD: occupation_field.data_it}, "historical": 4},
    {"params": {REMOTE: None, OCCUPATION_FIELD: occupation_field.data_it}, "historical": 303},
    {"params": {REMOTE: True, OCCUPATION_FIELD: occupation_field.forsaljning__inkop__marknadsforing},
     "historical": 15},
    pytest.param({"params": {REMOTE: None, OCCUPATION_FIELD: occupation_field.forsaljning__inkop__marknadsforing},
                  "historical": 920}, marks=pytest.mark.smoke),
    {"params": {REMOTE: False, OCCUPATION_FIELD: occupation_field.forsaljning__inkop__marknadsforing},
     "historical": 905},

])
def test_remote_occupation_field(test_case):
    """
    AND condition between REMOTE and other params
    """
    run_test_case(test_case)


@pytest.mark.parametrize("test_case", [
    {"params": {'q': 'Stockholm'}, "historical": 3348},
    {"params": {REMOTE: True, 'q': 'Stockholm'}, "historical": 9},
    {"params": {REMOTE: False, 'q': 'Stockholm'}, "historical": 3339},
    pytest.param({"params": {MUNICIPALITY: geo.stockholm}, "historical": 2718},
                 marks=pytest.mark.smoke),
    {"params": {REMOTE: True, MUNICIPALITY: geo.stockholm}, "historical": 10},
    {"params": {REMOTE: False, MUNICIPALITY: geo.stockholm}, "historical": 2708},
])
def test_remote_municipality(test_case):
    """
    AND condition between REMOTE and other params
    """
    run_test_case(test_case)


@pytest.mark.parametrize("test_case", [
    {"params": {'q': 'Stockholms Län'}, "historical": 1475},
    pytest.param({"params": {REMOTE: True, 'q': 'Stockholms Län'}, "historical": 9},
                 marks=pytest.mark.smoke),
    pytest.param({"params": {REMOTE: False, 'q': 'Stockholms Län'}, "historical": 1466},
                 marks=pytest.mark.smoke),
    {"params": {'region': geo.stockholms_lan}, "historical": 1396},
    {"params": {REMOTE: True, 'region': geo.stockholms_lan}, "historical": 9},
    {"params": {REMOTE: None, 'region': geo.vastra_gotalands_lan}, "historical": 719},
    {"params": {REMOTE: True, 'region': geo.vastra_gotalands_lan}, "historical": 5},
    pytest.param({"params": {REMOTE: False, 'region': geo.vastra_gotalands_lan}, "historical": 714},
                 marks=pytest.mark.smoke),
    {"params": {'region': geo.vastra_gotalands_lan, 'q': 'säljare'}, "historical": 46},
    {"params": {REMOTE: True, 'region': geo.vastra_gotalands_lan, 'q': 'säljare'}, "historical": 0},
])
def test_remote_region(test_case):
    """
    AND condition between REMOTE and other params
    """
    run_test_case(test_case)


@pytest.mark.parametrize("test_case", [
    {"params": {PUBLISHED_AFTER: TEST_DATE}, "historical": 4247},
    {"params": {REMOTE: True, PUBLISHED_AFTER: TEST_DATE}, "historical": 31},
    pytest.param({"params": {PUBLISHED_BEFORE: TEST_DATE}, "historical": 13503},
                 marks=pytest.mark.smoke),
    {"params": {REMOTE: True, PUBLISHED_BEFORE: TEST_DATE}, "historical": 3},

])
def test_remote_publish_date(test_case):
    """
    AND condition between REMOTE and other params
    """
    run_test_case(test_case)


@pytest.mark.parametrize("test_case", [
    pytest.param({"params": {ABROAD: True}, "historical": 23}, marks=pytest.mark.smoke),
    {"params": {ABROAD: False}, "historical": NUMBER_OF_HISTORICAL_ADS},
    pytest.param({"params": {REMOTE: True, ABROAD: False}, "historical": 34},
                 marks=pytest.mark.smoke),
    # abroad False does nothing
])
def test_abroad(test_case):
    """
    AND condition between REMOTE and other params
    """
    run_test_case(test_case)


@pytest.mark.parametrize("test_case", [
    pytest.param({"params": {UNSPECIFIED_SWEDEN_WORKPLACE: True}, "historical": 12852},
                 marks=pytest.mark.smoke),
    {"params": {UNSPECIFIED_SWEDEN_WORKPLACE: False},
     "historical": NUMBER_OF_HISTORICAL_ADS},
    {"params": {REMOTE: True, UNSPECIFIED_SWEDEN_WORKPLACE: True}, "historical": 5},
    {"params": {REMOTE: True, UNSPECIFIED_SWEDEN_WORKPLACE: False}, "historical": 34}

])
def test_unspecified_workplace(test_case):
    """
    AND condition between REMOTE and other params
    """
    run_test_case(test_case)


@pytest.mark.parametrize("test_case", [
    {"params": {EXPERIENCE_REQUIRED: True}, "historical": 3725},
    {"params": {EXPERIENCE_REQUIRED: False}, "historical": 1243},
    pytest.param({"params": {REMOTE: True, EXPERIENCE_REQUIRED: True}, "historical": 17},
                 marks=pytest.mark.smoke),
    {"params": {REMOTE: False, EXPERIENCE_REQUIRED: True}, "historical": 3708},
    {"params": {REMOTE: True, EXPERIENCE_REQUIRED: False}, "historical": 15},
    {"params": {REMOTE: False, EXPERIENCE_REQUIRED: False}, "historical": 1228}
])

def test_experience(test_case):
    """
    AND condition between REMOTE and other params
    """
    run_test_case(test_case)


@pytest.mark.parametrize("test_case", [
    {"params": {REMOTE: True, 'q': '-Stockholm'}, "historical": 25},
    {"params": {REMOTE: True, MUNICIPALITY: f"-{geo.stockholm}"}, "historical": 24},
    {"params": {REMOTE: True, 'region': f"-{geo.stockholms_lan}"}, "historical": 25},
    {"params": {REMOTE: True, 'region': f"-{geo.vastra_gotalands_lan}"}, "historical": 29},
    pytest.param({"params": {REMOTE: True, 'region': f"-{geo.skane_lan}"}, "historical": 32},
                 marks=pytest.mark.smoke),
    {"params": {REMOTE: True, 'region': f"-{geo.norrbottens_lan}"}, "historical": 34}
])
def test_remote_negative_geography(test_case):
    """
    Negative geographical parameters
    AND condition between REMOTE and other params
    """
    run_test_case(test_case)


def test_combination_municipality():
    """
    numbers for REMOTE True + REMOTE False should add upp to numbers when not using REMOTE
    AND condition between REMOTE and other params
    """
    number_region = get_total({'municipality': geo.stockholm})
    number_remote = get_total({REMOTE: True, 'municipality': geo.stockholm})
    number_not_remote = get_total({REMOTE: False, 'municipality': geo.stockholm})
    assert number_remote + number_not_remote == number_region


def test_combination_region():
    """
    numbers for REMOTE True + REMOTE False should add upp to numbers when not using REMOTE
    AND condition between REMOTE and other params
    """
    number_region = get_total({'region': geo.stockholms_lan})
    number_remote = get_total({REMOTE: True, 'region': geo.stockholms_lan})
    number_not_remote = get_total({REMOTE: False, 'region': geo.stockholms_lan})
    assert number_remote + number_not_remote == number_region


# noinspection PyDictDuplicateKeys
# (duplicate dictionary keys are intentional for testing)

@pytest.mark.parametrize("test_case", [
    {"params": {REMOTE: False, REMOTE: True}, "historical": 34},  # noinspection
    pytest.param({"params": {REMOTE: True, REMOTE: False},
                  "historical": 17716}, marks=pytest.mark.smoke),
    {"params": {REMOTE: True, REMOTE: False, REMOTE: True}, "historical": 34}

])
def test_duplicate_remote_param(test_case):
    """
    with duplicated params, value of last param is used
     # noqa F602  disables check for repeated dictionary keys
    """
    run_test_case(test_case)
