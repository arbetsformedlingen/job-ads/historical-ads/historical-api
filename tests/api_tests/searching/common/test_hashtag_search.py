import pytest

from tests.test_resources.api_test_runner import run_test_case


# marks all tests as historical
pytestmark = [pytest.mark.historical]


@pytest.mark.smoke
@pytest.mark.parametrize("test_case", [
    {'params': {'q': '#corona'}, 'historical': 0},
    {'params': {'q': 'corona'}, 'historical': 0},
    pytest.param({'params': {'q': "#jobbjustnu'"}, 'historical': 155}, marks=pytest.mark.smoke),
    {'params': {'q': "jobbjustnu'"}, 'historical': 9},
    {'params': {'q': "#metoo'"}, 'historical': 2},
    {'params': {'q': "metoo'"}, 'historical': 0},
    {'params': {'q': "#wedo'"}, 'historical': 3},
    {'params': {'q': "wedo'"}, 'historical': 1}
])
def test_hashtag_search(test_case):
    run_test_case(test_case)
