import pytest
from tests.test_resources.helper import get_search
from tests.test_resources.taxonomy_replace_helper import ad_ids_from_response
from tests.test_resources.concept_ids.municipality import get_muncipalities_with_hits, \
    get_ten_municipalities_with_hits, get_municipality_by_id


@pytest.mark.slow
@pytest.mark.parametrize("municipality", get_muncipalities_with_hits())
def test_search_by_municipality_code_all(municipality):
    _check_response_by_municipality_code(municipality)


@pytest.mark.smoke
@pytest.mark.parametrize("municipality", get_ten_municipalities_with_hits())
def test_search_by_municipality_code_smoke(municipality):
    _check_response_by_municipality_code(municipality)

"""
Skipping this test because many older ads lack a value for 'municipality_concept_id'. 
As a result, this test may fail with the current test data.
"""
@pytest.mark.skip
@pytest.mark.slow
@pytest.mark.parametrize("municipality", get_muncipalities_with_hits())
def test_search_by_municipality_concept_id_all(municipality):
    if municipality.get('id'):
        _check_response_by_municipality_concept_id(municipality)


@pytest.mark.skip
@pytest.mark.smoke
@pytest.mark.parametrize("municipality", get_ten_municipalities_with_hits())
def test_search_by_municipality_concept_id_smoke(municipality):
    _check_response_by_municipality_concept_id(municipality)


@pytest.mark.smoke
@pytest.mark.parametrize("municipality", get_ten_municipalities_with_hits())
def test_search_by_negative_municipality_concept_id_smoke(municipality):
    negative_municipality_id = f"-{municipality['id']}"
    municipality['id'] = negative_municipality_id
    MUNICIPALITY_ID_STOCKHOLM = "AvNB_uwa_6n6"
    positive_municipality =  get_municipality_by_id(MUNICIPALITY_ID_STOCKHOLM)

    _check_response_by_negative_municipality_concept_id(municipality, positive_municipality)


def _check_response_by_municipality_code(municipality):
    muncipality_code = municipality['code']
    response = get_search({'municipality': muncipality_code})

    hits = response['hits']
    assert (hits)
    ad_ids = ad_ids_from_response(response)
    assert ad_ids.sort() == municipality['ad_ids'].sort()
    for hit in hits:
        expected_municip = municipality['code']
        actual_municip = hit['workplace_address']['municipality_code']
        assert actual_municip == expected_municip, f"Expected {expected_municip} but got {actual_municip}"
        actual_region_code = hit['workplace_address']['region_code']
        if actual_region_code:
            # Some of the testdata have None as value for region_code, skip those checks since we test municipality primarily.
            expected_region_code = municipality['region_code']
            assert actual_region_code == expected_region_code, f"Expected {expected_region_code} but got {actual_region_code}"


def _check_response_by_municipality_concept_id(municipality):
    concept_id = municipality['id']
    response = get_search({'municipality': concept_id})
    hits = response['hits']
    assert (hits)
    ad_ids = ad_ids_from_response(response)
    assert ad_ids.sort() == municipality['ad_ids'].sort()
    for hit in hits:
        expected_municip = municipality['code']
        actual_municip = hit['workplace_address']['municipality_code']
        assert actual_municip == expected_municip, f"Expected {expected_municip} but got {actual_municip}"
        actual_region_code = hit['workplace_address']['region_code']
        if actual_region_code:
            # Some of the testdata have None as value for region_code, skip those checks since we test municipality primarily.
            expected_region_code = municipality['region_code']
            assert actual_region_code == expected_region_code, f"Expected {expected_region_code} but got {actual_region_code}"

def _check_response_by_negative_municipality_concept_id(neg_municipality, pos_municipality):

    neg_municipality_id = neg_municipality['id']
    pos_municipality_id = pos_municipality['id']

    response = get_search({'municipality': [neg_municipality_id, pos_municipality_id]})

    hits = response['hits']
    assert (hits)

    for hit in hits:
        expected_code = pos_municipality['code']
        actual_code = hit['workplace_address']['municipality_code']
        not_expected_code = neg_municipality['code']

        assert actual_code == expected_code, f"Expected {expected_code} but got {actual_code}"

        assert actual_code != not_expected_code, f"Did not expect {not_expected_code} but got {actual_code}"

