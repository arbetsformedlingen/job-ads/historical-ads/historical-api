import pytest
import copy

from tests.test_resources.helper import get_search, get_search_with_headers
from tests.test_resources.api_test_runner import HISTORICAL
from tests.test_resources.test_settings import TEST_USE_STATIC_DATA
from common.constants import X_FEATURE_FREETEXT_BOOL_METHOD
from tests.test_resources.test_settings import test_headers


# marks all tests as historical
pytestmark = [pytest.mark.historical]


@pytest.mark.parametrize("test_case", [
    {"query": "", "historical": 17750, "bool_method": "and"},
    {"query": "Sirius crew", "historical": 0, "bool_method": "and"},
    {"query": "Sirius crew", "historical": 11, "bool_method": "or"},
    {"query": "Sirius crew", "historical": 11, "bool_method": None},
    pytest.param({"query": "lagstiftning anställning ", "historical": 41, "bool_method": "and"}, marks=pytest.mark.smoke),
    {"query": "lagstiftning anställning ", "historical": 2566, "bool_method": "or"},
    {"query": "lagstiftning anställning ", "historical": 2566, "bool_method": None},
    {"query": "TechBuddy uppdrag", "historical": 1, "bool_method": "and"},
    {"query": "TechBuddy uppdrag", "historical": 3810, "bool_method": "or"},
    {"query": "TechBuddy uppdrag", "historical": 3810, "bool_method": None}
])
def test_freetext_bool_method(test_case):
    """
    Test with 'or' & 'and' values for X_FEATURE_FREETEXT_BOOL_METHOD header flag
    Default value is 'OR' (used in test cases with None as param)
    Searches with 'or' returns more hits
    """
    params = {'q': test_case["query"], 'limit': 0}
    # use default setting for X_FEATURE_FREETEXT_BOOL_METHOD == 'OR'
    bool_method = test_case["bool_method"]
    if not bool_method:
        response_json = get_search(params)
    else:
        tmp_headers = copy.deepcopy(test_headers)
        tmp_headers[X_FEATURE_FREETEXT_BOOL_METHOD] = bool_method
        response_json = get_search_with_headers(params, tmp_headers)

    expected = test_case["historical"]

    if TEST_USE_STATIC_DATA:
        assert response_json['total']['value'] == expected
