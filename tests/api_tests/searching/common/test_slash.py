import pytest

from tests.test_resources.api_test_runner import run_test_case

# marks all tests as historical
pytestmark = [pytest.mark.historical]


@pytest.mark.parametrize("test_case", [
    {"params": {"q": "försäljning/marknad"}, "historical": 35},
    {"params": {"q": "försäljning marknad"}, "historical": 62},
    pytest.param({"params": {"q": "försäljning / marknad"}, "historical": 108}, marks=pytest.mark.smoke),
    {"params": {"q": "lager/logistik"}, "historical": 50},
    {"params": {"q": "lager / logistik"}, "historical": 10},
    {"params": {"q": "lager logistik"}, "historical": 472},
    pytest.param({"params": {"q": "psykolog/beteendevetare"}, "historical": 53}, marks=pytest.mark.smoke),
    {"params": {"q": "psykolog / beteendevetare"}, "historical": 1},
    {"params": {"q": "psykolog beteendevetare"}, "historical": 71},
    {"params": {"q": "Affärsutvecklare/exploateringsingenjör"}, "historical": 8},
    {"params": {"q": "Affärsutvecklare / exploateringsingenjör"}, "historical": 0},
    {"params": {"q": "Affärsutvecklare exploateringsingenjör"}, "historical": 16},
    {"params": {"q": "Affärsutvecklare/exploateringsingenjörer"}, "historical": 5},
    {"params": {"q": "Affärsutvecklare / exploateringsingenjörer"}, "historical": 0},
    {"params": {"q": "Affärsutvecklare exploateringsingenjörer"}, "historical": 16},
    {"params": {"q": "barnpsykiatri/habilitering"}, "historical": 6},
    {"params": {"q": "barnpsykiatri / habilitering"}, "historical": 1},
    pytest.param({"params": {"q": "barnpsykiatri habilitering"}, "historical": 39}, marks=pytest.mark.smoke),
    {"params": {"q": "mentor/kontaktlärare"}, "historical": 1},
    {"params": {"q": "mentor / kontaktlärare"}, "historical": 7},
    {"params": {"q": "mentor kontaktlärare"}, "historical": 0},
    {"params": {"q": "Verktygsmakare/Montör"}, "historical": 86},
    pytest.param({"params": {"q": "Verktygsmakare / Montör"}, "historical": 4}, marks=pytest.mark.smoke),
    {"params": {"q": "Verktygsmakare Montör"}, "historical": 156},
    {"params": {"q": "Kolloledare/specialpedagog"}, "historical": 78},
    {"params": {"q": "Kolloledare / specialpedagog"}, "historical": 1},
    {"params": {"q": "Kolloledare specialpedagog"}, "historical": 90},
    {"params": {"q": "fritidshem/fritidspedagog"}, "historical": 63},
    {"params": {"q": "fritidshem / fritidspedagog"}, "historical": 0},
    pytest.param({"params": {"q": "fritidshem fritidspedagog"}, "historical": 59}, marks=pytest.mark.smoke),
    {"params": {"q": "UX/UI Designer"}, "historical": 2},
    {"params": {"q": "UX / UI Designer"}, "historical": 1},
    {"params": {"q": "UX UI Designer"}, "historical": 2}

])
def test_freetext_search_slash(test_case):
    """

    Search with terms that are joined by a slash '/' included (x/y)
    with the terms separately (x y)
    and with a slash surrounded by space (x / y)
    """
    run_test_case(test_case)


@pytest.mark.parametrize("test_case", [
    {"params": {"q": ".NET/C#"}, "historical": 38},
    {"params": {"q": ".NET / C#"}, "historical": 15},
    {"params": {"q": ".NET C#"}, "historical": 122},
    {"params": {"q": ".NET /C#"}, "historical": 11},
    {"params": {"q": ".NET/ C#"}, "historical": 30},
    pytest.param({"params": {"q": ".NET"}, "historical": 82},
                 marks=pytest.mark.smoke),
    {"params": {"q": "C#/.net"}, "historical": 40},
    {"params": {"q": "C# .net"}, "historical": 122},
    {"params": {"q": "C# /.net"}, "historical": 30},
    {"params": {"q": "C# / .net"}, "historical": 15},
    {"params": {"q": "C#"}, "historical": 94},
    {"params": {"q": "dotnet"}, "historical": 79}

])
def test_freetext_search_dot_hash_slash(test_case):
    """
    Search with terms that are joined by a slash '/' included (x/y)
    with the terms separately (x y)
    and with a slash surrounded by space (x / y)
    for words that have . or # (e.g. '.net', 'c#')
    """
    run_test_case(test_case)


@pytest.mark.parametrize("test_case", [
    {'params': {'q': 'programmerare'}, 'historical': 234},
    {'params': {'q': 'Systemutvecklare'}, 'historical': 234},
    {'params': {'q': 'Systemutvecklare/Programmerare'}, 'historical': 61},
    {'params': {'q': 'Systemutvecklare Programmerare'}, 'historical': 234},
    pytest.param(
        {'params': {'q': 'Systemutvecklare / Programmerare'}, 'historical': 18},
        marks=pytest.mark.smoke),
])
def test_freetext_search_slash_short(test_case):
    run_test_case(test_case)
