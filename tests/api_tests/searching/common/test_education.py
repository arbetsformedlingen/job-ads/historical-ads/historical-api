import pytest

from tests.test_resources.helper import get_by_id

pytestmark = [pytest.mark.historical]


def test_known_must_education_fields():
    """
    Ad should have data in the education* fields
    """
    ad_id = 'eda0d4fb469b37a248c74f29fa1711e668ab5be4'
    ad = get_by_id(ad_id)
    assert ad['must_have']['education']
    assert ad['must_have']['education_level']


def test_known_nice_education_fields():
    """
    Ad should have data in the education* fields
    """
    ad_id = '4f8d087990239fa46e092e3ba5689e8e8471f80c'
    ad = get_by_id(ad_id)
    assert ad['nice_to_have']['education']
    assert ad['nice_to_have']['education_level']
