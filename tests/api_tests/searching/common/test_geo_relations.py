import pytest

from tests.test_resources.helper import get_search
from tests.test_resources.api_test_runner import HISTORICAL
from tests.test_resources.concept_ids import concept_ids_geo as geo

# marks all tests as historical
pytestmark = [pytest.mark.historical]


@pytest.mark.parametrize("city, region, country", [
    (geo.stockholm, geo.stockholms_lan, geo.sverige),
    (geo.malmo, geo.skane_lan, geo.sverige)
])
def test_search_municipality(city, region, country):
    """
    Check that parent concept ids (region, country) are correct when searching for a municipality
    """
    json_response = get_search({'municipality': city})
    hits = json_response['hits']
    for hit in hits:
        assert hit['workplace_address']['municipality_concept_id'] == city
        assert hit['workplace_address']['region_concept_id'] == region
        assert hit['workplace_address']['country_concept_id'] == country


@pytest.mark.parametrize("cities, region, country", [
    ([geo.kiruna, geo.lulea, geo.gallivare, geo.kalix, geo.alvsbyn, geo.jokkmokk, geo.overtornea, geo.pitea,
      'vkQW_GB6_MNk', 'tfRE_hXa_eq7', 'y4NQ_tnB_eVd'], geo.norrbottens_lan, geo.sverige),
    ([geo.malmo, geo.lund, geo.bastad, geo.sjobo, geo.kavlinge, geo.helsingborg, geo.angelholm, geo.hoor,
      geo.hassleholm, geo.burlov, 'naG4_AUS_z2v', 'STvk_dra_M1X', 'Tcog_5sH_b46', 'Yt5s_Vf9_rds', 'najS_Lvy_mDD',
      'hdYk_hnP_uju'], geo.skane_lan, geo.sverige)
])
def test_search_region(cities, region, country):
    """
    Check that parent (country) and child (municipality) concept ids are correct when searching for ads in a region
    """
    json_response = get_search({'region': region})
    hits = json_response['hits']
    for hit in hits:
        assert hit['workplace_address']['municipality_concept_id'] in cities
        assert hit['workplace_address']['region_concept_id'] == region
        assert hit['workplace_address']['country_concept_id'] == country


@pytest.mark.parametrize("country", [geo.norge, geo.aland_tillhor_finland, geo.malta, geo.schweiz])
def test_search_country_except_sweden(country):
    """
    Test that countries except Sweden do not have concept ids for municipality and region.
    """
    json_response = get_search({'country': country})
    hits = json_response['hits']
    for hit in hits:
        assert hit['workplace_address']['municipality_concept_id'] is None
        assert hit['workplace_address']['region_concept_id'] is None
        assert hit['workplace_address']['country_concept_id'] == country


def test_search_country_sweden():
    """
    Test that concept id for country is correct when searching for ads in Sweden
    """
    country = geo.sverige
    json_response = get_search({'country': country})
    hits = json_response['hits']
    countries = [country]
    if HISTORICAL:
        countries.append("199")
    for hit in hits:
        assert hit['workplace_address']['country_concept_id'] in countries
