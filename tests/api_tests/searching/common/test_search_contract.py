import pytest
import tests.test_resources.ad_fields_data_type as type_check
from tests.test_resources.api_test_runner import HISTORICAL

pytestmark = [pytest.mark.historical]


def test_types_top_level(random_ads):
    for ad in random_ads:
        type_check.check_types_top_level(ad)


def test_types_occupation(random_ads):
    for ad in random_ads:
        type_check.check_types_occupation(ad)


def test_duration(random_ads):
    for ad in random_ads:
        type_check.check_duration(ad)


def test_employment_type(random_ads):
    for ad in random_ads:
        type_check.check_employment_type(ad)


def test_salary_type(random_ads):
    for ad in random_ads:
        type_check.check_salary_type(ad)


def test_types_employer(random_ads):
    for ad in random_ads:
        type_check.check_types_employer(ad)


def test_scope_of_work(random_ads):
    for ad in random_ads:
        type_check.check_scope_of_work(ad)


def test_application_details(random_ads):
    for ad in random_ads:
        type_check.check_application_details(ad)


def test_job_ad_description(random_ads):
    for ad in random_ads:
        type_check.check_job_ad_description(ad)


def test_workplace_address(random_ads):
    for ad in random_ads:
        type_check.check_workplace_address(ad)

def test_requirements(random_ads):
    # todo: create fixture to skip test for historical
    if not HISTORICAL:
        for ad in random_ads:
            type_check.check_requirements(ad)


def test_working_hours_type(random_ads):
    for ad in random_ads:
        type_check.check_working_hours_type(ad)


def test_contact_persons(random_ads):
    for ad in random_ads:
        type_check.check_types_contact(ad)


def test_length(random_ads):
    for ad in random_ads:
        # issue 117, "original_id"
        type_check.check_length(ad, 37)
