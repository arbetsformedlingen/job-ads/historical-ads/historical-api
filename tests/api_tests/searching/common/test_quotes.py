import pytest
from tests.test_resources.api_test_runner import run_test_case
from tests.test_resources.test_settings import EXPECTED_GYMNASIE_LARARE

# marks all tests as historical
pytestmark = [pytest.mark.historical]


@pytest.mark.smoke
@pytest.mark.parametrize("test_case", [
    {'params': {'q': '"gymnasielärare"'}, 'historical': 101, 'comment': 'a'},
    {'params': {'q': "'gymnasielärare'"}, 'historical': 132, 'comment': 'b'},
    {'params': {'q': '"gymnasielärare"'}, 'historical': 101, 'comment': 'c'},
    {'params': {'q': "'gymnasielärare'"}, 'historical': 132, 'comment': 'd'},
    {'params': {'q': 'gymnasielärare'}, 'historical': 132, 'comment': 'e'},
    pytest.param({'params': {'q': 'gymnasielärare'}, 'historical': 132, 'comment': 'f'},
                 marks=pytest.mark.smoke),
    {'params': {'q': 'gymnasielärare'}, 'historical': 132, 'comment': 'g'},
    {'params': {'q': 'gymnasielärare'}, 'historical': 132, 'comment': 'h'},
    {'params': {'q': 'gymnasielärare"'}, 'historical': 132, 'comment': 'i'},
    {'params': {'q': "gymnasielärare'"}, 'historical': 132, 'comment': 'j'},
    {'params': {'q': 'gymnasielärare'}, 'historical': 132, 'comment': 'k'},
    {'params': {'q': 'gymnasielärare'}, 'historical': 132, 'comment': 'l'},
    {'params': {'q': 'gymnasielärare lärare'}, 'historical': 892, 'comment': 'm'},
    {'params': {'q': "'gymnasielärare'"}, 'historical': 132, 'comment': 'n'},
    {'params': {'q': '"gymnasielärare" "lärare"'}, 'historical': 1265, 'comment': 'o'},
    {'params': {'q': '"gymnasielärare lärare"'}, 'historical': 0, 'comment': 'p'},
    {'params': {'q': '"gymnasielärare"'}, 'historical': 101, 'comment': 'q'},
    {'params': {'q': '"gymnasielärare'}, 'historical': 101, 'comment': 'r'},
    {'params': {'q': '"gymnasielärare"'}, 'historical': 101, 'comment': 's'},
    {'params': {'q': 'gymnasielärare'}, 'historical': 132, 'comment': 't'},
    {'params': {'q': 'gymnasielärare'}, 'historical': 132, 'comment': 'u'},

])
def test_different_quote_styles(test_case):
    # Different quotes gives different results.
    # Comment is to make the test case identifiable if it fails (quotes are not obvious in the error printout)
    run_test_case(test_case)


@pytest.mark.parametrize('test_case', [
    pytest.param({'params': {'q': '"c++'}, 'historical': 138},marks=pytest.mark.smoke),
    {'params': {'q': '"c++"'}, 'historical': 138},
    pytest.param({'params': {'q': '"c+'}, 'historical': 95}, marks=pytest.mark.smoke),
    {'params': {'q': '"c( '}, 'historical': 93},
])
def test_cplusplus_in_quotes(test_case):
    """
    Test for a bug where some quotes caused an 'internal server error' response
    """
    run_test_case(test_case)


@pytest.mark.parametrize("test_case", [
    {'params': {'q': 'python stockholm'}, 'historical': 17},
    {'params': {'q': '"python stockholm"'}, 'historical': 0},
    {'params': {'q': '"python" "stockholm"'}, 'historical': 3069},
    pytest.param({'params': {'q': '"python" stockholm'}, 'historical': 3344},marks=pytest.mark.smoke),
    {'params': {'q': 'python "stockholm"'}, 'historical': 73},
    {'params': {'q': '"python job in stockholm"'}, 'historical': 0},
    {'params': {'q': '"work from home" python stockholm'}, 'historical': 17},
])
def test_query_with_quotes(test_case):
    run_test_case(test_case)
