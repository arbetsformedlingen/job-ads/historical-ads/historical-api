import pytest
import requests
from tests.test_resources.helper import get_search_expect_error

pytestmark = [pytest.mark.historical]

date_str_with_errors = "2022-05-22T12:20:52 fm"
date_str_without_errors = "2021-05-22T12:20:52"


@pytest.mark.parametrize(
    "param",
    [
        "published-after",
        "published-before",
    ],
)
def test_date_str_without_errors(param):
    """
    Test that a Bad Request exception is not raised when the system
    can parse the date.
    """
    params = {param: date_str_without_errors}
    get_search_expect_error(params, expected_http_code=requests.codes.ok)


@pytest.mark.parametrize(
    "param",
    [
        "published-after",
        "published-before",
    ],
)
def test_date_str_with_errors(param):
    """
    Test that a Bad Request exception is raised when the system can not
    parse the date because of invalid formatting.
    """
    params = {param: date_str_with_errors}
    get_search_expect_error(params, expected_http_code=requests.codes.bad_request)
