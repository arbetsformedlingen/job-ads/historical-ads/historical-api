import pytest

from tests.test_resources.api_test_runner import run_test_case

# marks all tests as historical
pytestmark = [pytest.mark.historical]


# TODO: adapt for historical ads test data


@pytest.mark.smoke
@pytest.mark.parametrize("test_case", [
    {"params": {"q": "murar*"}, "historical": 14},
    {"params": {"q": "*utvecklare"}, "historical": 423},
    {"params": {"q": "utvecklare*"}, "historical": 164},
    pytest.param({"params": {"q": "*utvecklare*"}, "historical": 0,
                  "comment": "multiple wildcards doe not work"},
                  marks=pytest.mark.smoke),
    {"params": {"q": "Anläggningsarbetar*"}, "historical": 18},
    {"params": {"q": "Arbetsmiljöingenjö*"}, "historical": 10},
    {"params": {"q": "Behandlingsassisten*"}, "historical": 54},
    {"params": {"q": "Bilrekonditionerar*"}, "historical": 9},
    {"params": {"q": "Eventkoordinato*"}, "historical": 2},
    {"params": {"q": "Fastighetsförvaltar*"}, "historical": 26},
    pytest.param({"params": {"q": "Fastighetsskötar*"}, "historical": 68}, 
                 marks=pytest.mark.smoke),
    {"params": {"q": "Fastighet*"}, "historical": 692},
    {"params": {"q": "Kundtjänstmedarbetar*"}, "historical": 139},
    {"params": {"q": "Kundtjänst*"}, "historical": 429},
    {"params": {"q": "sjukskö*"}, "historical": 1283},
    {"params": {"q": "sköterska*"}, "historical": 7},
    {"params": {"q": "skötersk*"}, "historical": 30},
    {"params": {"q": "sj"}, "historical": 7, "comment": "min 3 characters"},
    {"params": {"q": "sj*"}, "historical": 0, "comment": "min 3 characters"},
    {"params": {"q": "sju*"}, "historical": 2746}
])
def test_wildcard_search_exact_match(test_case):
    """
    Test different wildcard queries
    check that the number of results is exactly as expected
    """
    run_test_case(test_case)
