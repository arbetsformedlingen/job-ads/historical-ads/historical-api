import pytest
import requests

from tests.test_resources.helper import get_search
from tests.test_resources.test_settings import NUMBER_OF_HISTORICAL_ADS, TEST_USE_STATIC_DATA
from common.constants import PUBLISHED_AFTER, PUBLISHED_BEFORE

@pytest.mark.historical
@pytest.mark.skipif(not TEST_USE_STATIC_DATA, reason="depends on mock data")
def test_empty_published_dates():
    expected = NUMBER_OF_HISTORICAL_ADS
    params = _historical_param_formatter("", "")
    result = get_search(params)
    assert result['total']['value'] == expected

@pytest.mark.historical
@pytest.mark.skipif(not TEST_USE_STATIC_DATA, reason="depends on mock data")
@pytest.mark.parametrize("from_date, to_date, expected", [
    ('2022', '2022', 2127),
    ('2022', '2023', 2723),
    ('2023', '2023', 596),
])
def test_YYYY( from_date, to_date, expected):
    params = _historical_param_formatter(from_date, to_date)
    result = get_search(params)
    assert result['total']['value'] == expected

@pytest.mark.historical
@pytest.mark.skipif(not TEST_USE_STATIC_DATA, reason="depends on mock data")
@pytest.mark.parametrize("from_date, to_date, expected", [
    ('2016-02', '2017-03', 1726),
    ('2016-02', '2021-03', 6729),
    ('2018-02', '2022-03', 5555),
    ('2020-12', '2021-01', 161),
])
def test_YYYY_MM(from_date, to_date, expected):
    params = _historical_param_formatter(from_date, to_date)
    result = get_search(params)
    assert result['total']['value'] == expected

@pytest.mark.historical
@pytest.mark.skipif(not TEST_USE_STATIC_DATA, reason="depends on mock data")
@pytest.mark.parametrize("from_date, to_date, expected", [
    ('2020-01-01', '2020-12-31', 992),
    ('2022-02-10', '2022-02-20', 74),
])
def test_YYYY_MM_DD(from_date, to_date, expected):
    params = _historical_param_formatter(from_date, to_date)
    result = get_search(params)
    assert result['total']['value'] == expected

@pytest.mark.historical
@pytest.mark.skipif(not TEST_USE_STATIC_DATA, reason="depends on mock data")
@pytest.mark.parametrize("from_date, to_date, expected", [
    ('2022-12-23T00:00:00', '2022-12-23T14:00:00', 9),
    ('2022-12-23T00:00:00', '2022-12-23T14:10:00', 10),
    ('2020-03-01T00:00:00', '2020-03-31T00:00:00', 101),
    ('2016-02-01T00:00:00', '2016-02-25T00:00:00', 108),
])
def test_YYYY_MM_DD_HH_MM_SS(from_date, to_date, expected):
    params = _historical_param_formatter(from_date, to_date)
    result = get_search(params)
    assert result['total']['value'] == expected


@pytest.mark.historical
@pytest.mark.skipif(not TEST_USE_STATIC_DATA, reason="depends on mock data")
@pytest.mark.parametrize("from_date, to_date, expected", [
    ('', '2020', 13552),
    ('', '2022', 17154),
    ('', '2021-02', 13730),
])
def test_empty_after_date( from_date, to_date, expected):
    params = _historical_param_formatter(from_date, to_date)
    result = get_search(params)
    assert result['total']['value'] == expected


@pytest.mark.historical
@pytest.mark.skipif(not TEST_USE_STATIC_DATA, reason="depends on mock data")
@pytest.mark.parametrize("from_date, to_date, expected", [
    ('2016-03', '', 10500),
    ('2017-03', '', 9044),
    ('2021-03', '', 4020),
    ('2022-03', '', 2386),
])
def test_empty_before_date( from_date, to_date, expected):
    params = _historical_param_formatter(from_date, to_date)
    result = get_search(params)
    assert result['total']['value'] == expected


@pytest.mark.historical
@pytest.mark.skipif(not TEST_USE_STATIC_DATA, reason="depends on mock data")
@pytest.mark.parametrize("from_date, to_date, expected", [
    ('2022', '2021', 0),
    ('2022-02', '2022-01', 0),
    ('2022-02-10', '2022-02-08', 0),
])
def test_reverse_order( from_date, to_date, expected):
    """
    This test depends on test ads from all years
    """
    params = _historical_param_formatter(from_date, to_date)
    result = get_search(params)
    assert result['total']['value'] == expected

@pytest.mark.historical
@pytest.mark.skipif(not TEST_USE_STATIC_DATA, reason="depends on mock data")
@pytest.mark.parametrize("from_date, to_date", [
    ('2022-02-10', '2022-02-30'),
    ('2022-02-10', '2022-AB-CD'),
    ('2022-AB-CD', '2022-02-10'),
    ('2022-02-20T00', '2022-02-20T10'),
])
def test_invalid_date(from_date, to_date):
    params = _historical_param_formatter(from_date, to_date)
    with pytest.raises(requests.exceptions.HTTPError):
        get_search(params)

def _historical_param_formatter(from_date: str, to_date: str) -> dict:
    if from_date and not to_date:
        params = {PUBLISHED_AFTER: from_date}
    elif to_date and not from_date:
        params = {PUBLISHED_BEFORE: to_date}
    elif from_date and to_date:
        params = {PUBLISHED_AFTER: from_date, PUBLISHED_BEFORE: to_date}
    else:
        params = {}
    return params
