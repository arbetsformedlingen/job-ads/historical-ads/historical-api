import pytest
from tests.test_resources.helper import get_search
from tests.test_resources.test_settings import NUMBER_OF_HISTORICAL_ADS, TEST_USE_STATIC_DATA
from common.constants import HISTORICAL_FROM, HISTORICAL_TO


def _historical_param_formatter(from_date: str, to_date: str) -> dict:
    if from_date and not to_date:
        params = {HISTORICAL_FROM: from_date}
    elif to_date and not from_date:
        params = {HISTORICAL_TO: to_date}
    elif from_date and to_date:
        params = {HISTORICAL_FROM: from_date, HISTORICAL_TO: to_date}
    else:
        params = {}
    return params


@pytest.mark.historical
@pytest.mark.skipif(not TEST_USE_STATIC_DATA, reason="depends on mock data")
@pytest.mark.parametrize("from_date, to_date, expected", [
    (None, None, NUMBER_OF_HISTORICAL_ADS),
    ('2017-03', None, 9044),
    ('2021-03', None, 4020),
    ('2016-02', '2021-03', 6729),
    ('2018-02', '2022-03', 5555),
    (None, '2021-02', 13730),
    ('2020-12', '2021-01', 161),
    (None, '2020', 13552),
])
def test_historical_date( from_date, to_date, expected):
    params = _historical_param_formatter(from_date, to_date)
    result = get_search(params)
    assert result['total']['value'] == expected


@pytest.mark.historical
@pytest.mark.skipif(not TEST_USE_STATIC_DATA, reason="depends on mock data")
@pytest.mark.parametrize("from_date, to_date, expected", [
    (None, None, NUMBER_OF_HISTORICAL_ADS),
    ('2016-03', None, 10500),
    ('2016-02', '2017-03', 1726),
    ('2020-12', '2021-01', 161),
    (None, '2022', 17154),
    ('2022', '2022', 2127),
    ('2022', '2021', 0),  # reverse order
])
def test_historical_date_full( from_date, to_date, expected):
    """
    This test depends on test ads from all years
    """
    params = _historical_param_formatter(from_date, to_date)
    result = get_search(params)
    assert result['total']['value'] == expected
