import logging
import json
import time
from flask_restx import abort
from opensearchpy import exceptions
from werkzeug.exceptions import Conflict
from common.multiple_historical_results_error import MultipleHistoricalResultsError

from common import constants
from common import settings, fields, fields as f
from common.main_opensearch_client import opensearch_client
from common.opensearch_connection_with_retries import opensearch_search_with_retry

log = logging.getLogger(__name__)


def search_for_ads(args, querybuilder, start_time=0, x_fields=None):
    if start_time == 0:
        start_time = int(time.time() * 1000)
    query_dsl = querybuilder.parse_args(args, x_fields)
    log.debug(f"(search for ads) Query constructed after: {int(time.time() * 1000 - start_time)} milliseconds")

    # First pass, find highest score:
    if args.get(constants.MIN_RELEVANCE):  # TODO: not executed by tests
        max_score_query = query_dsl.copy()
        max_score_query["from"] = 0
        max_score_query["size"] = 1
        max_score_query["track_total_hits"] = False
        del max_score_query["aggs"]
        del max_score_query["sort"]
        max_score_result = opensearch_search_with_retry(
            client=opensearch_client(), query=max_score_query, index=settings.ADS_ALIAS
        )
        if max_score_result:
            max_score = max_score_result.get("hits", {}).get("max_score")
            if max_score:
                query_dsl["min_score"] = (max_score - 1) * args.get(constants.MIN_RELEVANCE)
    log.debug(f"ARGS: {args}")
    log.debug(f"QUERY: {json.dumps(query_dsl)}")

    if args.get(constants.HISTORICAL_REQUEST_TIMEOUT):
        request_timeout = args.get(constants.HISTORICAL_REQUEST_TIMEOUT)
    else:
        request_timeout = settings.DB_DEFAULT_TIMEOUT
    query_result = opensearch_search_with_retry(
        client=opensearch_client(), query=query_dsl, index=settings.ADS_ALIAS, db_request_timeout=request_timeout
        )
    log.debug(
        f"(search for ads) opensearch results after: {int(time.time() * 1000 - start_time)} ms, timeout: {request_timeout}"
        )
    if not query_result:
        abort(500, "Failed to establish connection to database")
        return

    if args.get(constants.FREETEXT_QUERY) and not args.get(constants.X_FEATURE_DISABLE_SMART_FREETEXT):
        # First remove any phrases
        (phrases, qs) = querybuilder.extract_quoted_phrases(args.get(constants.FREETEXT_QUERY))
        query_result["concepts"] = _extract_concept_from_concepts(querybuilder.text_to_concepts(qs))

    log.debug(
        f"(search for ads) opensearch reports took: {query_result.get('took', 0)}, timed_out: {query_result.get('timed_out', '')}"
        )
    return transform_ad_search_result(args, query_result, querybuilder)


def _extract_concept_from_concepts(concepts):
    main_concepts = dict()
    for key, value in concepts.items():
        main_concepts[key] = [v["concept"].lower() for v in value]
    return main_concepts


def _format_ad(result):
    return result.get("_source")


def _search_for_single_ad_in_historical(query_fields: list, ad_id: str) -> dict:
    query = {"query": {"multi_match": {"query": ad_id, "fields": query_fields}}}
    query_result = opensearch_search_with_retry(opensearch_client(), query, settings.ADS_ALIAS)
    hits = query_result.get("hits", {}).get("hits", [])
    if hits:
        if len(hits) > 1:
            # Multiple matches found.
            # Raise error with unique ids for all matches.
            ids_for_hits = []
            for hit in hits:
                ids_for_hits.append({"id": hit["_id"]})
            raise MultipleHistoricalResultsError(ids_for_hits)
        return _format_ad(hits[0])
    else:
        return None


def fetch_ad_by_id(ad_id, historical=False):
    try:
        """
        Historical ads has an alias with multiple indices,
        so a search for the ad using the id must be made,
        the simple get operation is not possible on multi-index aliases
        """
        if query_result := _search_for_single_ad_in_historical(
                query_fields=[fields.EXTERNAL_ID, fields.ID, fields.ORIGINAL_ID], ad_id=ad_id
        ):
            return query_result

        # if the function has not returned query_result, a '404 not found' will be returned
        log.info(f"Ad: {ad_id} not found, returning 404 message")
        abort(404, "Ad not found")
    except exceptions.NotFoundError:
        log.exception(f"Failed to find id: {ad_id}")
        abort(404, "Ad not found")
        return
    except exceptions.ConnectionError as e:
        log.exception(f"Failed to connect to opensearch: {e}")
        abort(500, "Failed to establish connection to database")
        return
    except MultipleHistoricalResultsError as mhre:
        log.exception(f"Multiple matches found for id {ad_id}")
        error_message = {"info": f"Multiple matches found for id {ad_id}", "requested_id": f"{ad_id}",
                         "matches": mhre.args[0]}
        raise Conflict(error_message)


def transform_ad_search_stats_result(args, query_result, querybuilder, results: {}):
    if "aggregations" in query_result:
        results["positions"] = int(query_result.get("aggregations", {}).get("positions", {}).get("value", 0))
        results["aggs"] = querybuilder.filter_aggs(
            query_result.get("aggregations", {}), args.get(constants.FREETEXT_QUERY)
        )

        for stat in args.get(constants.STATISTICS) or []:  # TODO: never executed by tests
            log.debug(f"Statistic for field: {stat}")
            if "stats" not in results:
                results["stats"] = []
            values = []
            for b in query_result.get("aggregations", {}).get(stat, {}).get("buckets", []):
                source = b.get("id_and_name", {}).get("hits", {}).get("hits", {})[0].get("_source")
                if isinstance(source.get(f.stats_field[stat], {}), list):
                    field = source.get(f.stats_field[stat], {})[0]
                else:
                    field = source.get(f.stats_field[stat], {})
                value = {
                    "term": field.get(f.stats_label[stat], ""),
                    "concept_id": field.get(f.stats_concept_id[stat], ""),
                    "code": b["key"],
                    "count": b["doc_count"],
                }
                values.append(value)
            results["stats"].append({"type": stat, "values": values})
    return results


def transform_ad_search_result(args, query_result, querybuilder):
    results = query_result.get("hits", {})
    results["took"] = query_result.get("took", 0)
    results["concepts"] = query_result.get("concepts", {})
    results = transform_ad_search_stats_result(args, query_result, querybuilder, results)
    _modify_results(results)
    return results


def _modify_results(results):
    for hit in results["hits"]:
        try:
            hit["_source"] = _format_ad(hit)
        except KeyError:
            pass
        except ValueError:
            pass
