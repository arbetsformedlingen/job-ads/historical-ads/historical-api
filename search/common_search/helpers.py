import re

RE_PLUS_MINUS = re.compile(r"((^| )[+-])", re.UNICODE)


def clean_plus_minus(text):
    """
    removes plus (+) and minus (-) signs from start of words
    """
    return RE_PLUS_MINUS.sub(" ", text).strip()
