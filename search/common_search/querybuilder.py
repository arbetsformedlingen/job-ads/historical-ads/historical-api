import datetime
import json
import logging
import re
import time
from collections import defaultdict
from enum import Enum, auto

import opensearch_dsl
from dateutil import parser

from common import constants, settings, fields as f, taxonomy
from common.date_and_time_handling import get_formatted_start_date, get_formatted_end_date, format_start_date, \
    format_end_date
from common.helpers import calculate_utc_offset
from search.common_search.taxonomy_municipalities import TaxonomyMunicipalities
from search.common_search.text_to_label import TextToLabel
from search.rest.model import queries

log = logging.getLogger(__name__)


class QueryBuilderType(Enum):
    # naming convention: <app>_<endpoint>
    HISTORICAL_SEARCH = auto()
    HISTORICAL_STATS = auto()

    @classmethod
    def is_search_endpoint(cls, type):
        return type in [cls.HISTORICAL_SEARCH]


class QueryBuilder(object):
    def __init__(self, text_to_concept=None, text_to_label: TextToLabel = None, taxonomy_municipalities: TaxonomyMunicipalities=None):
        self.ttc = text_to_concept
        self.ttl = text_to_label
        self.taxonomy_municipalities = taxonomy_municipalities

    def set_qb_type(self, qb_type):
        self.qb_type = qb_type

    def text_to_concepts(self, text):
        if self.ttc:
            return self.ttc.text_to_concepts(text)
        else:
            log.warning("text_to_concepts() failed. No Ontology initiated.")
            return {}

    def ontology_extracted_locations(self):
        if self.ttc:
            return self.ttc.ontology.extracted_locations
        else:
            log.warning("ontology.extracted_locations() failed. No Ontology initiated.")
            return set()

    def text_to_labels(self, text):
        if self.ttl:
            return self.ttl.text_to_labels(text)
        else:
            log.warning("text_to_labels() failed. No TextToLabel initiated.")
            return set()

    def parse_args(self, args, x_fields=None):
        """
        Parse arguments for query and return an opensearch query dsl
        Keyword arguments:
        args -- dictionary containing parameters from query
        """
        log.debug(f"Building query. QueryBuilderType: {self.qb_type.name}")
        query_dsl = self._bootstrap_query(args, x_fields)

        # Check for empty query
        if not any(v is not None for v in args.values()) or not args.get(constants.CONTEXTUAL_TYPEAHEAD, True):
            log.debug("Constructing match-all query")
            query_dsl['query']['bool']['must'].append({'match_all': {}})
            if 'sort' not in query_dsl:
                query_dsl['sort'] = [f.sort_options.get('pubdate-desc')]
            return query_dsl

        must_queries = list()

        # bool args
        abroad = self._parse_boolean_arg(args.get(constants.ABROAD))
        unspecified_workplace = self._parse_boolean_arg(args.get(constants.UNSPECIFIED_SWEDEN_WORKPLACE))
        remote = self._parse_boolean_arg(args.get(constants.REMOTE))
        open_for_all = self._parse_boolean_arg(args.get(constants.OPEN_FOR_ALL))  # todo: param name
        trainee = self._parse_boolean_arg(args.get(constants.TRAINEE))
        larling = self._parse_boolean_arg(args.get(constants.LARLING))
        franchise = self._parse_boolean_arg(args.get(constants.FRANCHISE))
        hire_work_place = self._parse_boolean_arg(args.get(constants.HIRE_WORK_PLACE))
        drivers_licence = self._parse_boolean_arg(args.get(taxonomy.DRIVING_LICENCE_REQUIRED))
        experience = self._parse_boolean_arg(args.get(constants.EXPERIENCE_REQUIRED))

        must_queries.append(
            self._build_freetext_query(args.get(constants.FREETEXT_QUERY),
                                       args.get(constants.FREETEXT_FIELDS),
                                       args.get(constants.X_FEATURE_FREETEXT_BOOL_METHOD),
                                       args.get(constants.X_FEATURE_DISABLE_SMART_FREETEXT),
                                       args.get(constants.X_FEATURE_ENABLE_FALSE_NEGATIVE, False)))
        must_queries.append(self._build_employer_query(args.get(constants.EMPLOYER)))

        must_queries.append(self._build_label_query(args.get(constants.LABEL)))

        must_queries.append(self._build_yrkes_query(args.get(taxonomy.OCCUPATION),
                                                    args.get(taxonomy.GROUP),
                                                    args.get(taxonomy.FIELD)))
        must_queries.append(self.build_yrkessamlingar_query(args.get(taxonomy.COLLECTION)))

        '''
        The parameters (PUBLISHED_AFTER, PUBLISHED_BEFORE) and (HISTORICAL_FROM, and HISTORICAL_TO) are duplicates.
        Therefore, we only add the query parameter for historical data if the published parameter is None.
        Note: The HISTORICAL_FROM and HISTORICAL_TO parameters will be removed in the future.
        '''
        from_datestring = get_formatted_start_date(args.get(constants.PUBLISHED_AFTER), args.get(constants.HISTORICAL_FROM))
        to_datestring = get_formatted_end_date(args.get(constants.PUBLISHED_BEFORE), args.get(constants.HISTORICAL_TO))

        must_queries.append(self._filter_timeframe(from_datestring, to_datestring))
        must_queries.append(self._build_parttime_query(args.get(constants.PARTTIME_MIN),
                                                       args.get(constants.PARTTIME_MAX)))
        must_queries.append(self._build_plats_query(args.get(taxonomy.MUNICIPALITY),
                                                    args.get(taxonomy.REGION),
                                                    args.get(taxonomy.COUNTRY),
                                                    unspecified_workplace,
                                                    abroad))
        must_queries.append(self._build_generic_bool_query(remote, 'remote_work'))
        must_queries.append(self._build_generic_bool_query(open_for_all, 'open_for_all'))
        must_queries.append(self._build_generic_bool_query(trainee, 'trainee'))
        must_queries.append(self._build_generic_bool_query(franchise, 'franchise'))
        must_queries.append(self._build_generic_bool_query(larling, 'larling'))
        must_queries.append(self._build_generic_bool_query(hire_work_place, 'hire_work_place'))

        must_queries.append(self._build_generic_query([f.MUST_HAVE_SKILLS + "." +
                                                       f.CONCEPT_ID + ".keyword",
                                                       f.MUST_HAVE_SKILLS + "." +
                                                       f.LEGACY_AMS_TAXONOMY_ID,
                                                       f.NICE_TO_HAVE_SKILLS + "." +
                                                       f.CONCEPT_ID + ".keyword",
                                                       f.NICE_TO_HAVE_SKILLS + "." +
                                                       f.LEGACY_AMS_TAXONOMY_ID],
                                                      args.get(taxonomy.SKILL)))
        must_queries.append(self._build_generic_query([f.MUST_HAVE_LANGUAGES + "." +
                                                       f.CONCEPT_ID + ".keyword",
                                                       f.MUST_HAVE_LANGUAGES + "." +
                                                       f.LEGACY_AMS_TAXONOMY_ID,
                                                       f.NICE_TO_HAVE_LANGUAGES + "." +
                                                       f.CONCEPT_ID + ".keyword",
                                                       f.NICE_TO_HAVE_LANGUAGES + "." +
                                                       f.LEGACY_AMS_TAXONOMY_ID],
                                                      args.get(taxonomy.LANGUAGE)))
        must_queries.append(self._build_generic_query([f.WORKING_HOURS_TYPE + "." +
                                                       f.CONCEPT_ID + ".keyword",
                                                       f.WORKING_HOURS_TYPE + "." +
                                                       f.LEGACY_AMS_TAXONOMY_ID],
                                                      args.get(taxonomy.WORKTIME_EXTENT)))
        must_queries.append(self._build_generic_query([f.DRIVING_LICENCE + "." +
                                                       f.CONCEPT_ID + ".keyword",
                                                       f.DRIVING_LICENCE + "." +
                                                       f.LEGACY_AMS_TAXONOMY_ID],
                                                      args.get(taxonomy.DRIVING_LICENCE)))
        must_queries.append(self._build_generic_query([f.EMPLOYMENT_TYPE + "." +
                                                       f.CONCEPT_ID + ".keyword",
                                                       f.EMPLOYMENT_TYPE + "." +
                                                       f.LEGACY_AMS_TAXONOMY_ID],
                                                      args.get(taxonomy.EMPLOYMENT_TYPE)))
        must_queries.append(self._build_generic_query([f.DURATION + '.' +
                                                      f.CONCEPT_ID + '.keyword',
                                                      f.DURATION + '.' +
                                                      f.LEGACY_AMS_TAXONOMY_ID],
                                                      args.get('duration')))

        seasonal_date_from = args.get(constants.START_SEASONAL_TIME)
        seasonal_date_to = args.get(constants.END_SEASONAL_TIME)

        if seasonal_date_from and seasonal_date_to:
            must_queries.append(self._add_years_timeframe(seasonal_date_from, seasonal_date_to))

        if isinstance(drivers_licence, bool):
            must_queries.append(
                {"term": {
                    f.DRIVING_LICENCE_REQUIRED: drivers_licence
                }}
            )

        if isinstance(experience, bool):
            must_queries.append({"term": {f.EXPERIENCE_REQUIRED: experience}})

        filter_queries = list()
        geo_filter = self._build_geo_dist_filter(args.get(constants.POSITION),
                                                 args.get(constants.POSITION_RADIUS))
        filter_queries.append(geo_filter)

        query_dsl = self._assemble_queries(query_dsl, must_queries, filter_queries)

        statistics = args.get(constants.STATISTICS)
        limit = args.get(constants.STAT_LMT) or 5
        if statistics:
            query_dsl['aggs'] = {stat: self._create_searchendpoint_aggs_query(stat, limit) for stat in statistics}

        return query_dsl

    @staticmethod
    def filter_aggs(aggs, freetext):
        # will not use in future
        fwords = freetext.split(' ') if freetext else []
        value_dicts = []
        for agg in aggs:
            if agg.startswith('complete_'):
                value_dicts += [{"type": agg[12:], **bucket}
                                for bucket in aggs[agg]['buckets']]
        filtered_aggs = []
        value_list = []
        for kv in sorted(value_dicts, key=lambda k: k['doc_count'], reverse=True):
            found_words = kv['key'].split(' ')
            value = ' '.join([w for w in found_words if w not in fwords])
            if kv['key'] not in fwords and value not in value_list:
                ac_hit = {
                    "value": value,
                    "found_phrase": kv['key'],
                    "type": kv['type'],
                    "occurrences": kv['doc_count']
                }
                value_list.append(value)
                filtered_aggs.append(ac_hit)

        if len(filtered_aggs) > 50:
            return filtered_aggs[0:50]
        return filtered_aggs

    def _parse_x_fields(self, x_fields):
        # Remove all spaces from field
        x_fields = re.sub(r'\s', '', x_fields).lower()
        if 'hits{' in x_fields:
            # Find out which fields are wanted
            hitsfields = self._find_hits_subelement(x_fields)
            # Remove lower nestings
            hitsfields = re.sub("[{].*?[}]", "", hitsfields)
            fieldslist = hitsfields.split(',')
            if f.AD_URL in fieldslist:
                fieldslist.append('id')

            return fieldslist
        return []

    @staticmethod
    def _find_hits_subelement(text):
        istart = []  # stack of indices of opening parentheses
        bracket_positions = {}
        for i, c in enumerate(text):
            if c == '{':
                istart.append(i)

            if c == '}':
                try:
                    bracket_positions[istart.pop()] = i
                except IndexError:
                    pass
        idx = text.find('hits{') + 4
        r = text[idx + 1:bracket_positions[idx]]
        return r

    def _bootstrap_query(self, args, x_fields):
        query_dsl = dict()
        query_dsl['from'] = args.pop(constants.OFFSET, 0)
        query_dsl['size'] = args.pop(constants.LIMIT, 10)
        query_dsl['track_total_hits'] = True
        query_dsl['track_scores'] = True

        if args.pop(constants.DETAILS, '') == queries.OPTIONS_BRIEF:
            query_dsl['_source'] = [f.ID, f.HEADLINE, f.APPLICATION_DEADLINE,
                                    f.EMPLOYMENT_TYPE + "." + f.LABEL,
                                    f.WORKING_HOURS_TYPE + "." + f.LABEL,
                                    f.EMPLOYER_NAME,
                                    f.PUBLICATION_DATE]

        if x_fields:
            query_dsl['_source'] = self._parse_x_fields(x_fields)

        # Remove api-key from args to make sure an empty query can occur
        api_key = 'api-key'
        if api_key in args:
            args.pop(api_key)

        query_dsl['query'] = {
            'bool': {
                'must': [],
                'filter': []
            }
        }

        query_dsl['aggs'] = {
            "positions": {
                "sum": {"field": f.NUMBER_OF_VACANCIES}
            }
        }

        if args.get(constants.SORT) and args.get(constants.SORT) in f.sort_options.keys():
            query_dsl['sort'] = f.sort_options.get(args.pop(constants.SORT))
        else:
            query_dsl['sort'] = f.sort_options.get('relevance')
        return query_dsl

    @staticmethod
    def _parse_boolean_arg(arg):
        if isinstance(arg, bool):
            return arg
        elif isinstance(arg, str):
            if arg.lower() == 'true':
                return True
            elif arg.lower() == 'false':
                return False
            else:
                return arg
        else:
            return arg

    @staticmethod
    def _escape_special_chars_for_complete(inputstr):
        escaped_str = inputstr
        chars_to_escape = ['#']

        for char in chars_to_escape:
            if char in inputstr:
                escaped_str = inputstr.replace(char, '[%s]' % char)
        return escaped_str

    @staticmethod
    def _assemble_queries(query_dsl, additional_queries, additional_filters):
        for query in additional_queries:
            if query:
                query_dsl['query']['bool']['must'].append(query)
        for af in additional_filters:
            if af:
                query_dsl['query']['bool']['filter'].append(af)
        return query_dsl

    @staticmethod
    def _rewrite_word_for_regex(word):
        if word is None:
            word = ''
        bad_chars = ['+', '.', '[', ']', '{', '}', '(', ')', '^', '$',
                     '*', '\\', '|', '?', '"', '\'', '&', '<', '>']
        if any(c in bad_chars for c in word):
            modded_term = ''
            for c in word:
                if c in bad_chars:
                    modded_term += '\\'
                modded_term += c
            return modded_term
        return word

    @staticmethod
    def extract_quoted_phrases(text):
        text = ' '.join([w.strip(',!?:;\' ') for w in re.split('\\s|\\,', text)])
        if text.count('"') == 1:
            if text[:1] == '"':
                text += '"'
            else:
                text = text.strip('"')
        must_matches = re.findall(r'\+\"(.+?)\"', text)
        neg_matches = re.findall(r'\-\"(.+?)\"', text)
        for neg_match in neg_matches:
            text = re.sub('-"%s"' % neg_match, '', text)
        for must_match in must_matches:
            text = re.sub(r'\+"%s"' % must_match, '', text)

        matches = re.findall(r'\"([^\"]+?)\"', text)
        for match in matches:
            text = re.sub(r'\"*%s\"*' % QueryBuilder._rewrite_word_for_regex(match), '', text)
        return {"phrases": matches, "phrases_must": must_matches,
                "phrases_must_not": neg_matches}, text.strip()

    # Parses FREETEXT_QUERY and FREETEXT_FIELDS
    def _build_freetext_query(self, querystring, queryfields, freetext_bool_method,
                              disable_smart_freetext, enable_false_negative=False):
        if not querystring:
            return None
        if not queryfields:
            queryfields = queries.QF_CHOICES.copy()

        (phrases, querystring) = self.extract_quoted_phrases(querystring)
        original_querystring = querystring
        if disable_smart_freetext:
            concepts = {}
        else:
            concepts = self.text_to_concepts(querystring)

        labels = self.text_to_labels(querystring)

        querystring = self._remove_identified_concepts_from_querystring(querystring, concepts)

        ft_query = self._create_base_ft_query(querystring, labels, concepts, freetext_bool_method)

        # Make all "musts" concepts "shoulds" as well
        for qf in queryfields:
            if qf in concepts:
                must_key = "%s_must" % qf
                concepts[qf] += [c for c in concepts.get(must_key, [])]

        # Add concepts to query
        for concept_type in queryfields:
            sub_should = self._freetext_concepts({"bool": {}}, concepts, [concept_type], labels, "should",
                                                 enable_false_negative)
            if 'should' in sub_should['bool']:
                if 'must' not in ft_query['bool']:
                    ft_query['bool']['must'] = []
                ft_query['bool']['must'].append(sub_should)
        # Remove unwanted concepts from query
        self._freetext_concepts(ft_query, concepts, queryfields, labels, 'must_not', enable_false_negative)
        # Require musts
        self._freetext_concepts(ft_query, concepts, queryfields, labels, 'must', enable_false_negative)

        self._add_phrases_query(ft_query, phrases)
        freetext_headline = phrases.get('phrases', [])
        ft_query = self._freetext_headline(ft_query, freetext_headline, original_querystring)
        return ft_query

    # Add phrase queries
    @staticmethod
    def _add_phrases_query(ft_query, phrases):
        for bool_type in ['should', 'must', 'must_not']:
            key = 'phrases' if bool_type == 'should' else "phrases_%s" % bool_type

            for phrase in phrases[key]:
                if bool_type not in ft_query['bool']:
                    ft_query['bool'][bool_type] = []
                ft_query['bool'][bool_type].append({"multi_match":
                                                        {"query": phrase,
                                                         "fields": [
                                                             "headline", "description.text", "employer.name", "original_id" ],
                                                         "type": "phrase"}})

        return ft_query

    def _remove_identified_concepts_from_querystring(self, querystring, concepts):
        # Sort all concepts by string length
        all_concepts = sorted(concepts.get('occupation', []) +
                              concepts.get('occupation_must', []) +
                              concepts.get('occupation_must_not', []) +
                              concepts.get('skill', []) +
                              concepts.get('skill_must', []) +
                              concepts.get('skill_must_not', []) +
                              concepts.get('location', []) +
                              concepts.get('location_must', []) +
                              concepts.get('location_must_not', []),
                              key=lambda c: len(c),
                              reverse=True)
        # Remove found concepts from querystring
        for term in [concept['term'] for concept in all_concepts]:
            querystring = self._remove_term_from_querystring(term, querystring)

        # Remove duplicate spaces
        querystring = self._remove_duplicate_spaces(querystring)
        return querystring

    def _remove_duplicate_spaces(self, querystring):
        return re.sub('\\s+', ' ', querystring).strip()

    def _remove_term_from_querystring(self, term, querystring):
        clean_term = self._rewrite_word_for_regex(term)
        p = re.compile(f'(^|\\s+)(\\+{clean_term}|\\-{clean_term}|{clean_term})(\\s+|$)')
        querystring = p.sub('\\1\\3', querystring).strip()
        return querystring

    def _create_base_ft_query(self, querystring, labels, concepts, method):
        method = 'and' if method == 'and' else constants.DEFAULT_FREETEXT_BOOL_METHOD
        # Creates a base query dict for "independent" freetext words
        # (e.g. words not found in text_to_concepts)
        suffix_words = ' '.join([w[1:] for w in querystring.split(' ')
                                 if w.startswith('*')])
        prefix_words = ' '.join([w[:-1] for w in querystring.split(' ')
                                 if w and w.endswith('*')])
        inc_words = ' '.join([w for w in querystring.split(' ')
                              if w and not w.startswith('+')
                              and not w.startswith('-') and not w.startswith('*')
                              and not w.endswith('*')])
        req_words = ' '.join([w[1:] for w in querystring.split(' ')
                              if w.startswith('+')
                              and w[1:].strip()])
        exc_words = ' '.join([w[1:] for w in querystring.split(' ')
                              if w.startswith('-')
                              and w[1:].strip()])
        shoulds = self._freetext_fields(inc_words, method) if inc_words else []
        musts = self._freetext_fields(req_words, method) if req_words else []
        mustnts = self._freetext_fields(exc_words, method) if exc_words else []

        shoulds_labels = self._create_freetext_labels_query_values(labels, concepts, 'should')
        mustnts_labels = self._create_freetext_labels_query_values(labels, concepts, 'must_not')

        ft_query = {"bool": {}}
        # Add "common" words to query
        if shoulds or musts or prefix_words or suffix_words:
            ft_query['bool']['must'] = []

        if shoulds or shoulds_labels:
            # Include all "must" words in should, to make sure any single "should"-word
            # not becomes exclusive
            if 'must' not in ft_query['bool']:
                ft_query['bool']['must'] = []
            shoulds_node = {"bool": {"should": shoulds + musts}}
            ft_query['bool']['must'].append(shoulds_node)
            if shoulds_labels:
                shoulds_node['bool']['should'] += shoulds_labels

        # Wildcards after shoulds so they don't end up there
        if prefix_words:
            musts += self._freetext_wildcard(prefix_words, "prefix", method)
        if suffix_words:
            musts += self._freetext_wildcard(suffix_words, "suffix", method)
        if musts:
            ft_query['bool']['must'].append({"bool": {"must": musts}})
        if mustnts or mustnts_labels:
            ft_query['bool']['must_not'] = mustnts
            if mustnts_labels:
                ft_query['bool']['must_not'] += mustnts_labels

        return ft_query

    @staticmethod
    def _freetext_fields(searchword, method=constants.DEFAULT_FREETEXT_BOOL_METHOD):
        return [{
            "multi_match": {
                "query": searchword,
                "type": "cross_fields",
                "operator": method,
                "fields": [f.HEADLINE + "^3", f.KEYWORDS_EXTRACTED + ".employer^2",
                           f.DESCRIPTION_TEXT, f.ID, f.EXTERNAL_ID, f.SOURCE_TYPE,
                           f.KEYWORDS_EXTRACTED + ".location^5", "original_id"]
            }
        }]

    @staticmethod
    def _freetext_wildcard(searchword, wildcard_side, method=constants.DEFAULT_FREETEXT_BOOL_METHOD):
        return [{
            "multi_match": {
                "query": searchword,
                "type": "cross_fields",
                "operator": method,
                "fields": [f.HEADLINE + "." + wildcard_side,
                           f.DESCRIPTION_TEXT + "." + wildcard_side]
            }
        }]

    @staticmethod
    def _freetext_headline(query_dict, freetext_headline, querystring):
        # Remove plus and minus from querystring for headline search
        freetext_headline = ' '.join(
            ['"' + re.sub(r'(^| )[\\+]{1}', ' ', string) + '"' for string in freetext_headline])
        querystring = freetext_headline + re.sub(r'(^| )[\\+]{1}', ' ', querystring)
        if 'must' not in query_dict['bool']:
            query_dict['bool']['must'] = []

        for should in query_dict['bool']['must']:
            try:
                match = {
                    "match": {
                        f.HEADLINE + ".words": {
                            "query": querystring.strip(),
                            "operator": "and",
                            "boost": 5
                        }
                    }
                }
                if 'should' in should.get('bool', {}):
                    should['bool']['should'].append(match)

            except KeyError as e:
                log.error(f"No bool clause for headline query. Error: {e}")
        return query_dict

    def _extract_freetext_labels_should(self, labels) -> dict:
        labels_to_handle = labels.get("label", [])
        extracted_labels_dict = {}
        for label in labels_to_handle:
            extracted_labels_dict[label['term'].lower()] = label

        return extracted_labels_dict

    def _create_freetext_labels_query_values(self, labels, concepts, bool_type):
        # Make sure that we don't add labels that clashes with terms in concepts
        all_concept_terms = set()
        if bool_type == 'should':
            concept_keys = ['occupation', 'skill', 'location']
            for concept_key in concept_keys:
                terms = [c['term'].lower() for c in concepts.get(concept_key, [])]
                for term in terms:
                    all_concept_terms.add(term)

        key = "label"
        dict_key = f"{key}_{bool_type}" if bool_type != 'should' else key

        labels_to_handle = labels.get(dict_key, [])
        labels_to_return = []

        for label in labels_to_handle:
            label_term_lower = label['term'].lower()
            if not label_term_lower in all_concept_terms:
                value = label['label']
                boost_value = 10
                search_value = {
                    "term": {
                        f"{f.LABEL}": {
                            "value": value,
                            "boost": boost_value
                        }
                    }
                }
                labels_to_return.append(search_value)

        return labels_to_return

    def _freetext_concepts(self, query_dict, concepts, concept_keys, labels, bool_type, enable_false_negative=False):
        if labels['label'] and bool_type == 'should':
            extracted_labels_should = self._extract_freetext_labels_should(labels)
        else:
            extracted_labels_should = {}

        for key in concept_keys:
            dict_key = "%s_%s" % (key, bool_type) if bool_type != 'should' else key
            current_concepts = [c for c in concepts.get(dict_key, []) if c]
            for concept in current_concepts:
                if bool_type not in query_dict['bool']:
                    query_dict['bool'][bool_type] = []

                base_fields = []
                if key == 'location' and bool_type != 'must':
                    base_fields.append(f.KEYWORDS_EXTRACTED)
                    base_fields.append(f.KEYWORDS_ENRICHED)
                    # Add freetext search for location that does not exist
                    # in extracted locations, for example 'kallhäll'.
                    value = concept['term'].lower()
                    if value not in self.ttc.get_extracted_ads_locations():
                        geo_ft_query = self._freetext_fields(value)
                        query_dict['bool'][bool_type].append(geo_ft_query[0])
                elif key == 'occupation' and bool_type != 'must':
                    base_fields.append(f.KEYWORDS_EXTRACTED)
                    base_fields.append(f.KEYWORDS_ENRICHED)
                else:
                    curr_base_field = f.KEYWORDS_EXTRACTED \
                        if key in ['employer'] else f.KEYWORDS_ENRICHED
                    base_fields.append(curr_base_field)

                for base_field in base_fields:
                    term_lower = concept['term'].lower()
                    if base_field == f.KEYWORDS_EXTRACTED:
                        value = term_lower
                        boost_value = 10
                    else:
                        value = concept['concept'].lower()
                        boost_value = 9

                    field = "%s.%s.raw" % (base_field, key)
                    query_dict['bool'][bool_type].append(
                        {
                            "term": {
                                field: {
                                    "value": value,
                                    "boost": boost_value
                                }
                            }
                        }
                    )
                    if term_lower in extracted_labels_should:
                        query_dict['bool'][bool_type].append(
                            {
                                "term": {
                                    f"{f.LABEL}": {
                                        "value": extracted_labels_should[term_lower]['label'],
                                        "boost": 10
                                    }
                                }
                            }
                        )

                    if enable_false_negative and base_field == f.KEYWORDS_ENRICHED and (
                            key == 'skill'):
                        # Add extra search for the current known term in headline, employer and description to be sure
                        # not to miss search hits where the term wasn't identified during enrichment. Only search
                        # skills to avoid irrelevant hits on occupations and locations...
                        query_dict['bool'][bool_type].append(
                            {'multi_match': {
                                'query': concept['term'].lower(),
                                'type': 'cross_fields',
                                'operator': 'and',
                                'fields': [
                                    'headline^0.1',
                                    'keywords.extracted.employer^0.1',
                                    'description.text^0.1'
                                ]
                            }}
                        )
        return query_dict

    # Parses EMPLOYER
    @staticmethod
    def _build_employer_query(employers):
        if employers:
            bool_segment = {"bool": {"should": [], "must_not": [], "must": []}}
            for employer in employers:
                negative_search = employer.startswith('-')
                positive_search = employer.startswith('+')
                bool_type = 'should'
                if negative_search or positive_search:
                    employer = employer[1:]
                    bool_type = 'must_not' if negative_search else 'must'
                if employer.isdigit():
                    bool_segment['bool'][bool_type].append(
                        {"prefix": {f.EMPLOYER_ORGANIZATION_NUMBER: employer}}
                    )
                else:
                    bool_segment['bool'][bool_type].append(
                        {
                            "multi_match": {
                                "query": " ".join(employers),
                                "operator": "or",
                                "fields": [f.EMPLOYER_NAME,
                                           f.EMPLOYER_WORKPLACE]
                            }
                        }
                    )
            return bool_segment
        return None

    @staticmethod
    def _build_label_query(labels):
        """Parse the labels of the query ('nyckelord' from Ledigtarbete)."""
        if not labels:
            return None

        bool_segment = {"bool":{'must':[]}}
        for label in labels:
            bool_segment['bool']['must'].append(
                {
                    "term": {
                        f"{f.LABEL}": {
                            "value": label.lower()
                        }
                    }
                }
            )

        return bool_segment

    # Parses OCCUPATION, FIELD, GROUP
    @staticmethod
    def _build_yrkes_query(yrkesroller, yrkesgrupper, yrkesomraden):
        # TODO: default values
        yrken = yrkesroller or []
        yrkesgrupper = yrkesgrupper or []
        yrkesomraden = yrkesomraden or []
        # Todo f-string formatting
        yrke_term_query = [{
            "term": {
                f.OCCUPATION + "." + f.CONCEPT_ID + ".keyword": {
                    "value": y,
                    "boost": 2.0}}} for y in yrken if y and not y.startswith('-')]
        yrke_term_query += [{
            "term": {
                f.OCCUPATION + "." + f.LEGACY_AMS_TAXONOMY_ID: {
                    "value": y,
                    "boost": 2.0}}} for y in yrken if y and not y.startswith('-')]
        yrke_term_query += [{
            "term": {
                f.OCCUPATION_GROUP + "." + f.CONCEPT_ID + ".keyword": {
                    "value": y,
                    "boost": 1.0}}} for y in yrkesgrupper if y and not y.startswith('-')]
        yrke_term_query += [{
            "term": {
                f.OCCUPATION_GROUP + "." + f.LEGACY_AMS_TAXONOMY_ID: {
                    "value": y,
                    "boost": 1.0}}} for y in yrkesgrupper if y and not y.startswith('-')]
        yrke_term_query += [{
            "term": {
                f.OCCUPATION_FIELD + "." + f.CONCEPT_ID + ".keyword": {
                    "value": y,
                    "boost": 1.0}}} for y in yrkesomraden if y and not y.startswith('-')]
        yrke_term_query += [{
            "term": {
                f.OCCUPATION_FIELD + "." + f.LEGACY_AMS_TAXONOMY_ID: {
                    "value": y,
                    "boost": 1.0}}} for y in yrkesomraden if y and not y.startswith('-')]
        neg_yrke_term_query = [{
            "term": {
                f.OCCUPATION + "." + f.CONCEPT_ID + ".keyword": {
                    "value": y[1:]}}} for y in yrken if y and y.startswith('-')]
        neg_yrke_term_query += [{
            "term": {
                f.OCCUPATION + "." + f.LEGACY_AMS_TAXONOMY_ID: {
                    "value": y[1:]}}} for y in yrken if y and y.startswith('-')]
        neg_yrke_term_query += [{
            "term": {
                f.OCCUPATION_GROUP + "." + f.CONCEPT_ID + ".keyword": {
                    "value": y[1:]}}} for y in yrkesgrupper if y and y.startswith('-')]
        neg_yrke_term_query += [{
            "term": {
                f.OCCUPATION_GROUP + "." + f.LEGACY_AMS_TAXONOMY_ID: {
                    "value": y[1:]}}} for y in yrkesgrupper if y and y.startswith('-')]
        neg_yrke_term_query += [{
            "term": {
                f.OCCUPATION_FIELD + "." + f.CONCEPT_ID + ".keyword": {
                    "value": y[1:]}}} for y in yrkesomraden if y and y.startswith('-')]
        neg_yrke_term_query += [{
            "term": {
                f.OCCUPATION_FIELD + "." + f.LEGACY_AMS_TAXONOMY_ID: {
                    "value": y[1:]}}} for y in yrkesomraden if y and y.startswith('-')]

        if yrke_term_query or neg_yrke_term_query:
            query = {'bool': {}}
            if yrke_term_query:
                query['bool']['should'] = yrke_term_query
            if neg_yrke_term_query:
                query['bool']['must_not'] = neg_yrke_term_query
            return query

        else:
            return None

    # Parses COLLECTIONS
    @staticmethod
    def build_yrkessamlingar_query(yrkessamlingar):
        start_time = int(time.time() * 1000)
        yrkessamlingar = yrkessamlingar or []
        yrkessamling_term_query = [{
            "term": {
                f.OCCUPATION_COLLECTION + "." + f.CONCEPT_ID + ".keyword": {
                    "value": y,
                    "boost": 2.0}}} for y in yrkessamlingar if y and not y.startswith('-')]
        yrkessamling_term_query += [{
            "term": {
                f.OCCUPATION_COLLECTION + "." + f.LEGACY_AMS_TAXONOMY_ID: {
                    "value": y,
                    "boost": 2.0}}} for y in yrkessamlingar if y and not y.startswith('-')]
        neg_yrkessamling_term_query = [{
            "term": {
                f.OCCUPATION_COLLECTION + "." + f.CONCEPT_ID + ".keyword": {
                    "value": y[1:]}}} for y in yrkessamlingar if y and y.startswith('-')]
        neg_yrkessamling_term_query += [{
            "term": {
                f.OCCUPATION_COLLECTION + "." + f.LEGACY_AMS_TAXONOMY_ID: {
                    "value": y[1:]}}} for y in yrkessamlingar if y and y.startswith('-')]
        if yrkessamling_term_query or neg_yrkessamling_term_query:
            query = {'bool': {}}
            if yrkessamling_term_query:
                query['bool']['should'] = yrkessamling_term_query
            if neg_yrkessamling_term_query:
                query['bool']['must_not'] = neg_yrkessamling_term_query

            log.debug(f"Occupation-collections. Query built in: {int(time.time() * 1000 - start_time)} ms.")
            return query
        else:
            return None

    # Parses MUNICIPALITY and REGION
    def _build_plats_query(self, kommunkoder_input, lanskoder_input, landskoder_input, unspecify_input, abroad_input):
        kommuner = []
        neg_kommuner = []
        lan = []
        neg_lan = []
        land = []
        neg_land = []
        for kkod in kommunkoder_input if kommunkoder_input else []:
            if kkod.startswith('-'):
                neg_kommuner.append(kkod[1:])
            else:
                kommuner.append(kkod)
        for lkod in lanskoder_input if lanskoder_input else []:
            if lkod.startswith('-'):
                neg_lan.append(lkod[1:])
            else:
                lan.append(lkod)
        for ckod in landskoder_input if landskoder_input else []:
            if ckod.startswith('-'):
                neg_land.append(ckod[1:])
            else:
                land.append(ckod)

        # Note: Many older ads does not have a value for municipality_concept_id in the data (like 'mBKv_q3B_SK8'), but they all have "code" (like '0140').
        # For that reason, translate eventual input of municipality_concept_id to municipality_code for the field WORKPLACE_ADDRESS_MUNICIPALITY_CODE.
        kommuner_code = self.create_translated_municipality_list(kommuner)
        neg_kommuner_code = self.create_translated_municipality_list(neg_kommuner)

        plats_term_query = [{"term": {
            f.WORKPLACE_ADDRESS_MUNICIPALITY_CODE: {
                "value": kkod, "boost": 2.0}}} for kkod in kommuner_code]
        plats_term_query += [{"term": {
            f.WORKPLACE_ADDRESS_MUNICIPALITY_CONCEPT_ID: {
                "value": kkod, "boost": 2.0}}} for kkod in kommuner]

        # Add unspecified field, when it is true, system will return all adds with unspecified in Sweden.
        if unspecify_input:
            plats_term_query += [
                {
                    "bool": {
                        "filter": {"term": {f.WORKPLACE_ADDRESS_COUNTRY_CONCEPT_ID: {
                            "value": constants.SWEDEN_CONCEPT_ID}}},
                        "must_not": {"exists": {"field": f.WORKPLACE_ADDRESS_REGION_CONCEPT_ID}},
                        "boost": 1.0
                    }
                },
            ]

        if abroad_input:
            plats_term_query += [
                {
                    "bool": {
                        "must_not": {"term": {f.WORKPLACE_ADDRESS_COUNTRY_CONCEPT_ID: {
                            "value": constants.SWEDEN_CONCEPT_ID}}},
                        "boost": 1.0
                    }
                },
            ]

        plats_term_query += [{"term": {
            f.WORKPLACE_ADDRESS_REGION_CODE: {
                "value": lkod, "boost": 1.0}}} for lkod in lan]
        plats_term_query += [{"term": {
            f.WORKPLACE_ADDRESS_REGION_CONCEPT_ID: {
                "value": lkod, "boost": 1.0}}} for lkod in lan]

        plats_term_query += [{"term": {
            f.WORKPLACE_ADDRESS_COUNTRY_CODE: {
                "value": ckod, "boost": 1.0}}} for ckod in land]
        plats_term_query += [{"term": {
            f.WORKPLACE_ADDRESS_COUNTRY_CONCEPT_ID: {
                "value": ckod, "boost": 1.0}}} for ckod in land]

        plats_bool_query = {"bool": {
            "should": plats_term_query}
        } if plats_term_query else {}
        neg_komm_term_query = []
        neg_lan_term_query = []
        neg_land_term_query = []
        if neg_kommuner:
            neg_komm_term_query = [{"term": {
                f.WORKPLACE_ADDRESS_MUNICIPALITY_CODE: {
                    "value": kkod}}} for kkod in neg_kommuner_code]
            neg_komm_term_query += [{"term": {
                f.WORKPLACE_ADDRESS_MUNICIPALITY_CONCEPT_ID: {
                    "value": kkod}}} for kkod in neg_kommuner]
        if neg_lan:
            neg_lan_term_query = [{"term": {
                f.WORKPLACE_ADDRESS_REGION_CODE: {
                    "value": lkod}}} for lkod in neg_lan]
            neg_lan_term_query += [{"term": {
                f.WORKPLACE_ADDRESS_REGION_CONCEPT_ID: {
                    "value": lkod}}} for lkod in neg_lan]

        if neg_land:
            neg_land_term_query = [{"term": {
                f.WORKPLACE_ADDRESS_COUNTRY_CODE: {
                    "value": ckod}}} for ckod in neg_land]
            neg_land_term_query += [{"term": {
                f.WORKPLACE_ADDRESS_COUNTRY_CONCEPT_ID: {
                    "value": ckod}}} for ckod in neg_land]

        if neg_komm_term_query or neg_lan_term_query or neg_land_term_query:
            if 'bool' not in plats_bool_query:
                plats_bool_query['bool'] = {}
            plats_bool_query['bool'][
                'must_not'] = neg_komm_term_query + neg_lan_term_query + neg_land_term_query
        return plats_bool_query


    def create_translated_municipality_list(self, kommunkoder_input):
        kommuner_code = []
        for kkod in kommunkoder_input:
            translated_kkod = kkod
            # kkod could either be a concept_id or a municipality_code
            if len(kkod.strip()) > 4:
                # kkod is a concept_id
                taxonomy_municipality = self.taxonomy_municipalities.get_municipality_by_concept_id(kkod)
                if taxonomy_municipality:
                    translated_kkod = taxonomy_municipality["lau_2_code_2015"]

            kommuner_code.append(translated_kkod)
        return kommuner_code

    @staticmethod
    def _build_generic_bool_query(bool_param, query_term):
        if bool_param is None:
            """
            If parameter is not used, search results should not be affected,
            check for None is included to make True/False code identical
            """
            pass
        else:
            """
            Both True and False for the boolean parameter are "must".
            True: only ads with that parameter set to "True" shall be included
            False: No ads with that parameter shall be included
            """
            bool_query = {
                "bool": {
                    "must": {"term": {query_term: bool_param}},
                }
            }
            return bool_query

    # Parses PUBLISHED_AFTER and PUBLISHED_BEFORE
    @staticmethod
    def _filter_timeframe(from_datestring, to_datestring):
        if not from_datestring and not to_datestring:
            return None

        range_query = {"range": {f.PUBLICATION_DATE: {}}}
        from_datetime = parser.parse(from_datestring) if from_datestring else None
        to_datetime = parser.parse(to_datestring) if to_datestring else None

        if from_datetime:
            log.debug(f"Filter ads from: {from_datetime}")
            range_query['range'][f.PUBLICATION_DATE]['gte'] = from_datetime.isoformat()
        if to_datetime:
            log.debug(f"Filter ads to: {to_datetime}")
            range_query['range'][f.PUBLICATION_DATE]['lte'] = to_datetime.isoformat()

        return range_query

    @staticmethod
    def _add_years_timeframe(seasonal_date_from, seasonal_date_to):
        should = []
        for year in range(settings.HISTORICAL_START_YEAR, datetime.datetime.now().year + 1):
            from_formatted = format_start_date(str(year) + '-' + seasonal_date_from)
            to_formatted = format_end_date(str(year) + '-' + seasonal_date_to)
            should.append(QueryBuilder._filter_timeframe(from_formatted, to_formatted))
        return {'bool': {'should': should}}

    # Parses PARTTIME_MIN and PARTTIME_MAX
    @staticmethod
    def _build_parttime_query(parttime_min, parttime_max):
        if not parttime_min and not parttime_max:
            return None
        if not parttime_min:
            parttime_min = 0.0
        if not parttime_max:
            parttime_max = 100.0
        parttime_query = {
            "bool": {
                "must": [
                    {
                        "range": {
                            f.SCOPE_OF_WORK_MIN: {
                                "lte": parttime_max,
                                "gte": parttime_min
                            },
                        }
                    },
                    {
                        "range": {
                            f.SCOPE_OF_WORK_MAX: {
                                "lte": parttime_max,
                                "gte": parttime_min
                            }
                        }
                    }
                ]
            }
        }
        return parttime_query

    @staticmethod
    def _build_generic_query(keys, itemlist):
        items = [] if not itemlist else itemlist
        term_query = []
        neg_term_query = []
        if isinstance(keys, str):
            keys = [keys]
        for key in keys:
            term_query += [{"term": {key: {"value": item}}}
                           for item in items if not item.startswith('-')]

            neg_term_query += [{"term": {key: {"value": item[1:]}}}
                               for item in items if item.startswith('-')]

        if term_query or neg_term_query:
            query = {'bool': {}}
            if term_query:
                query['bool']['should'] = term_query
            if neg_term_query:
                query['bool']['must_not'] = neg_term_query
            return query

        return None

    # Parses POSITION and POSITION_RADIUS
    @staticmethod
    def _build_geo_dist_filter(positions, coordinate_ranges):
        geo_bool = {"bool": {"should": []}} if positions else {}
        for index, position in enumerate(positions or []):
            latitude = None
            longitude = None
            coordinate_range = coordinate_ranges[index] \
                if coordinate_ranges is not None and index < len(coordinate_ranges) \
                else constants.DEFAULT_POSITION_RADIUS
            if position and ',' in position:
                try:
                    latitude = float(re.split(', ?', position)[0])
                    longitude = float(re.split(', ?', position)[1])
                except ValueError as e:
                    log.warning(f"Bad position-parameter: {position}. Error: {e}")

            geo_filter = {}
            if not latitude or not longitude or not coordinate_range:
                return {}
            elif ((-90 <= latitude <= 90)
                  and (-180 <= longitude <= 180) and (coordinate_range > 0)):
                geo_filter["geo_distance"] = {
                    "distance": str(coordinate_range) + "km",
                    # OBS! order in REST request: latitude,longitude
                    f.WORKPLACE_ADDRESS_COORDINATES: [longitude, latitude]
                }
            if geo_filter:
                geo_bool['bool']['should'].append(geo_filter)
        return geo_bool

    @staticmethod
    def create_auto_complete_suggester(word):
        """"
        parse args and create auto complete suggester
        """
        enriched_typeahead_field = f.KEYWORDS_ENRICHED_SYNONYMS

        fields = ['compound', ]
        search = opensearch_dsl.Search()
        search = search.source('suggest')
        for field in fields:
            search = search.suggest(
                '%s-suggest' % field,
                word,
                completion={
                    'field': '%s.%s.suggest' % (enriched_typeahead_field, field),
                    "skip_duplicates": True,
                    "size": 50,
                    "fuzzy": {
                        "min_length": 3,
                        "prefix_length": 0
                    }
                }
            )
        return search.to_dict()

    @staticmethod
    def create_phrase_suggester(input_words):
        """"
        parse args and create phrase suggester
        """
        enriched_typeahead_field = f.KEYWORDS_ENRICHED_SYNONYMS

        field = '%s.compound' % enriched_typeahead_field
        search = opensearch_dsl.Search()
        search = search.source('suggest')
        search = search.suggest(
            '%s_simple_phrase' % field,
            input_words,
            phrase={
                'field': '%s.trigram' % field,
                'size': 10,
                'max_errors': 2,
                'direct_generator': [{
                    'field': '%s.trigram' % field,
                    'suggest_mode': 'always',
                    'min_word_length': 1
                }, {
                    'field': '%s.reverse' % field,
                    'suggest_mode': 'always',
                    'pre_filter': 'reverse',
                    'post_filter': 'reverse',
                    'min_word_length': 1
                }]
            }
        )
        return search.to_dict()

    @staticmethod
    def create_suggest_search(suggest):
        enriched_typeahead_field = f.KEYWORDS_ENRICHED_SYNONYMS

        field = '%s.compound' % enriched_typeahead_field
        search = defaultdict(dict)
        query = search.setdefault('query', {})
        match = query.setdefault('match', {})
        field = match.setdefault(field, {})
        field['query'] = suggest
        field['operator'] = 'and'

        return json.dumps(search)

    @staticmethod
    def create_check_search_word_type_query(word):
        """"
            Create check search word type query
        """
        enriched_typeahead_field = f.KEYWORDS_ENRICHED_SYNONYMS
        search = defaultdict(dict)
        aggs = search.setdefault('aggs', {})
        for field in ('location', 'skill', 'occupation'):
            aggs['search_type_%s' % field] = {
                'terms': {
                    'field': '%s.%s.raw' % (enriched_typeahead_field, field),
                    'include': QueryBuilder._rewrite_word_for_regex(word)
                }
            }
        return json.dumps(search)

    @staticmethod
    def create_suggest_extra_word_query(word, first_word_type, second_word_type):
        """"
           Create suggest extra word query
        """
        enriched_typeahead_field = f.KEYWORDS_ENRICHED_SYNONYMS
        search = defaultdict(dict)
        aggs = search.setdefault('aggs', {})
        first_word = aggs.setdefault('first_word', {})
        first_word['filter'] = {
            'term': {
                '%s.%s.raw' % (enriched_typeahead_field, first_word_type): word,
            }
        }
        first_word['aggs'] = {
            'second_word': {
                'terms': {
                    'field': '%s.%s.raw' % (enriched_typeahead_field, second_word_type),
                    'size': 6
                }
            }
        }
        return json.dumps(search)

    @staticmethod
    def _currently_published_query_filter():
        # Make sure to only serve published ads
        offset = calculate_utc_offset()
        return {
            'bool': {
                'must': [],
                'filter': [
                    {
                        'range': {
                            f.PUBLICATION_DATE: {
                                'lte': 'now+%dH/m' % offset
                            }
                        }
                    },
                    {
                        'range': {
                            f.LAST_PUBLICATION_DATE: {
                                'gte': 'now+%dH/m' % offset
                            }
                        }
                    },
                    {
                        'term': {
                            f.REMOVED: False
                        }
                    },
                ]
            },
        }

    def _typeahead_aggs(self, args):
        complete_aggs = {}
        complete_string = args.get(constants.TYPEAHEAD_QUERY)
        complete_fields = args.get(constants.FREETEXT_FIELDS)

        if not complete_fields:
            complete_fields = queries.QF_CHOICES.copy()
            complete_fields.remove('employer')

        complete_string = self._rewrite_word_for_regex(complete_string)
        word_list = complete_string.split(' ')
        complete = word_list[-1]

        ngrams_complete = []
        for n in list(range(len(word_list) - 1)):
            ngrams_complete.append(' '.join(word_list[n:]))

        size = 60 // len(complete_fields)

        enriched_typeahead_field = f.KEYWORDS_ENRICHED_SYNONYMS

        for field in complete_fields:
            base_field = f.KEYWORDS_EXTRACTED \
                if field in ['employer'] else enriched_typeahead_field

            complete_aggs["complete_00_%s" % field] = {
                "terms": {
                    "field": "%s.%s.raw" % (base_field, field),
                    "size": size,
                    "include": "%s.*" % self._escape_special_chars_for_complete(complete)
                }
            }
            x = 1
            for ngram in ngrams_complete:
                if ngram != complete:
                    complete_aggs["complete_%s_%s_remainder" % (str(x).zfill(2), field)] = {
                        "terms": {
                            "field": "%s.%s.raw" % (base_field, field),
                            "size": size,
                            "include": "%s.*" % self._escape_special_chars_for_complete(ngram)
                        }
                    }
                    x += 1
        return complete_aggs

    @staticmethod
    def _create_searchendpoint_aggs_query(stat, limit):
        return {
            "terms": {
                "field": f.stats_options[stat],
                "size": limit
            },
            "aggs": {
                "id_and_name": {
                    "top_hits": {
                        "size": 1,
                        "_source": {
                            "includes": [f.stats_concept_id_options[stat], f.stats_concept_name_options[stat]]}
                    }
                }
            }
        }
