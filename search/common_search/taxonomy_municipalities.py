import logging

from common import settings
from search.common_search.taxonomy_lookup import TaxonomyLookup

log = logging.getLogger(__name__)


class TaxonomyMunicipalities(object):
    municipalities = None

    def __init__(self):
        pass

    def _fetch_municipalities_from_taxonomy(self):
        taxonomy_lookup = TaxonomyLookup()
        tax_municipalities = taxonomy_lookup.fetch_taxonomy_values(MUNICIPALITY_QUERY % settings.TAXONOMY_VERSION)
        log.info("Fetched %s municipalities from taxonomy" % len(tax_municipalities))
        return tax_municipalities

    def get_municipality_by_concept_id(self, concept_id):
        if not self.municipalities:
            self.municipalities = self._fetch_municipalities_from_taxonomy()

        for municipality in self.municipalities:
            if "id" in municipality and municipality["id"] == concept_id:
                return municipality
        return None


MUNICIPALITY_QUERY = """query municipality {
  concepts(type: "municipality", include_deprecated: true, version: %s) {
    id
    preferred_label
    deprecated_legacy_id
    lau_2_code_2015
    broader{
      id
      preferred_label
      deprecated_legacy_id
      national_nuts_level_3_code_2019
    }
  }
  }
}
"""
