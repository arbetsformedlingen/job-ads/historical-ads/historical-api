import logging
import io
import os
import requests
from flask import send_file
from werkzeug.exceptions import ServiceUnavailable
from common import settings
from search.common_search.ad_search import fetch_ad_by_id

log = logging.getLogger(__name__)

current_dir = os.path.dirname(os.path.realpath(__file__)) + '/'

not_found_file = None


def format_base_url(base_url: str = settings.COMPANY_LOGO_BASE_URL) -> str:
    """
    COMPANY_LOGO_BASE_URL is an environment variable with no checks.
    if it ends with a slash, remove that slash, otherwise don't change it
    Necessary slash is added when 'eventual_logo_url' is built (making that part of the code more readable
    """
    if base_url.endswith("/"):
        base_url = base_url[:-1]
    return base_url


base_url = format_base_url()


def get_correct_logo_url(ad: dict) -> str:
    logo_url = None
    if eventual_logo_url := logo_url_creator(ad):
        logo_url = get_eventual_logo_url(eventual_logo_url)
    return logo_url


def get_eventual_logo_url(eventual_logo_url: str):
    """
    This function makes a call to the logo endpoint to check response
    Download (if response OK to request) is made later
    """
    try:
        r = requests.head(eventual_logo_url, timeout=10)
    except requests.RequestException as e:
        """
        If there is an exception (requests.RequestException is base for several different exceptions),
        log it and raise http 503 "Service Unavailable"
        """
        log.error(f"Error for eventual logo url {eventual_logo_url}: {e}")
        raise ServiceUnavailable('Error getting logo')
    else:
        r.raise_for_status()
        if r.status_code == 200:
            log.debug(f"Eventual logo url {eventual_logo_url}")
            return eventual_logo_url
        else:  # Status code is not an error, but not 200/OK
            log.warning(f"Http status code for {eventual_logo_url} was {r.status_code} ")
            return None


def get_not_found_logo_file():
    global not_found_file
    if not_found_file is None:
        not_found_filepath = current_dir + "../resources/1x1-00000000.png"
        log.debug(f'Opening global file: {not_found_filepath}')
        with open(not_found_filepath, 'rb') as f:
            not_found_file = f.read()

    return not_found_file


def fetch_ad_logo(ad_id: str):
    if settings.COMPANY_LOGO_FETCH_DISABLED:
        logo_url = None
    else:
        ad = fetch_ad_by_id(ad_id)
        logo_url = get_correct_logo_url(ad)

    if logo_url is None:
        log.debug(f"Logo url for ad with id {ad_id} not found, sending empty image")
        return file_formatter(get_not_found_logo_file())
    else:
        try:
            r = requests.get(logo_url, stream=True, timeout=settings.COMPANY_LOGO_TIMEOUT)
        except requests.RequestException as e:
            """
            If there is an exception (requests.RequestException is base for several different exceptions),
            log it and raise http 503 "Service Unavailable"
            """
            log.error(f"Error for logo url {logo_url}: {e}")
            raise ServiceUnavailable(f'Error getting logo for id {ad_id}')
        if r.status_code != 200:
            log.error(f"Status code {r.status_code} for {logo_url}")
            r.raise_for_status()
        log.debug(f"Logo found on: {logo_url}")
        return file_formatter(r.raw.read(decode_content=False))


def file_formatter(file_object):
    return send_file(
        io.BytesIO(file_object),
        download_name='logo.png',
        mimetype='image/png'
    )


def logo_url_creator(ad: dict) -> str:
    """
    Build a url to the logo from information in the "employer" field.
    If "workplace_id" is used in the ads, that is used, otherwise the employer's organization number is used.
    """
    eventual_logo_url = None

    if isinstance(ad, dict) and (employer := ad.get('employer', {})):
        if workplace_id := employer.get('workplace_id', None):
            '''
            Special logo for workplace_id for ads with source_type VIA_AF_FORMULAR or
            VIA_PLATSBANKEN_AD or VIA_ANNONSERA (workplace_id > 0)
            '''
            if int(workplace_id) > 0:
                eventual_logo_url = f'{base_url}/arbetsplatser/{workplace_id}/logotyper/logo.png'

        elif org_number := employer.get('organization_number', None):
            eventual_logo_url = f'{base_url}/organisation/{org_number}/logotyper/logo.png'
    return eventual_logo_url
