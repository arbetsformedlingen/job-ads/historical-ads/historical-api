import logging

import requests

from common import settings

log = logging.getLogger(__name__)


class TaxonomyLookup(object):
    def __init__(self):
        pass

    def fetch_taxonomy_values(self, query):
        log.debug(f"Fetching data from Taxonomy with query: {query}")
        params = {"query": query}
        taxonomy_response = requests.get(settings.TAXONOMY_GRAPHQL_URL, headers={}, params=params, timeout=10)
        taxonomy_response.raise_for_status()

        json_values = taxonomy_response.json()
        values = json_values.get("data", {}).get("concepts", [])
        log.debug(f"Taxonomy values: {values}")
        return values
