import logging

from flashtext.keyword import KeywordProcessor

from search.common_search.ads_labels import AdsLabels
from search.common_search.helpers import clean_plus_minus

log = logging.getLogger(__name__)

OP_NONE = ''
OP_MINUS = '-'


class TextToLabel(object):
    extracted_ads_labels = None
    ads_labels = None

    def __init__(self, ads_labels: AdsLabels = None):
        log.info(f"Initializing TextToLabel")
        if not ads_labels:
            self.ads_labels = AdsLabels()
        else:
            self.ads_labels = ads_labels


    @staticmethod
    def init_keyword_processor(keyword_processor):
        [keyword_processor.add_non_word_boundary(token) for token in list('åäöÅÄÖ()-*')]

    def get_keyword_processor(self):
        current_extracted_labels = self.ads_labels.get_extracted_ads_labels()
        if self.extracted_ads_labels != current_extracted_labels:
            self.extracted_ads_labels = current_extracted_labels
            self.keyword_processor = KeywordProcessor()
            self.init_keyword_processor(self.keyword_processor)
            self.init_dictionary(self.keyword_processor)

        return self.keyword_processor

    def init_dictionary(self, keyword_processor):
        for label in self.extracted_ads_labels:
            label_obj = {'term': label.lower(), 'label': label, 'operator': ''}
            keyword_processor.add_keyword(label_obj['term'], label_obj)


    def text_to_labels(self, text):
        # Note: Remove eventual '+' and '-' in every freetext query word since flashText is
        # configured so it can't find words starting with minus/hyphen.
        search_text = clean_plus_minus(text)
        text_lower = text.lower()

        extracted_labels_orig = self.get_keyword_processor().extract_keywords(search_text, span_info=True)
        extracted_labels = [el[0] for el in extracted_labels_orig]

        log.debug(f'Text: {text} extracted_labels: {extracted_labels}')

        text_lower_plus_blank_end = text_lower + ' '

        for extracted_label_obj in extracted_labels:
            label_term = extracted_label_obj['term']
            negative_label_term = '-' + label_term + ' '
            if ' ' + negative_label_term in text_lower_plus_blank_end or \
                    text_lower_plus_blank_end.startswith(negative_label_term):
                # Will end up with a "must_not"-search for this label.
                extracted_label_obj['operator'] = OP_MINUS
            else:
                # Unless the end user typed a minus before the label term, label is a "must"-search.
                extracted_label_obj['operator'] = OP_NONE

        labels_should = [el for el in extracted_labels if self.filter_labels(el, OP_NONE)]
        labels_must_not = [el for el in extracted_labels if self.filter_labels(el, OP_MINUS)]

        result = {
            'label': labels_should,
            'label_must_not': labels_must_not
        }

        return result

    @staticmethod
    def filter_labels(label_obj, operator):
        return label_obj['operator'] == operator
