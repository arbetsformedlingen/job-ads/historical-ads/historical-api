import json
import logging

from beaker.cache import CacheManager
from beaker.util import parse_cache_config_options

from common import settings
from common.opensearch_connection_with_retries import opensearch_search_with_retry
from common.main_opensearch_client import opensearch_client

log = logging.getLogger(__name__)

# Note: We want to cache the values for labels in ads, since we don't want to make an aggs query for every
# request to the API. The cache is set to expire after a certain time, and will be reloaded when the cache expires,
# so eventual new labels in the ads will be loaded.
cache_opts = {
    'cache.type': 'memory',
    'cache.expire': settings.ADS_LABELS_CACHE_EXP_SECONDS
}

cache = CacheManager(**parse_cache_config_options(cache_opts))

class AdsLabels(object):
    '''
    Class for fetching unique labels from ads in database.
    '''
    client = None

    def __init__(self):
        self.ads_index = settings.ADS_ALIAS

    @cache.cache('get_extracted_ads_labels')
    def get_extracted_ads_labels(self):
        return self._get_unique_labels_from_ads()

    def _get_client(self):
        if not self.client:
            self.client = opensearch_client()
        return self.client

    def _get_unique_labels_from_ads(self) -> set:
        '''
        Loads unique labels from the ads in Elastic.
        :return: A set with labels from structured ad data.
        '''
        query = {
            "size": 0,
            "aggs": {
                "unique_labels": {
                    "terms": {
                        "field": "label.keyword",
                        "size": 20000
                    }
                }
            }
        }
        log.info(f"AdsLabels(_get_unique_labels_from_ads). Searching for labels in ads from index: {self.ads_index}")
        log.debug(f"AdsLabels(_get_unique_labels_from_ads). Query: {json.dumps(query)}")

        results = opensearch_search_with_retry(self._get_client(), query, self.ads_index, hard_failure=True)
        ext_buckets = results.get('aggregations', {}).get('unique_labels', {}).get('buckets', [])
        found_labels = [p['key'] for p in ext_buckets]

        log.info(f"AdsLabels(_get_unique_labels_from_ads). Found {len(found_labels)} unique labels in the ads: {found_labels}")

        return set(found_labels)
