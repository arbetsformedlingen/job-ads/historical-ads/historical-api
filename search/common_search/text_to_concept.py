import logging

from common import settings
from search.common_search.ads_locations import AdsLocations
from search.common_search.helpers import clean_plus_minus
from search.common_search.synonym_dictionary import SynonymDictionary

log = logging.getLogger(__name__)

OP_NONE = ''
OP_PLUS = '+'
OP_MINUS = '-'


class TextToConcept(object):
    COMPETENCE_KEY = 'KOMPETENS'
    OCCUPATION_KEY = 'YRKE'
    TRAIT_KEY = 'FORMAGA'
    LOCATION_KEY = 'GEO'
    REMOVED_TAG = '<removed>'

    def __init__(self):
        log.info(f"Using synonym dictionary loaded from endpoint: {settings.JAE_API_URL}/synonymdictionary")

        self._synonym_dictionary = None
        self._extracted_ads_locations = None

        if not settings.DELAY_LOAD_SYNONYM_DICTIONARY_STARTUP:
            # Cache synonym dictionary directly unless it's a local call (tests or docker build)
            self._extracted_ads_locations = self.get_extracted_ads_locations()
            self._synonym_dictionary = self.get_synonym_dictionary()

    def get_synonym_dictionary(self):
        if self._synonym_dictionary is None:
            log.info('Creating SynonymDictionary')
            self._synonym_dictionary = SynonymDictionary()
            log.info('Done creating SynonymDictionary')
            for extracted_term in self.get_extracted_ads_locations():
                # Make sure that scraped/structured GEO terms from the ads also have been added to the synonym dictionary
                self._synonym_dictionary.add_concept(extracted_term, extracted_term, self.LOCATION_KEY)

        return self._synonym_dictionary

    def get_extracted_ads_locations(self):
        if self._extracted_ads_locations is None:
            log.info('Starting to extract locations from AdsLocations')
            ads_locations = AdsLocations()
            self._extracted_ads_locations = ads_locations.get_extracted_locations()
            log.info('Done extracting locations from AdsLocations')

        return self._extracted_ads_locations

    def text_to_concepts(self, text):
        # Note: Remove eventual '+' and '-' in every freetext query word since flashText is
        # configured so it can't find words starting with minus/hyphen.
        search_text = clean_plus_minus(text)
        text_lower = text.lower()
        synonym_concepts_orig = self.get_synonym_dictionary().get_concepts(search_text, concept_type=None,
                                                                           span_info=True)
        synonym_concepts = [c[0] for c in synonym_concepts_orig]
        log.debug(f'Text: {text} synonym_concepts: {synonym_concepts}')
        text_lower_plus_blank_end = text_lower + ' '

        for concept in synonym_concepts:
            concept_term = concept['term']
            negative_concept_term = '-' + concept_term + ' '
            if ' ' + negative_concept_term in text_lower_plus_blank_end or \
                    text_lower_plus_blank_end.startswith(negative_concept_term):
                concept['operator'] = OP_MINUS
            elif '+' + concept_term + ' ' in text_lower_plus_blank_end:
                concept['operator'] = OP_PLUS
            else:
                concept['operator'] = OP_NONE

        skills = [c for c in synonym_concepts if self.filter_concepts(c, self.COMPETENCE_KEY, OP_NONE)]
        occupations = [c for c in synonym_concepts if self.filter_concepts(c, self.OCCUPATION_KEY, OP_NONE)]
        traits = [c for c in synonym_concepts if self.filter_concepts(c, self.TRAIT_KEY, OP_NONE)]
        locations = [c for c in synonym_concepts if self.filter_concepts(c, self.LOCATION_KEY, OP_NONE)]
        skills_must = [c for c in synonym_concepts if self.filter_concepts(c, self.COMPETENCE_KEY, OP_PLUS)]
        occupations_must = [c for c in synonym_concepts if self.filter_concepts(c, self.OCCUPATION_KEY, OP_PLUS)]
        traits_must = [c for c in synonym_concepts if self.filter_concepts(c, self.TRAIT_KEY, OP_PLUS)]
        locations_must = [c for c in synonym_concepts if self.filter_concepts(c, self.LOCATION_KEY, OP_PLUS)]
        skills_must_not = [c for c in synonym_concepts if self.filter_concepts(c, self.COMPETENCE_KEY, OP_MINUS)]
        occupations_must_not = [c for c in synonym_concepts if self.filter_concepts(c, self.OCCUPATION_KEY, OP_MINUS)]
        traits_must_not = [c for c in synonym_concepts if self.filter_concepts(c, self.TRAIT_KEY, OP_MINUS)]
        locations_must_not = [c for c in synonym_concepts if self.filter_concepts(c, self.LOCATION_KEY, OP_MINUS)]

        result = {'skill': skills,
                  'occupation': occupations,
                  'trait': traits,
                  'location': locations,
                  'skill_must': skills_must,
                  'occupation_must': occupations_must,
                  'trait_must': traits_must,
                  'location_must': locations_must,
                  'skill_must_not': skills_must_not,
                  'occupation_must_not': occupations_must_not,
                  'trait_must_not': traits_must_not,
                  'location_must_not': locations_must_not}

        return result

    @staticmethod
    def filter_concepts(concept, concept_type, operator):
        return concept['type'] == concept_type and concept['operator'] == operator
