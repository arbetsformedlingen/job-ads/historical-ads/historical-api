import logging
import time

from flask_restx import Resource
from flask_restx import abort

from common.ad_formatter import format_hits_with_only_original_values
from search.common_search.ad_search import fetch_ad_by_id
from search.common_search.querybuilder import QueryBuilderType
from search.common_search.stats.stats import get_stats
from search.rest.endpoint.common import QueryBuilderContainer, \
    search_endpoint
from search.rest.model.apis import ns_historical, open_results, job_ad
from search.rest.model.queries import load_ad_query, historical_query, stats_query
from search.rest.model.swagger import swagger_doc_params, swagger_filter_doc_params, taxonomy_doc_params

log = logging.getLogger(__name__)

querybuilder_container = QueryBuilderContainer()


@ns_historical.route('ad/<id>', endpoint='ad')
class AdById(Resource):

    @ns_historical.doc(
        description='Load a job ad by ID',
    )
    @ns_historical.response(404, 'Job ad not found')
    @ns_historical.expect(load_ad_query)
    @ns_historical.marshal_with(job_ad)
    def get(self, id, **kwargs):
        result = fetch_ad_by_id(str(id), historical=True)
        return format_hits_with_only_original_values([result])[0]


@ns_historical.route('search')
class Search(Resource):

    @ns_historical.doc(
        description='Search using parameters and/or freetext',
        params={
            **swagger_doc_params, **swagger_filter_doc_params
        },
    )
    @ns_historical.expect(historical_query)
    @ns_historical.marshal_with(open_results)
    def get(self, **kwargs):
        start_time = int(time.time() * 1000)
        try:
            args = historical_query.parse_args()
            return search_endpoint(querybuilder_container.get(QueryBuilderType.HISTORICAL_SEARCH), args, start_time)
        except ValueError as e:
            log.warning(e)
            abort(400)
        except Exception as e:
            log.exception(e)
            raise

@ns_historical.route('stats')
class Stats(Resource):

    @ns_historical.doc(
        description='Load stats by taxonomy type',
        params={
            **taxonomy_doc_params
        },
    )
    @ns_historical.expect(stats_query)
    def get(self, **kwargs):
        args = stats_query.parse_args()
        return get_stats(args)
