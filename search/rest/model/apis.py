from flask_restx import Api, Namespace, fields

from common import constants
from common.result_model import root_api, job_ad

# Common result models

job_ad_searchresult = root_api.inherit('JobAdSearchResult', job_ad, {
    'relevance': fields.Float(),
})

stat_item = root_api.model('StatDetail', {
    'term': fields.String(),
    'concept_id': fields.String(),
    'code': fields.String(),
    'count': fields.Integer()
})

search_stats = root_api.model('Stats', {
    'type': fields.String(),
    'values': fields.List(fields.Nested(stat_item, skip_none=True))
})

freetext_concepts = root_api.model('FreetextConcepts', {
    'skill': fields.List(fields.String()),
    'occupation': fields.List(fields.String()),
    'location': fields.List(fields.String()),
    'skill_must': fields.List(fields.String()),
    'occupation_must': fields.List(fields.String()),
    'location_must': fields.List(fields.String()),
    'skill_must_not': fields.List(fields.String()),
    'occupation_must_not': fields.List(fields.String()),
    'location_must_not': fields.List(fields.String()),
})

number_of_hits = root_api.model('NumberOfHits', {
    'value': fields.Integer()
})

open_results = root_api.model('SearchResults', {
    'total': fields.Nested(number_of_hits),
    'positions': fields.Integer(),
    'query_time_in_millis': fields.Integer(),
    'result_time_in_millis': fields.Integer(),
    'stats': fields.List(fields.Nested(search_stats, skip_none=True)),
    'freetext_concepts': fields.Nested(freetext_concepts, skip_none=True),
    'hits': fields.List(fields.Nested(job_ad_searchresult), attribute='hits', skip_none=True)
})

# Historical ads

historical_api = Api(version=constants.API_VERSION,
                     title='Historical job ads',
                     description='## An API for searching and retrieving historical job ads from the Swedish Public Employment Service\n'
                                 '**Useful links:**\n'
                                 '- [Historical Ads info - API and files](https://gitlab.com/arbetsformedlingen/job-ads/getting-started-code-examples/historical-ads-info)\n'
                                 '- [Taxonomy Getting Started](https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-api/-/blob/develop/GETTINGSTARTED.md)\n'
                                 '- [Taxonomy Atlas](https://atlas.jobtechdev.se)\n',
                     default_label="An API for retrieving historical job ads",
                     contact_url='https://forum.jobtechdev.se/c/vara-api-er-dataset/historiska-annonser/30',
                     contact='Contact: JobTech Forum',
                     license='Ads are licensed under CC0',
                     license_url='https://creativecommons.org/publicdomain/zero/1.0/deed.sv'
                     )

ns_historical = Namespace('Historical ads',
                          description="Search and retrieve historical job ads from Arbetsförmedlingen. The oldest ads are from 2016.\n")

historical_api.add_namespace(ns_historical, '/')

for name, definition in root_api.models.items():
    ns_historical.add_model(name, definition)
