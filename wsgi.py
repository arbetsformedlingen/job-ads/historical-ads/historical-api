import logging
from flask import Flask
from flask_cors import CORS
from jobtech.common.customlogging import configure_logging
from common import appconf, settings
from common.main_opensearch_client import opensearch_client
from search.rest.model.apis import historical_api
from search.rest.endpoint.historical import querybuilder_container
# Import all Resources that are to be made visible for the app
# PyCharm marks them as unused imports, but they are needed
# noinspection PyUnresolvedReferences

app = Flask(__name__, static_url_path='')
CORS(app)
# Set up logging with the package names that should be included in the log output
configure_logging([__name__.split('.')[0], 'jobtech', 'common', 'search'])
log = logging.getLogger(__name__)
log.info(logging.getLevelName(log.getEffectiveLevel()) + ' log level activated')
log.info(f"Starting: {__name__}")

opensearch_client.launch(db_timeout=settings.DB_HISTORICAL_TIMEOUT)
querybuilder_container.launch()

if __name__ == '__main__':
    # Used only when starting this script directly, i.e. for debugging
    appconf.initialize_app(app, historical_api)
    app.run(debug=True)
else:
    # Main entrypoint
    appconf.initialize_app(app, historical_api)
