API_VERSION = '1.31.0'

X_FEATURE_FREETEXT_BOOL_METHOD = 'x-feature-freetext-bool-method'
X_FEATURE_DISABLE_SMART_FREETEXT = 'x-feature-disable-smart-freetext'
X_FEATURE_ENABLE_FALSE_NEGATIVE = 'x-feature-enable-false-negative'

OFFSET = 'offset'
LIMIT = 'limit'
FREETEXT_QUERY = 'q'
TYPEAHEAD_QUERY = 'typehead'
CONTEXTUAL_TYPEAHEAD = 'contextual'
UNSPECIFIED_SWEDEN_WORKPLACE = 'unspecified-sweden-workplace'
ABROAD = 'abroad'
REMOTE = 'remote'
TRAINEE = 'trainee'
LARLING = 'larling'
FRANCHISE = 'franchise'
HIRE_WORK_PLACE = 'hire-work-place'
FREETEXT_FIELDS = 'qfields'
SORT = 'sort'
PUBLISHED_BEFORE = 'published-before'
PUBLISHED_AFTER = 'published-after'
EXPERIENCE_REQUIRED = 'experience'
STATISTICS = 'stats'
STAT_LMT = 'stats.limit'
PARTTIME_MIN = 'parttime.min'
PARTTIME_MAX = 'parttime.max'
POSITION = 'position'
POSITION_RADIUS = 'position.radius'
DEFAULT_POSITION_RADIUS = 5
EMPLOYER = 'employer'
OPEN_FOR_ALL = 'open_for_all'
HISTORICAL_FROM = 'historical-from'
HISTORICAL_TO = 'historical-to'
HISTORICAL_REQUEST_TIMEOUT = 'request-timeout'
START_SEASONAL_TIME = 'start-seasonal-time'
END_SEASONAL_TIME = 'end-seasonal-time'
OCCUPATION = 'occupation-name'
OCCUPATION_GROUP = 'occupation-group'
OCCUPATION_FIELD = 'occupation-field'
MUNICIPALITY = 'municipality'
DEFAULT_FREETEXT_BOOL_METHOD = 'or'
MAX_OFFSET = 2000
MAX_LIMIT = 100
MAX_TAXONOMY_LIMIT = 8000
MAX_COMPLETE_LIMIT = 50
RESULT_MODEL = 'resultmodel'
DETAILS = 'resdet'
MIN_RELEVANCE = 'relevance-threshold'
SHOW_COUNT = 'show-count'
DATE = 'date'
UPDATED_BEFORE_DATE = 'updated-before-date'
MAX_DATE = '3000-01-01T00:00:00'
OCCUPATION_CONCEPT_ID = 'occupation-concept-id'
LOCATION_CONCEPT_ID = 'location-concept-id'
OCCUPATION_LIST = ['occupation', 'occupation_field', 'occupation_group']
LOCATION_LIST = ['region', 'country', 'municipality']
SHOW_EXPIRED = 'show-expired'
SWEDEN_CONCEPT_ID = 'i46j_HmG_v64'
UNWANTED_SUGGESTED_WORDS = ['sverige', 'svenska']
DATE_FORMAT = "%Y-%m-%dT%H:%M:%S"
TAXONOMY_TYPE = 'taxonomy-type'
STATS_BY = 'stats-by'
LEGACY_ID = 'legacy_ams_taxonomy_id'
CONCEPT_ID = 'concept_id'
LABEL = 'label'
TAXONOMY_TYPE_LIST = ['occupation-name', 'occupation-group', 'occupation-field', 'employment-type', 'country', 'region',
                      'municipality', 'language', 'skill']
RESTPLUS_SWAGGER_UI_DOC_EXPANSION = 'list'
RESTPLUS_VALIDATE = False
RESTPLUS_MASK_SWAGGER = False
RESTPLUS_ERROR_404_HELP = False
result_models = ['pbapi', 'simple']
HISTORICAL_ORIGINAL_ID = 'original-id'
DURATION = 'duration'