import os


# Opensearch settings
DB_HOST = os.getenv("DB_HOST", "127.0.0.1").split(",")
DB_PORT = os.getenv("DB_PORT", 9200)
DB_USER = os.getenv("DB_USER")
DB_PWD = os.getenv("DB_PWD")
DB_USE_SSL = os.getenv("DB_USE_SSL", "true").lower() == "true"
DB_VERIFY_CERTS = os.getenv("DB_VERIFY_CERTS", "true").lower() == "true"

ADS_ALIAS = os.getenv("ADS_ALIAS", "historical.develop")

DB_TAX_INDEX_ALIAS = os.getenv("DB_TAX_INDEX_ALIAS", "taxonomy")
DB_RETRIES = int(os.getenv("DB_RETRIES", 1))
DB_RETRIES_SLEEP = int(os.getenv("DB_RETRIES_SLEEP", 2))
DB_DEFAULT_TIMEOUT = int(os.getenv("DB_DEFAULT_TIMEOUT", 10))

DB_HISTORICAL_TIMEOUT = int(os.getenv("DB_HISTORICAL_TIMEOUT", 300))

DELAY_LOAD_SYNONYM_DICTIONARY_STARTUP = (
    os.getenv("DELAY_LOAD_SYNONYM_DICTIONARY_STARTUP", "false").lower() == "true"
)

BASE_PB_URL = os.getenv(
    "BASE_PB_URL", "https://arbetsformedlingen.se/platsbanken/annonser/"
)

COMPANY_LOGO_BASE_URL = os.getenv(
    "COMPANY_LOGO_BASE_URL",
    "https://www.arbetsformedlingen.se/rest/arbetsgivare/rest/af/v3/",
)
COMPANY_LOGO_FETCH_DISABLED = (
    os.getenv("COMPANY_LOGO_FETCH_DISABLED", "false").lower() == "true"
)
COMPANY_LOGO_TIMEOUT = os.getenv("COMPANY_LOGO_TIMEOUT", 5)

ADS_LABELS_CACHE_EXP_SECONDS = int(os.getenv("ADS_LABELS_CACHE_EXP_SECONDS", 600))

HISTORICAL_START_YEAR = int(os.getenv("HISTORICAL_START_YEAR", 2016))

JAE_API_URL = os.getenv("JAE_API_URL", "https://jobad-enrichments-api.jobtechdev.se")

TAXONOMY_PROCESSES = int(os.getenv("TAXONOMY_PROCESSES", 8))


TAXONOMY_URL = os.getenv('TAXONOMY_URL', "https://taxonomy.api.jobtechdev.se/v1/taxonomy")
TAXONOMY_GRAPHQL_URL = f"{TAXONOMY_URL}/graphql?"
TAXONOMY_VERSION = int(os.getenv('TAXONOMY_VERSION', 21))