import calendar
import re

from datetime import datetime, timedelta
from dateutil import parser
from common import constants


def get_formatted_start_date(published_after, historical_from):
    if published_after:
        return format_start_date(published_after)
    return format_start_date(historical_from)


def get_formatted_end_date(published_before, historical_to):
    if published_before:
        return format_end_date(published_before)
    return format_end_date(historical_to)


def format_start_date(start_date):
    """
    Will format date and add month and day if missing.
    See examples in unit tests.
    Returns a timestamp as string.
    """
    if not start_date:
        return None

    # Check if start_date is a purely numeric string but not a valid date format
    if not re.match(r'^\d{4}$|^\d{4}-\d{2}$|^\d{4}-\d{2}-\d{2}$|^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}$', start_date):
        raise ValueError("Invalid date format: Input should be a valid date string")

    default_start = datetime(year=2000, month=1, day=1)
    return parser.parse(start_date, default=default_start).strftime(constants.DATE_FORMAT)


def format_end_date(end_date):
    """
    If only year is passed, the date will be formatted to the last second of the year (yyyy-12-31T23:59:59).
    If year and month are passed, the last day of that month will be used (leap year check is included) (yyyy-mm-<last day of month>T23:59:59).
    If yyyy-mm-dd is passed, hh:mm:ss will be modified to 23:59:59.
    See examples in unit tests.
    Returns a timestamp as string.
    """
    dt = datetime.now() - timedelta(days=1)
    default_end_date = dt.replace(hour=23, minute=59, second=59, microsecond=0)
    if not end_date:
        """
        Default value to prevent the query from returning ads with 
        future dates.
        """
        return default_end_date.strftime(constants.DATE_FORMAT)
    elif len(end_date) == 4:  # YYYY
        dt = parser.parse(end_date)
        new_end_date = dt.replace(month=12, day=31, hour=23, minute=59, second=59)
    elif len(end_date) == 7:  # YYYY-MM
        dt = parser.parse(end_date)
        # Check how many days the month has for a particular year (handles leap years)
        number_of_days_in_month = calendar.monthrange(dt.year, dt.month)[1]
        new_end_date = dt.replace(day=number_of_days_in_month, hour=23, minute=59, second=59)
    elif len(end_date) == 10:  # YYYY-MM-DD
        # Create a datetime object and modify hh:mm:ss
        dt = parser.parse(end_date)
        new_end_date = dt.replace(hour=23, minute=59, second=59)
    else: # YYYY-MM-DDTHH:MM:SS
        new_end_date = parser.parse(end_date)

    if new_end_date > default_end_date:
        new_end_date = default_end_date

    return new_end_date.strftime(constants.DATE_FORMAT)
