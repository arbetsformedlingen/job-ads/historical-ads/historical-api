# Changelog
## 1.32.1 (June, 2024)
* Bug Fix: Historical ads from the future shows up. Added default 'published-after' date.
* Bug Fix: Fixed the issue with parsing date for 'published-after' parameter.
* Made the value of DB_USE_SSL and DB_VERIFY_CERTS 'True' by default.
* Bug Fix: Fixed an issue with parsing date for 'published-before' parameter when using format YYYY-mm-ddTHH:MM:SS
* Improved documentation in Swagger for 'published-before' and 'published-after' parameters.
* Fixed bug when fetching an ad by "id" and the ad has "removed": true, so the ad will be returned as expected.
* Improved handling when searching on municipality_id, for more/same hits as when searching by municipality_code.

## 1.32.0 (May, 2024)
* Standardize parsing mechanism for 'published-before' and 'published-after' parameters to ensure consistent error handling.
* Cleaned out dead code, all references of JobSearch and JobStream removed.

## 1.31.0
* Finally removed deprecated endpoint for temporary Jobtech Taxonomy.
* Fixed logging configuration so info-logging is printed for the packages 'search' and 'stream'.
* Update to Debian 12 in Dockerfile.

## 1.30.5 (January 2024)
* Added query parameter `duration`.
* Removed code and dependencies for using APM.
* Update Python version in Dockerfile to 3.10.13 and use docker.io instead of Jobtech mirror.
* Update dependencies.

## 1.29.0 (June 2023)
* Load synonym dictionary from JobAd Enrichments API instead of from file or Elastic.

## 1.28.0 (April 2023)
* Historical ads: When searching for a duplicate ID in the ad/id endpoint, a custom response with http status code 409/CONFLICT will be returned with ids that can be used to retrieve all duplicates.
* Historical ads: Search for historical "original id" (the id the ad had when it was published) is now possible in the q-field.
* Update documentation for `/complete` endpoint.
* Update dependencies.

## 1.27.0 (February 2023)
* Load synonym dictionary from file instead of Elastic for quicker start with less resource usage.
* Use Python 3.10.7 in Dockerfile.
* Do not include application contacts with null values.
* Clean up of Elastic index variables.

## 1.26.0 (April 2022)
* Install module JobTech common från JobTech's pypi.
* Keep configuration that uses environment variables in settings.py, move other values to constants.py.
* Internal name changes and cleanup.
* Use common formatter of api response for all api:s.
* Use common code for stream and snapshot endpoints.
* Use older Elasticsearch client for compatibility.

## 1.25.0 (March 2022)
* Add links to readme, code example and taxonomy info in Swagger.
* Update Python version to 3.10.1.
* Remove check of api key, and associated field and text in Swagger.
* Remove taxonomy api key (used by tests) since that service no longer require it.
* Use default values for Elastic APM user context (no api key to get info from).
* Add package psmisc to get the killall command in the Docker.

## 1.24.0 (March 2022)
* Add 'education' and 'education_level' to api response in 'must_have' and 'nice_to_have'. Empty list [] if there is no data.
* Configuration changes for APM.
* Remove check of api key for historical ads.
* Install 'jobtech-common' module from GitLab with desired commit hash.
* Modify and rename ad/id endpoint function so that it can handle historical ads as well (multiple indices).

## 1.23.0
* Add bool parameter ('hire-work-place') to find ads matching phrases related to rental of a place / chair.
* Improve error handling in ad/<id>/logo endpoint.
* Set http keep-alive for better performance.
* Update Python version in Dockerfile.

## 1.22.0
* Add bool parameter ('franchise') to find ads matching phrases related to positions as franchisee.

## 1.21.0
* Add bool parameter ('larling') to find ads matching phrases related to positions as "lärling".

## 1.20.0
* Add bool parameter ('trainee') to find ads matching phrases related to trainee work.
* Add bool parameter ('open_for_all') to find ads matching the "öppen för alla" criteria.
* Add seasonal time filter for historical ads.
* Change uwsgi setup and logging.

## 1.19.1
* Remove nginx.
* Phrases for "remote work" removed from Swagger.
* Increase uwsgi timeout to get historical long response.

## 1.19.0
* Add Contact Persons to ad.
* Fix bug in complete where 'Sverige' was suggested.
* Add option to use short timestamps in historical ads.
* Increase timeout for Elastic queries in historical ads.

## 1.18.3
* Revert bugfix in 1.18.2 for "complete when contextual=False, wrong number of occurrences were given for misspelled terms" due to performance issues.

## 1.18.2
* Add field 'result_time_in_millis' to responses in /complete endpoint.
* Renamed 'place of work' in Swagger to 'employer name' to make it clearer which fields in the ads are included in search.
* Fix bug in complete when contextual=False, wrong number of occurrences were given for misspelled terms.
* Fix bug in complete when some characters were used (e.g. quotes, brackets or parentheses).
* Code changes to how the application starts.
* Fix bug with negative search (minus) and occupations with dashes.
* Search with quotes now also searches in employer name.

## 1.18.1
* Fix bug in stream result set (remove array type of "employment_type").
* Fix bug in stream result set (show occupation group and field values instead of null).
* Fix bug in /ad endpoint (show 404 for en ad in main index set as "removed").

## 1.18.0
* Make taxonomy version 2 filterable and searchable.
* Make ads searchable by related taxonomy concept ids, but only show original concept id (entered by employer).

## 1.17.1
* Remove unnecessary log.

## 1.17.0
* Add filter for remote jobs.
* Add retries and error handling to Elastic connections.
* Fix bug in /complete endpoint when search is misspelled and filters are used.
* Fix bug where boolean args are sent as string.
* Improve performance for stats.

## 1.16.1
* Fix bug related to headline search.
* Fix bug related to quotes.
* Fix bug where irrelevant results were shown for '.net' searches.

## 1.16.0
* Fix multi word /complete spelling bug.
* Set DEFAULT_FREETEXT_BOOL_METHOD to 'or'.
* Abroad filter added for inclusion of ads from other countries than sweden in region and municipality filtered searches.

## 1.15.0
* Add occupation-collection filter parameter to search.
* Remove flags 'x-feature-allow-empty-typeahead', 'x-feature-include-synonyms-typeahead', 'x-feature-spellcheck-typeahead', 'x-feature-suggest-extra-word' and set behavior as if they were set to 'True'.
* Fix bug in complete that gave geographical suggestions that were not relevant.

## 1.14.1
* Enable "false negative" (i.e. free text search in ad text). Toggled by new x-feature parameter where disabled is default.

## 1.14.0
* Use new version of jae elasticsearch index (i.e. v-2.0.1.210) for Ontology dictionary. This is dependent on calling the new enrichment endpoint from the elastic importer, that should correspond.

## 1.13.2
* Fix max score bug.
* Add slack client.

## 1.13.1
* Set APM log level.

## 1.13.0
* Fix removed ads ID data type inconsistency.
* Add test cases.

## 1.12.1
* Add limit to complete endpoint.

## 1.12.0
* Allow empty space for autocomplete endpoint.
* Phrase search searches in both description.text and headline.

## 1.11.1
* Add concept Id to aggregation.

## 1.11.0
* Suggest extra word.
* Add taxonomy description.
* Fix spell check extra space problem.

## 1.10.0
* Auto complete & spell check.

## 1.9.0
* Add one field called unspecified sweden workplace.

## 1.8.1
* Fixes bug that searched several fields for identified occupations.

## 1.8.0
* Bug fixes.
* Adds concept ID for municipality, region and country in workplace_address.
* Changes behaviour in search for locations. Any locations are now treated as boolean OR query.
* Adds "phrase searching". Use quotes to search the ad text for specific phrases.
* Adds feature toggle to disable query analysis in favour of simpler freetext search.
* Adds spellchecking typeahead feature toggle.
* Add prefix and postfix wildcard ('*') to search for partial words in ad text.

## 1.7.2
* Comma treated as delimiter in freetext queries.
* Fixes a bug that caused the employer field to be queried when it shouldn't.
* Fixes a bug in taxonomy-search where some labels couldn't be searched.
* Only display logo_url if available.

## 1.7.1
* Fix for terms containing hash/# in typeahead.
* Changed relevance sort order to have descending publication date as secondary.

## 1.

7.0
* Adds ML enriched locations to typeahead.
* Fixes a bug that wouldn't display number of ads for municipalities and regions in taxonomy search.
* Adds header x-feature-include-synonyms-typeahead for choosing if enriched synonyms should be included in typeahead.

## 1.6.0
* Introduces new lowercase type for request parsing.
* Fixes a bug in context-unaware typeahead.
* Fixes plus/minus-searches in employer.
* Adds header x-feature-freetext-bool-method for choosing search method for unclassified freetext words.
* Adds header x-feature-allow-empty-typeahead, enabling empty queries in typeahead.
* Adds ML-enriched searches for location.

## 1.5.1
* Bugfix reverting freetextsearch for locations in ad descriptions.

## 1.5.0
* Search-contextual switch for typeahead.
* Fixes an issue with taxonomy search.

## 1.4.3
* Buggfixes for freetext search and location.

## 1.4.2
* Buggfixes and documentation updates.

## 1.4.1
* New endpoint for fetching company logo file.

## 1.4.0
* New webpage url field in job ad.
* New logo url field in job ad.
* Language added as filter in taxonomy search.

## 1.3.0
* New parameter: relevance-threshold.
* Introduces new field: score.
* Location searches in headline instead of description text.
* Adds secondary sorting on relevance for all sort options.

## 1.2.0
* Parameter published-after now supports number of minutes as parameter.
* Location search in ad description requires exact phrase.
* Employer name is no longer default in typeahead.
* Ad loading now accepts alternate id.
* Queries now once again supports alternate locations in query.
* Typeahead rework to include all variants of query string.
* Fixes a bug in typeahead where location would be removed from choices.
* Fixes a bug in typeahead for capitalized searchwords.
* Fixes a bug in typeahead for '<' and '>'.

## 1.1.0
* Adds ability to search for IDs in Jobtech Taxonomy.
* More comprehensive typeahead functionality which includes bigrams and complete phrase.
* Compensates for timezone offset in date filter.
* Proper response model in swagger.json.
* Freetext matches in headline always qualifies as a hit.
* Narrows results for freetext query when using 'unknown words' instead of widening.
* Makes freetext search more accurate when searching headlines.

## 1.0.0
* Initial release.