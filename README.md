# Search ads API

This repository contains code for Historical APIs.

* ___Historical___ is a repository for ads that is not longer published or has expired.

## Requirements

* Python 3.9.4 or later
* Access to a host or server running Opensearch version 2.6.0 or later

## Environment variables

The application is entirely configured using environment variables.
Default values are provided with the listing.

### Application configuration

| Variable Name | Default Value    | Description                                                        |
|---------------|------------------|--------------------------------------------------------------------|
| DB_HOST       | 127.0.0.1        | Specifies which opensearch host to use for searching.              |
| DB_PORT       | 9200             | Port number to use for opensearch                                  |
| ADS_ALIAS     | current_ads      | Specifies which index to search ads from |
| DB_USER       |                  | Sets username for opensearch (no default value)                    |
| DB_PWD        |                  | Sets password for opensearch (no default value)                    |
| DB_TAX_INDEX  | taxonomy         | Specifies which index contains taxonomy information.               |

### Flask configuration

| Variable Name       | Default Value    | Description   |
|---                  |---               |---            |
| FLASK_APP           |                  | The name of the application. |
| FLASK_ENV           |                  | Set to "development" for development. |

## Running the service

To start up the application, set the appropriate environment variables as described above. Then run the following
commands.

    pip install -r requirements.txt
    export FLASK_ENV=development
    flask run

Go to <http://127.0.0.1:5000> to access the swagger API.  

## Tests

### Run unit tests

    pytest tests/unit_tests

### Run integration tests

When running integration tests, an actual application is started,
so you need to specify environment variables for opensearch in order for it to run properly.

    pytest tests/integration_tests

### Run API tests

When running api tests, the application must be started and a static test dataset must be used, see 
details below for importing the dataset.  

pytest tests/api_tests

#### Import the historical test dataset for the API tests: 
1. Login to https://minio-console.arbetsformedlingen.se/buckets/jobbdata/ 
2. Download the file `test_ads_historical.zip` and extract. The file `test_ads_2016_2023q1.jsonl` in the zipfile is used in the import.
3. Clone repo https://gitlab.com/arbetsformedlingen/job-ads/jobsearch-importers
4. In jobsearch-importers, run the command:  
   `historical opensearch <local path to downloaded file>\test_ads_2016_2023q1.jsonl`
    Example:  
   `historical opensearch C:\Users\username\Downloads\test_ads_historical\test_ads_2016_2023q1.jsonl`
 

### Smoke tests

A subset of the api tests to test broadly but not with all test cases. Used to save time, if smoke tests fail, there is no
point in running the entire test suite.

    pytest -m "smoke" tests/api_tests

### Test coverage

<https://pytest-cov.readthedocs.io/en/latest/>

    pytest --cov=. tests/
